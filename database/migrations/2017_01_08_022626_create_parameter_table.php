<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
                $table->integer('procedure_id')->unsigned()->index();
                $table->foreign('procedure_id')->references('id')->on('sampling_procedure')->onDelete('no action');
            $table->string('MDL');
            $table->integer('price');
            $table->smallInteger('accreditation');
            $table->string('standard');
            $table->smallInteger('check')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter');
    }
}
