<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSampleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sample', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_sample')->unique();
            

            $table->string('name');
            $table->date('date_sampled');
            $table->time('time_sampled');
            $table->string('status');
            $table->integer('price');
            $table->string('coordinat_S');
            $table->string('coordinat_E');
            $table->string('ph')->nullable;
            $table->string('suhu')->nullable;
            $table->string('sal')->nullable;
            $table->string('do')->nullable;
            $table->string('dhl')->nullable;
            $table->string('debit')->nullable;
            $table->string('kappro')->nullable;
            $table->text('info')->nullable;
            $table->boolean('nonaktif');
            $table->timestamps();

            $table->integer('job_id')->unsigned()->index();
            $table->foreign('job_id')->references('id')->on('job')->onDelete('cascade');
            $table->integer('ref_test_id')->unsigned()->index();
            $table->foreign('ref_test_id')->references('id')->on('ref_test')->onDelete('no action');
            $table->integer('procedure_id')->unsigned()->index();
            $table->foreign('procedure_id')->references('id')->on('sampling_procedure')->onDelete('no action');
            $table->integer('matrix_id')->unsigned()->index();
            $table->foreign('matrix_id')->references('id')->on('sample_matrix')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sample');
    }
}
