<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSamplingProcedureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sampling_procedure', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_procedure');
            $table->integer('matrix_id')->unsigned()->index();
                $table->foreign('matrix_id')->references('id')->on('sample_matrix')->onDelete('cascade');
            $table->boolean('nonaktif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sampling_procedure');
    }
}
