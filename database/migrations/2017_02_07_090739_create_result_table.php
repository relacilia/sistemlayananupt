<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result', function (Blueprint $table) {
            $table->increments('id');
            $table->string('value');
            $table->string('status_result');
            $table->timestamps();

            $table->integer('para_sample_id')->unsigned()->index();
            $table->foreign('para_sample_id')->references('id')->on('parameter_sample')->onDelete('no action');
            $table->boolean('nonaktif');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result');
    }
}
