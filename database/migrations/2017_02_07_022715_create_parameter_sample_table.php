<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterSampleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_sample', function (Blueprint $table) {
            $table->increments('id');
                $table->integer('sample_id')->unsigned()->index();
                $table->foreign('sample_id')->references('id')->on('sample')->onDelete('cascade');
                $table->integer('parameter_id')->unsigned()->index();
                $table->foreign('parameter_id')->references('id')->on('parameter')->onDelete('no action');
                $table->integer('user_id')->unsigned()->nullable()->index();
                $table->foreign('User_id')->references('id')->on('users')->onDelete('no action');
            $table->string('status_parameter')->nullable();
            $table->boolean('nonaktif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_sample');
    }
}
