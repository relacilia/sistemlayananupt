<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER `para_sample_status` AFTER UPDATE ON `parameter_sample`
        FOR EACH ROW BEGIN
                DECLARE count_tm, count_oktm, count_osv, count_all Int;
                SET count_tm = 0;
                SET count_all = 0;
                SET count_oktm = 0;
                SET count_osv = 0;
                
                SELECT count(*) INTO count_all FROM parameter_sample WHERE sample_id = NEW.sample_id;
                SELECT SUM(IF(status_parameter = "Onprogress (TM)", 1,0)) INTO count_tm FROM parameter_sample WHERE sample_id = NEW.sample_id;
                SELECT SUM(IF(status_parameter = "OK (TM)", 1,0)) INTO count_oktm FROM parameter_sample WHERE sample_id = NEW.sample_id;
                SELECT SUM(IF(status_parameter = "Onprogress (SV)", 1,0)) INTO count_osv FROM parameter_sample WHERE sample_id = NEW.sample_id;

                IF count_all = count_tm THEN
                    UPDATE sample SET status = "Onprogress (TM)" WHERE id = NEW.sample_id;
                ELSEIF count_all = count_oktm THEN
                    UPDATE sample SET status = "OK (TM)" WHERE id = NEW.sample_id;
                ELSEIF count_osv > 0 THEN
                    UPDATE sample SET status = "Onprogress (SV)" WHERE id = NEW.sample_id;
                ELSE
                    UPDATE sample SET status = "Onprogress (AN)" WHERE id = NEW.sample_id;
                END IF;
        END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `para_sample_status`');
    }
}
