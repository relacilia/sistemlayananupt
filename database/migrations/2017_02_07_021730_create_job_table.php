<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('job', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sampled_by');
            $table->string('attention');
            $table->string('location');
            $table->string('status');
            $table->date('date_recent');
            $table->time('time_recent');
            $table->integer('number_samples');
            $table->integer('price')->nullable();
            $table->date('interval');
            $table->boolean('nonaktif');
            $table->timestamps();

            $table->integer('customer_id')->unsigned();
                $table->foreign('customer_id')->references('id_customer')->on('customer')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job');
    }
}
