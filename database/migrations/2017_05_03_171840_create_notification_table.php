<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('para_sample_id')->unsigned()->index();
            $table->foreign('para_sample_id')->references('id')->on('parameter_sample')->onDelete('cascade');
            $table->integer('parameter_id')->unsigned()->index();
            $table->foreign('parameter_id')->references('id')->on('parameter')->onDelete('cascade');
            $table->integer('source_id')->unsigned()->index();
            $table->foreign('source_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('destination_id')->unsigned()->index();
            $table->foreign('destination_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('link');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification');
    }
}
