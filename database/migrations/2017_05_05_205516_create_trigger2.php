<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrigger2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
       CREATE TRIGGER `sample_status` AFTER UPDATE ON `sample`
         FOR EACH ROW BEGIN
                DECLARE count_tm, count_oktm, count_all Int;
                SET count_tm = 0;
                SET count_all = 0;
                SET count_oktm = 0;
                
                SELECT count(*) INTO count_all FROM sample WHERE job_id = NEW.job_id;
                SELECT SUM(IF(status = "Onprogress (TM)", 1,0)) INTO count_tm FROM sample WHERE job_id = NEW.job_id;
                SELECT SUM(IF(status = "OK (TM)", 1,0)) INTO count_oktm FROM sample WHERE job_id = NEW.job_id;

                IF count_all = count_oktm THEN
                    UPDATE job SET status = "OK" WHERE id = NEW.job_id;
                ELSEIF count_tm > 0 THEN
                    UPDATE job SET status = "Onprogress (TM)" WHERE id = NEW.job_id;
                ELSE
                    UPDATE job SET status = "Onprogress" WHERE id = NEW.job_id;
                END IF;
        END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `sample_status`');
    }
}
