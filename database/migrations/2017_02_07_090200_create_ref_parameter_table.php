<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefParameterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_parameter', function (Blueprint $table) {
            $table->increments('id');
            $table->string('volume_waste_maksimum')->nullable();
            $table->string('sign')->nullable();
            $table->string('dosage_maksimum')->nullable();
            $table->boolean('nonaktif');
            $table->timestamps();

            $table->integer('ref_test_id')->unsigned()->index();
            $table->foreign('ref_test_id')->references('id')->on('ref_test')->onDelete('no action');
            $table->integer('parameter_id')->unsigned()->index();
            $table->foreign('parameter_id')->references('id')->on('parameter')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_parameter');
    }
}
