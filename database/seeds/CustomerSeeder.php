<?php

use Illuminate\Database\Seeder;
use App\Customer;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {    	
        $cus = new Customer();
        $cus->id_customer = 1 ;
        $cus->name_customer = 'DINAS LINGKUNGAN HIDUP PROVINSI JAWA TIMUR' ;
        $cus->phone = '(031) 8541807' ;
        $cus->fax = '(031) 8530482' ;
        $cus->address = 'Jl. Wisata Menanggal 38' ;
        $cus->city = 'Surabaya' ;
        $cus->save();
    }
}
