<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {    
        //1	
        $front_office = new Role();
        $front_office->name = 'Front_Office';
        $front_office->save();
        //2
        $analyst = new Role();
        $analyst->name = 'Analyst';
        $analyst->save();
        //3
        $supervisor = new Role();
        $supervisor->name = 'Supervisor';
        $supervisor->save();
        //4
        $technical_manager = new Role();
        $technical_manager->name = 'Technical_manager';
        $technical_manager->save();
        //5
        $head_of_unit = new Role();
        $head_of_unit->name = 'Head_of_unit';
        $head_of_unit->save();
        //6
        $kasi_pt = new Role();
        $kasi_pt->name = 'Section_head';
        $kasi_pt->save();
        //7
        $viewer = new Role();
        $viewer->name = 'Viewer';
        $viewer->save();
        //8
        $admin = new Role();
        $admin->name = 'Administrator';
        $admin->save();

        // Membuat sample front
        $admin = new User();
        $admin->name = 'Front';
        $admin->email = 'front@dlh.com';
        $admin->password = bcrypt('qweasd');
        $admin->save();
        $admin->attachRole($front_office);

        // Membuat sample analis
        $member = new User();
        $member->name = "Analyst";
        $member->email = 'analyst@dlh.com';
        $member->password = bcrypt('qweasd');
        $member->save();
        $member->attachRole($analyst);

        // Membuat sample penyelia
        $member = new User();
        $member->name = "Supervisor";
        $member->email = 'supervisor@dlh.com';
        $member->password = bcrypt('qweasd');
        $member->save();
        $member->attachRole($supervisor);

        // Membuat sample tm
        $member = new User();
        $member->name = "Technical_manager";
        $member->email = 'technical_manager@dlh.com';
        $member->password = bcrypt('qweasd');
        $member->save();
        $member->attachRole($technical_manager);

        // Membuat sample admin
        $member = new User();
        $member->name = "Administrator";
        $member->email = 'admin@dlh.com';
        $member->password = bcrypt('qweasd');
        $member->save();
        $member->attachRole($admin);

        // Membuat sample head
        $member = new User();
        $member->name = "Head of unit";
        $member->email = 'head@dlh.com';
        $member->password = bcrypt('qweasd');
        $member->save();
        $member->attachRole($head_of_unit);

         // Membuat sample kasi
        $member = new User();
        $member->name = "Section Head";
        $member->email = 'section@dlh.com';
        $member->password = bcrypt('qweasd');
        $member->save();
        $member->attachRole($kasi_pt);

         // Membuat sample viewer
        $member = new User();
        $member->name = "Viewer 1";
        $member->email = 'viewer@dlh.com';
        $member->password = bcrypt('qweasd');
        $member->save();
        $member->attachRole($viewer);
    }
}
