<?php

use Illuminate\Database\Seeder;
use App\Parameter;

class ParameterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {    	
        $cus = new Parameter();
        $cus->id_customer = 1 ;
        $cus->name = 'BOD' ;
        $cus->id_procedure = 1 ;
        $cus->MDL = '20' ;
        $cus->price = 20000 ;
        $cus->accreditation = 1 ;
        $cus->standard = 'ml' ;
        $cus->save();

        $cus2 = new Parameter();
        $cus2->id_customer = 2 ;
        $cus2->name = 'COD' ;
        $cus2->id_procedure = 1 ;
        $cus2->MDL = '20' ;
        $cus2->price = 30000 ;
        $cus2->accreditation = 1 ;
        $cus2->standard = 'ml' ;
        $cus2->save();

        $cus3 = new Parameter();
        $cus3->id_customer = 3 ;
        $cus3->name = 'pH' ;
        $cus3->id_procedure = 2 ;
        $cus3->MDL = '20' ;
        $cus3->price = 30000 ;
        $cus3->accreditation = 1 ;
        $cus3->standard = 'ml' ;
        $cus3->save();
    }
}
