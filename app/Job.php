<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 'job';

    protected $fillable = [
        'status', 'customer_id', 
    ];

    public function customer() {
        return $this->belongsTo('App\Customer', 'customer_id');
    }

     public function samples() {
        return $this->hasMany('App\Sample');
    }
}
