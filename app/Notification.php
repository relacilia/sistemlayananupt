<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notification';

    protected $fillable = [
        'source_id', 'destination_id', 'link'
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function parameter() {
        return $this->belongsTo('App\Parameter', 'parameter_id');
    }
}
