<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'log';

    protected $fillable = [
        'user_id', 'info', 'table_name'
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
