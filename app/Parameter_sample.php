<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter_sample extends Model
{
    protected $table = 'parameter_sample';

    protected $fillable = [
        'sample_id', 'parameter_id', 'user_id', 'status_parameter'
    ];

    public function parameter() {
        return $this->belongsTo('App\Parameter', 'parameter_id');
    }

    public function sample()
    {
        return $this->belongsTo('App\Sample', 'sample_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function results() {
        return $this->hasMany('App\Result', 'para_sample_id');
    }
}
