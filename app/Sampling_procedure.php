<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sampling_procedure extends Model
{

	protected $table = 'sampling_procedure';

    protected $fillable = [
        'name_procedure',
    ];

    public function matrix() {
        return $this->belongsTo('App\sample_matrix', 'matrix_id');
    }
}
