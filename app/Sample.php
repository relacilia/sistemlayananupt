<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sample extends Model
{
    protected $table = 'sample';

    protected $fillable = [
        'name', 'price', 'kode_sample', 'ref_test_id', 'job_id', 'procedure_id', 'matrix_id', 'date_sampled', 'time_sampled', 'status', 'price', 'preservative', 'coordinat_S', 'coordinat_E'
    ];

    public function matrix() {
        return $this->belongsTo('App\sample_matrix', 'matrix_id');
    }

    public function job() {
        return $this->belongsTo('App\Job');
    }

    public function ref() {
        return $this->belongsTo('App\Ref_test', 'ref_test_id');
    }

    public function procedure() {
        return $this->belongsTo('App\Sampling_procedure', 'procedure_id');
    }

    public function parameter_samples() {
        return $this->hasMany('App\Parameter_sample', 'sample_id');
    }


}
