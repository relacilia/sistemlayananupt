<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ref_test extends Model
{
    protected $table = 'ref_test';

    protected $fillable = [
        'name'
    ];

    public function ref_parameter() {
        return $this->hasMany('App\Ref_param', 'ref_test_id');
    }
}
