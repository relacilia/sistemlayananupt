<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ref_param extends Model
{
    protected $table = 'ref_parameter';

    protected $fillable = [
        'ref_test_id', 'parameter_id', 'sign','dosage_maksimum'
    ];

    public function parameter() {
        return $this->belongsTo('App\Parameter', 'parameter_id');
    }

    public function ref_test()
    {
        return $this->belongsTo('App\Ref_test','ref_test_id');
    }
}
