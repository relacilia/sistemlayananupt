<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hierarchy_ref_test extends Model
{
    protected $table = 'hierarchy_ref_test';

    protected $fillable = [
        'id_parent', 'id_child'
    ];

    public function ref_test() {
        return $this->belongsTo('App\Ref_test', 'id_ref_test');
    }
}
