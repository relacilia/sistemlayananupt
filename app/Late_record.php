<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Late_record extends Model
{
    protected $table = 'late_record';

    protected $fillable = [
        'user_id', 'duration', 'date_input'
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
