<?php

namespace App\Console;

use app\Job;
use app\Job_late_record;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $job = Job::get();
            $now = Carbon::now()->toDateString(); 
            foreach ($job as $key => $value) {
                if($value->interval <= $now && $value->status != 'OK'){
                    $late = new Job_late_record;
                    $late->job_id = $value->id;
                    $late->save();
                }
            }
        })->dailyAt('06:44');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
