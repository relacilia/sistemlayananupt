<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (Auth::user()->hasRole('Front_Office'))
              return redirect('/front/home');
            else if (Auth::user()->hasRole('Analyst'))
              return redirect('/analyst/home');
            else if (Auth::user()->hasRole('Supervisor'))
              return redirect('/supervisor/home');
            else if (Auth::user()->hasRole('Technical_manager'))
              return redirect('/tm/home');
            else if (Auth::user()->hasRole('Administrator'))
              return redirect('/admin/home');
            else if (Auth::user()->hasRole('Head_of_unit'))
              return redirect('/head/home');
            else if (Auth::user()->hasRole('Section_head'))
              return redirect('/kasi/home');
        }

        return $next($request);
    }
}
