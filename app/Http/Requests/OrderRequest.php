<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $name = $this->request->get('cus_sample');
        foreach($name as $key => $val)
        {
            $rules['time.'.$key] = 'date_format:H:i';
        }

        return $rules;
    }
}

        //     // 's1.'.$key => 'regex:/^-?\s?[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
        //     // 's2[$key]' => 'regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
        //     // 's3[$key]' => 'regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
        //     // 'e1[$key]' => 'regex:/^-?\s?[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
        //     // 'e2[$key]' => 'regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
        //     // 'e3[$key]' => 'regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
        //     // 'ph[$key]' => 'regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
        //     // 'suhu[$key]' => 'regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
        //     // 'dlh[$key]' => 'regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
        //     // 'do[$key]' => 'regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
        //     // 'sal[$key]' => 'regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/'