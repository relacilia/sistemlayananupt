<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sample;
use App\Job;
use App\Log;
use App\Notification;
use App\Late_record;
use App\Result;
use App\Parameter;
use App\Assignment;
use App\Parameter_sample;
use App\Extra;
use Auth;
use DB;
use Carbon\Carbon;

class AnalystController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $param;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $notif = Notification::where('destination_id','=',Auth::user()->id)->get(); 

        $param = Assignment::where('user_id','=',Auth::user()->id)->orderBy('parameter_id', 'asc')->get();
        $count = $param->count();
        $param_sample = Parameter_sample::where('status_parameter','=','Onprogress (AN)')
                                    ->Orwhere('status_parameter','=','Revision (SV)')
                                    ->Orwhere('status_parameter','=','Revision (TM)')
                                    ->get();
        $parameter = $param_sample->where('parameter_id',$param[0]->parameter_id)
                            ->where('user_id','=',Auth::user()->id);

        $collection= collect();
        foreach ($parameter as $key => $value) {
            $data[$key]= DB::table('Sample')
            ->join('Parameter_sample', 'Parameter_sample.sample_id', '=', 'Sample.id')
            ->join('Job', 'Job.id', '=', 'Sample.job_id')
            ->select('Sample.*', 'Job.date_recent')
            ->distinct()
            ->where('Sample.id','=',$value->sample_id)
            ->groupBy('Job.date_recent')
            ->get();
            $collection = $collection->merge($data[$key]);
        }

        foreach ($param as $key => $value) {
            $value->parameter->name;
        }

        $byDate = $collection->unique('date_recent');
        $temp = array();
        foreach ($byDate as $key => $value) {
            $temp[$key] = $collection->where('date_recent',$value->date_recent)->count();
        }

        return view('analyst.joblist')->with([
            'param' => $param,
            'parameter' => $byDate,
            'total' => $temp,
            'menu_active' => $param[0]->parameter,
            'notif' => $notif
        ]);
    }

    public function sample_list($id, $date)
    {
        $notif = Notification::where('destination_id','=',Auth::user()->id)->get();

        $param = Assignment::where('user_id','=',Auth::user()->id)->orderBy('parameter_id', 'asc')->get();
        $menu_active = Parameter::find($id);
        $param_sample = Parameter_sample::where('status_parameter','=','Onprogress (AN)')
                                    ->Orwhere('status_parameter','=','Revision (SV)')
                                    ->Orwhere('status_parameter','=','Revision (TM)')
                                    ->get();
        $parameter = $param_sample->where('parameter_id',$id)
                            ->where('user_id','=',Auth::user()->id);
        $result=array();
        $collection= collect();

        foreach ($parameter as $key => $value) {
            $data[$key]= DB::table('Sample')
            ->join('Parameter_sample', 'Parameter_sample.sample_id', '=', 'Sample.id')
            ->join('Job', 'Job.id', '=', 'Sample.job_id')
            ->select('Sample.*', 'Job.date_recent', 'Parameter_sample.status_parameter', 'Parameter_sample.id')
            ->distinct()
            ->where('Parameter_sample.id','=',$value->id)
            ->where('Job.date_recent','=',$date)
            ->groupBy('Job.date_recent')
            ->get();

            $collection = $collection->merge($data[$key]);
        }

        foreach ($collection as $key => $value) {
            $result[$key] = Result::where('para_sample_id','=', $value->id)->get();
        }

        $count2 = $parameter->count();

        return view('analyst.samplelist')->with([
            'param' => $param,
            'parameter' => $collection,
            'count' => $count2,
            'menu_active' => $menu_active,
            'result' => $result,
            'notif' => $notif
        ]);
    }

    public function get_sample($id)
    {
        $notif = Notification::where('destination_id','=',Auth::user()->id)->get();

        $param = Assignment::where('user_id','=',Auth::user()->id)->orderBy('parameter_id', 'asc')->get();

        $menu_active = Parameter::find($id);
        $param_sample = Parameter_sample::where('status_parameter','=','Onprogress (AN)')
                                    ->Orwhere('status_parameter','=','Revision (SV)')
                                    ->Orwhere('status_parameter','=','Revision (TM)')
                                    ->get();
        $parameter = $param_sample->where('parameter_id',$id)
                            ->where('user_id','=',Auth::user()->id);

        $collection= collect();

        foreach ($parameter as $key => $value) {
            $data[$key]= $value->sample->job;
            $collection = $collection->prepend($data[$key]);
        }

        $byDate = $collection->unique('date_recent');

        $temp = array();
        foreach ($byDate as $key => $value) {
            $temp[$key] = $collection->where('date_recent',$value->date_recent)->count();
        }

        $count2 = $parameter->count();

        return view('analyst.joblist')->with([
            'param' => $param,
            'parameter' => $byDate,
            'total' => $temp,
            'count' => $count2,
            'menu_active' => $menu_active,
            'notif' => $notif
        ]);
    }

    public function number_of_working_days($from, $to) {
        $workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
        $holidayDays = ['*-12-25', '*-01-01', '2013-12-23']; # variable and fixed holidays

        $from = new \DateTime($from);
        $to = new \DateTime($to);
        $to->modify('+1 day');
        $interval = new \DateInterval('P1D');
        $periods = new \DatePeriod($from, $interval, $to);

        $days = 0;
        foreach ($periods as $period) {
            if (!in_array($period->format('N'), $workingDays)) continue;
            if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
            if (in_array($period->format('*-m-d'), $holidayDays)) continue;
            $days++;
        }
        return $days;
    }

    public function add_result($id, Request $request){
        $param      = Parameter_sample::find($id);
        $date       = $param->sample->job->date_recent;
        $current    = Carbon::now()->format('Y-m-d');
        $analyzing  = Extra::pluck('analyzing')->first();
        $total      = $this->number_of_working_days($date, $current);

        if($total > $analyzing){
            $late               = new Late_record;
            $late->user_id      = \Auth::user()->id;
            $late->date_input   = Carbon::now()->toDateTimeString();
            $late->duration     = $total;
            $late->save();

            $log = new Log;
            $log->info = "INSERT ID ".$late->id;
            $log->table_name = "Late_record";
            $log->user_id = Auth::user()->id;
            $log->save();
        }

        Notification::where('para_sample_id','=',$id)->delete();
        $result = new Result;
        $result->para_sample_id = $id;
        $result->value = $request->input('result');
        $result->status_result = 'waiting';
        $result->save();

        $log2 = new Log;
        $log2->info = "INSERT ID ".$result->id;
        $log2->table_name = "RESULT";
        $log2->user_id = Auth::user()->id;
        $log2->save();

        $sample = Parameter_sample::where('id','=',$id)
                ->update(['status_parameter' => 'Onprogress (SV)']);

        $log3 = new Log;
        $log3->info = "UPDATE ID ".$id;
        $log3->table_name = "Parameter_sample";
        $log3->user_id = Auth::user()->id;
        $log3->save();

        $pesanAlert = 'Hasil analisis berhasil ditambahkan!';
        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert],
        ]);
    }
}
