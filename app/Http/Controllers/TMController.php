<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sample;
use App\Job;
use App\Log;
use App\Result;
use App\Parameter;
use App\Assignment;
use App\Customer;
use App\Parameter_sample;
use Auth;
use DB;
use Carbon\Carbon;

class TMController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $job = Job::where('status','=','Onprogress (TM)')->where('nonaktif','=',0)->get();

        return view('TM.home')->with([
            'job' => $job,
        ]);
    }

    public function samplelist($id)
    {
        $sample = Sample::where('job_id','=',$id)->where('nonaktif','=',0)->get();
        $job = Job::find($id);
        foreach ($sample as $key => $value) {
             $value->matrix->name;
        }

        return view('TM.samplelist')->with([
            'sample' => $sample,
            'job'   => $job
        ]);
    }

     public function detail_sample($id, $kode)
    {
        $job        = Job::find($id);
        $sample     = Sample::find($kode);
        $param      = Parameter_sample::where('sample_id','=',$kode)->get();
        $hasil[]    = null;
        $job_history = array();

        $jobs    = Job::take(2)->where('customer_id','=', $job->customer_id)
                        ->where('status','=','OK')                               
                        ->get();
        
        foreach ($jobs as $key => $value) {
            $matrix = $value->samples->where('matrix_id','=',$sample->matrix_id)->first();
            if($matrix != null){
                $job_history[$key] = $value;
            }
        }
        
        foreach ($param as $key => $value) {
            $value->parameter->name;
            $value->user->name;
            $hasil[$key] = Result::where('para_sample_id','=', $value->id)->get();
        }

        return view('TM.sample_detail')->with([
            'param'         => $param,
            'job'           => $job,
            'cur_sample'    => $sample,
            'result'        => $hasil,
            'jobs'          => $job_history,
        ]);
    }

    public function acceptation($id)
    {
        $param = Parameter_sample::find($id);
        $paras = Parameter_sample::where('id','=',$id)->update(['status_parameter' => 'OK (TM)']);
        $log = new Log;
        $log->info = "UPDATE ID ".$id;
        $log->table_name = "Parameter_sample";
        $log->user_id = Auth::user()->id;
        $log->save();

        $result = Result::where('para_sample_id','=', $id)->orderBy('created_at','desc')->first();
        $result->status_result = 'OK (TM)';
        $result->save();
        $log2 = new Log;
        $log2->info = "UPDATE ID ".$result->id;
        $log2->table_name = "Result";
        $log2->user_id = Auth::user()->id;
        $log2->save();

        $pesanAlert = $param->parameter->name." diterima";
        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert],
        ]);
    }

    public function revision($id)
    {
        $param = Parameter_sample::find($id);
        $paras = Parameter_sample::where('id','=',$id)->update(['status_parameter' => 'Revision (TM)']);
        $log = new Log;
        $log->info = "UPDATE ID ".$id;
        $log->table_name = "Parameter_sample";
        $log->user_id = Auth::user()->id;
        $log->save();

        $result = Result::where('para_sample_id','=', $id)->orderBy('created_at','desc')->first();
        $result->status_result = 'Revision (TM)';
        $result->save();
        $log2 = new Log;
        $log2->info = "UPDATE ID ".$result->id;
        $log2->table_name = "Result";
        $log2->user_id = Auth::user()->id;
        $log2->save();
        
        $pesanAlert = $param->parameter->name." ditolak";
        return back()->with([
            'alert'         => [    'type'      => 'danger', 
                                    'message'   => $pesanAlert],
        ]);
    }
}
