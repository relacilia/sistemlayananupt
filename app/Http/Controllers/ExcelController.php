<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Customer;
use App\Job;
use App\Extra;
use App\Log;
use App\Late_record;
use App\Parameter_sample;
use App\sample_matrix;
use App\Sample;
use App\Result;
use Auth;
use DB;
use Carbon\Carbon;

class ExcelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function downloadExcel($data)
    {
        Excel::load('file.xlsx', function($excel) use ($data) {

             $excel->sheet('Sheet1', function ($sheet) use ($data) {
                $job = Job::where('id','=',$data)->first();
                
                $sheet->cell('B11', function($cell) use ($job) {
                    $cell->setValue(': '.$job->id);
                });
                $cus = Customer::where('id_customer','=',$job->customer_id)->first();
                $sheet->cell('B12', function($cell) use ($cus) {
                    $cell->setValue(': '.$cus->name_customer);
                });
                $sheet->cell('B13', function($cell) use ($job) {
                    $cell->setValue(': '.$job->attention);
                });
                $sheet->cell('B14', function($cell) use ($job) {
                    $cell->setValue(': '.$job->sampled_by);
                });
                $sheet->cell('G12', function($cell) use ($job) {
                    $cell->setValue(': '.$job->date_recent);
                });
                $sheet->cell('G13', function($cell) use ($job) {
                    $cell->setValue(': '.$job->time_recent);
                });
                $sheet->cell('G14', function($cell) use ($job) {
                    $cell->setValue(': '.$job->interval);
                });

                $sample = Sample::where('job_id','=',$data)->where('nonaktif','=',0)->get();
                $count = $sample->count();

                // $sample = array_values(array_sort($sample, function ($value) {
                //     return $value['id_job'];
                // }));


                foreach ($sample as $key => $value) {
                    $sheet->cells('A'.($key+18), function($cell) use ($value) {
                         $cell->setValue($value['kode_sample']);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('B'.($key+18), function($cell) use ($value) {
                         $cell->setValue($value['name']);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $matrix = sample_matrix::where('id','=',$value['matrix_id'])->first();
                    $sheet->cells('C'.($key+18), function($cell) use ($matrix) {
                         $cell->setValue($matrix->alias);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('D'.($key+18), function($cell) use ($value) {
                         $cell->setValue($value['date_sampled']);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('E'.($key+18), function($cell) use ($value) {
                         $cell->setValue($value['time_sampled']);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('F'.($key+18), function($cell) use ($value) {
                         $cell->setValue($value['coordinat_S']);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('G'.($key+18), function($cell) use ($value) {
                         $cell->setValue($value['coordinat_E']);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                };

                $sheet->cell('A'.(18+$count+2), function($cell){
                    $cell->setValue('Pengirim');
                });

                $sheet->cell('A'.(18+$count+6), function($cell) use ($job) {
                    $cell->setValue($job->sampled_by);
                });

                $sheet->cell('G'.(18+$count+2), function($cell){
                    $cell->setValue('Penerima');
                });

                $sheet->cell('G'.(18+$count+6), function($cell) use ($job) {
                    $cell->setValue(Auth::user()->name);
                });

             });

        })->setFilename('job'.$data)->export('xlsx');

         return Redirect::back();

    }

    public function downloadPdf($data)
    {
        Excel::load('file.xlsx', function($excel) use ($data) {

             $excel->sheet('Sheet1', function ($sheet) use ($data) {
                $job = Job::where('id','=',$data)->first();
                
                $sheet->cell('B11', function($cell) use ($job) {
                    $cell->setValue(': '.$job->id);
                });
                $cus = Customer::where('id_customer','=',$job->customer_id)->first();
                $sheet->cell('B12', function($cell) use ($cus) {
                    $cell->setValue(': '.$cus->name_customer);
                });
                $sheet->cell('B13', function($cell) use ($job) {
                    $cell->setValue(': '.$job->attention);
                });
                $sheet->cell('B14', function($cell) use ($job) {
                    $cell->setValue(': '.$job->sampled_by);
                });
                $sheet->cell('G12', function($cell) use ($job) {
                    $cell->setValue(': '.$job->date_recent);
                });
                $sheet->cell('G13', function($cell) use ($job) {
                    $cell->setValue(': '.$job->time_recent);
                });
                $sheet->cell('G14', function($cell) use ($job) {
                    $cell->setValue(': '.$job->interval);
                });

                $sample = Sample::where('job_id','=',$data)->where('nonaktif','=',0)->get();
                $count = $sample->count();

                // $sample = array_values(array_sort($sample, function ($value) {
                //     return $value['id_job'];
                // }));


                foreach ($sample as $key => $value) {
                    $sheet->cells('A'.($key+18), function($cell) use ($value) {
                         $cell->setValue($value['kode_sample']);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('B'.($key+18), function($cell) use ($value) {
                         $cell->setValue($value['name']);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $matrix = sample_matrix::where('id','=',$value['matrix_id'])->first();
                    $sheet->cells('C'.($key+18), function($cell) use ($matrix) {
                         $cell->setValue($matrix->alias);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('D'.($key+18), function($cell) use ($value) {
                         $cell->setValue($value['date_sampled']);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('E'.($key+18), function($cell) use ($value) {
                         $cell->setValue($value['time_sampled']);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('F'.($key+18), function($cell) use ($value) {
                         $cell->setValue($value['coordinat_S']);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('G'.($key+18), function($cell) use ($value) {
                         $cell->setValue($value['coordinat_E']);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                };

                $sheet->cell('A'.(18+$count+2), function($cell){
                    $cell->setValue('Pengirim');
                });

                $sheet->cell('A'.(18+$count+6), function($cell) use ($job) {
                    $cell->setValue($job->sampled_by);
                });

                $sheet->cell('G'.(18+$count+2), function($cell){
                    $cell->setValue('Penerima');
                });

                $sheet->cell('G'.(18+$count+6), function($cell) use ($job) {
                    $cell->setValue(Auth::user()->name);
                });

             });

        })->setFilename('job'.$data)->export('pdf');

         return Redirect::back();

    }

    public function downloadExcelAnalis($id, $data)
    {
        Excel::load('document\analisis.xlsx', function($excel) use ($id, $data) {

             $excel->sheet('Sheet1', function ($sheet) use ($id, $data) {
            $param_sample = Parameter_sample::where('status_parameter','=','Onprogress (AN)')
                                    ->Orwhere('status_parameter','=','Revision (SV)')
                                    ->Orwhere('status_parameter','=','Revision (TM)')
                                    ->get();
            $parameter = $param_sample->where('parameter_id',$id)
                            ->where('user_id','=',Auth::user()->id);
                                                
                $collection= collect();
                    foreach ($parameter as $key => $value) {
                        $temp[$key]= DB::table('Sample')
                        ->join('Parameter_sample', 'Parameter_sample.sample_id', '=', 'Sample.id')
                        ->join('Job', 'Job.id', '=', 'Sample.job_id')
                        ->select('Sample.*', 'Job.date_recent', 'Parameter_sample.id')
                        ->distinct()
                        ->where('Parameter_sample.id','=', $value->id)
                        ->where('Job.date_recent','=', $data)
                        ->where('Sample.status','!=', 'Onprogress (SV)')
                        ->where('Sample.status','!=', 'OK')
                        ->groupBy('Job.date_recent')
                        ->get();
                        $collection = $collection->merge($temp[$key]);
                    }

                // $sheet->cell('C7', function($cell) use ($collection) {
                //     $cell->setValue(': '.$collection[0]->date_recent);
                // });

                foreach ($collection as $key => $value) {
                    $sheet->cells('A'.($key+2), function($cell) use ($key) {
                         $cell->setValue($key+1);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('B'.($key+2), function($cell) use ($value) {
                         $cell->setValue($value->id);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('C'.($key+2), function($cell) use ($value) {
                         $cell->setValue($value->kode_sample);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('D'.($key+2), function($cell) use ($value) {
                         $cell->setValue($value->name);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('E'.($key+2), function($cell) use ($value) {
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                };
             });

        })->setFilename('analis_'.Auth::user()->id.'_'.$data)->export('xlsx');
         return Redirect::back();
    }

    public function number_of_working_days($from, $to) {
        $workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
        $holidayDays = ['*-12-25', '*-01-01', '2013-12-23']; # variable and fixed holidays

        $from = new \DateTime($from);
        $to = new \DateTime($to);
        $to->modify('+1 day');
        $interval = new \DateInterval('P1D');
        $periods = new \DatePeriod($from, $interval, $to);

        $days = 0;
        foreach ($periods as $period) {
            if (!in_array($period->format('N'), $workingDays)) continue;
            if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
            if (in_array($period->format('*-m-d'), $holidayDays)) continue;
            $days++;
        }
        return $days;
    }

    public function get_file(Request $request)
    {
        ini_set('max_execution_time', 300);
        if($request->file('result')){
            $path = $request->file('result')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count() ){
                $count = 0;
                foreach ($data as $key => $value) {
                    if($value->psid != null){
                       $result = new Result;
                       $result->para_sample_id = $value->psid;
                       $result->value = $value->hasil;
                       $result->status_result = 'waiting';
                       $result->save();

                       $log2 = new Log;
                        $log2->info = "INSERT ID ".$result->id;
                        $log2->table_name = "RESULT";
                        $log2->user_id = Auth::user()->id;
                        $log2->save();

                        $param      = Parameter_sample::find($value->psid);
                        $date       = $param->sample->job->date_recent;
                        $current    = Carbon::now()->format('Y-m-d');
                        $analyzing  = Extra::pluck('analyzing')->first();
                        $total      = $this->number_of_working_days($date, $current);

                        if($total > $analyzing){
                            $late               = new Late_record;
                            $late->user_id      = \Auth::user()->id;
                            $late->date_input   = Carbon::now()->toDateTimeString();
                            $late->duration     = $total;
                            $late->save();

                            $log = new Log;
                            $log->info = "INSERT ID ".$late->id;
                            $log->table_name = "Late_record";
                            $log->user_id = Auth::user()->id;
                            $log->save();
                        }


                       $sample = Parameter_sample::where('id','=',$value->psid)
                        ->update(['status_parameter' => 'Onprogress (SV)']);
                        $log3 = new Log;
                        $log3->info = "UPDATE ID ".$value->psid;
                        $log3->table_name = "Parameter_sample";
                        $log3->user_id = Auth::user()->id;
                        $log3->save();
                       if($value->hasil != null)
                       {
                            $count++;
                       }
                    }
                }
                $pesanAlert = 'Hasil analisis berhasil ditambahkan';
                $type = 'success';
            } else {
                $pesanAlert = 'Hasil analisis gagal ditambahkan';
                $type = 'danger';
            }
        }
        return back()->with([
            'alert'         => [    'type'      => $type, 
                                    'message'   => $pesanAlert],
            ]);
    }

    public function sertif()
    {
        //
        $test = Ref_param::pluck('dosage_maksimum');
        $tt = $test[10];
        $aa = substr($tt, 0,1);
        if($aa == '<' || $aa == '>' || $aa == '±' || $aa == '≤' || $aa == '≥'){
            dd("meh");
        }
        //
    }

    function romanic_number($integer, $upcase = true) 
    { 
        $table = array('X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1); 
        $return = ''; 
        while($integer > 0) 
        { 
            foreach($table as $rom=>$arb) 
            { 
                if($integer >= $arb) 
                { 
                    $integer -= $arb; 
                    $return .= $rom; 
                    break; 
                } 
            } 
        } 

        return $return; 
    } 

    public function downloadSertifikat ($data)
    {   
        Excel::load('document\sertifikat.xlsx', function($excel) use ($data) {

             $excel->sheet('Sheet1', function ($sheet) use ($data) {
                 $sample = Sample::where('id','=',$data)->first();
                 $date = $sample->job->date_recent;
                 $date = new Carbon($date);
                 if($sample->job->customer_id == 1){
                     $sheet->cell('A2', function($cell) use ($sample,$date) {
                        $cell->setValue('NO: 660 / '.$sample->job->id.' / 207.5 /');
                     });
                    $sheet->cell('F5', function($cell) use ($sample,$date) {
                        $rom = $this->romanic_number($date->month);
                        $cell->setValue($sample->matrix->alias.'/'.$rom.'/'.$date->year.'/'.$sample->kode_sample.'-DLH');
                     });
                 }
                 else{
                     $sheet->cell('A2', function($cell) use ($sample,$date) {
                        $cell->setValue('NO: 660 / '.$sample->job->id.' / 207.5 / '.$date->year);
                     });
                     $sheet->cell('F5', function($cell) use ($sample,$date) {
                        $rom = $this->romanic_number($date->month);
                        $cell->setValue($sample->matrix->alias.'/'.$rom.'/'.$date->year.'/'.$sample->kode_sample);
                     });
                }
                $param = Parameter_sample::where('sample_id','=',$data)->get();
                foreach ($param as $key => $value) {
                    $sheet->cells('A'.($key+23), function($cell) use ($key) {
                         $cell->setValue($key+1);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('B'.($key+23), function($cell) use ($value) {
                         $cell->setValue($value->parameter->name);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('D'.($key+23), function($cell) use ($value) {
                         $cell->setValue($value->parameter->name);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('E'.($key+23), function($cell) use ($value) {
                         $cell->setValue($value->parameter->name);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('G'.($key+23), function($cell) use ($value) {
                         $cell->setValue($value->parameter->name);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('H'.($key+23), function($cell) use ($value) {
                         $cell->setValue($value->parameter->name);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('I'.($key+23), function($cell) use ($value) {
                         $cell->setValue($value->parameter->name);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cells('J'.($key+23), function($cell) use ($value) {
                         $cell->setValue($value->parameter->name);
                         $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                }
             });

        })->setFilename('sertifikat'.$data)->export('xlsx');

         return Redirect::back();
    }
}
