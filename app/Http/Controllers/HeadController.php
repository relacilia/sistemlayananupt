<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Job;
use App\Sample;
use App\Ref_param;


class HeadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current = Carbon::now();
        $current_date  = $current->toDateString();
        $day[6] = $current_date;
        $total[6]  =  Job::where('date_recent','like',$current_date.'%')->sum('price');
        $job[6]  =  Job::where('date_recent','like',$current_date.'%')->count();
        $sample[6] = Sample::whereHas('job', function ($query) use($current_date) {
                $query->where('date_recent', 'like', $current_date);
            })->count();

        for($i=5 ; $i>=0; $i--)
        {
            $day[$i] = $current->subDays(1)->toDateString();
            $total[$i] = Job::where('date_recent','like',$day[$i].'%')->sum('price');
            $job[$i]   = Job::where('date_recent','like',$day[$i].'%')->count();
            $deday = $day[$i];
            $sample[$i] = Sample::whereHas('job', function ($query) use($deday) {
                $query->where('date_recent', 'like', $deday);
            })->count();
        }

        return view('head.daily_graph')->with([
            'price' => $total,
            'date' => $day,
            'job'   => $job,
            'sample' => $sample,
            'menu_active' => 'Harian'
        ]);
    }

    public function daily(Request $request)
    {
        $month      = $request->input('month');
        if (strlen($month) == 1){
            $month = sprintf("%02d", $month);
        }
        $year       = $request->input('year');
        $current    = $year.'-'.$month;

        $total  = array();
        $job    = array();
        $sample = array();
        $date = Job::where('date_recent','like',$current.'%')->distinct('date_recent')->pluck('date_recent')->sort();
        foreach ($date as $key => $value) {
            $total[$key]    = Job::where('date_recent','like',$value)->sum('price');
            $job[$key]      = Job::where('date_recent','like',$value)->count();
            $sample[$key]   =  Sample::whereHas('job', function ($query) use($value) {
                                    $query->where('date_recent', 'like', $value);
            })->count();
        }

        $months = [
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December'
        ];
        
        return view('head.daily')->with([
            'price' => $total,
            'date'  => $date,
            'job'   => $job,
            'sample'=> $sample,
            'month' => $months,
            'curr_month' => $month,
            'menu_active' => 'Harian'
        ]);
    }

    public function monthly_graph($year)
    {
        $month = array(
            '01','02','03','04','05','06','07','08','09','10','11','12
        ');

        for($i=0 ; $i<12; $i++)
        {
            $total[$i] = Job::where('date_recent','like',$year.'-'.$month[$i].'%')->sum('price');
            $job[$i]   = Job::where('date_recent','like',$year.'-'.$month[$i].'%')->count();
            $deday = $year.'-'.$month[$i].'%';
            $sample[$i] = Sample::whereHas('job', function ($query) use($deday) {
                $query->where('date_recent', 'like', $deday);
            })->count();
        }

        return view('head.monthly_graph')->with([
            'year'      => $year,
            'price'     => $total,
            'job'       => $job,
            'sample'    => $sample
        ]);
    }

    public function monthly(Request $request)
    {
        $year       = $request->input('year');
        $month      = array('01','02','03','04','05','06','07','08','09','10','11','12');

        for($i=0 ; $i<12; $i++)
        {
            $total[$i] = Job::where('date_recent','like',$year.'-'.$month[$i].'%')->sum('price');
            $job[$i]   = Job::where('date_recent','like',$year.'-'.$month[$i].'%')->count();
            $deday = $year.'-'.$month[$i].'%';
            $sample[$i] = Sample::whereHas('job', function ($query) use($deday) {
                $query->where('date_recent', 'like', $deday);
            })->count();
        }

        return view('head.monthly')->with([
            'month'      => $month,
            'year'      => $year,
            'price'     => $total,
            'job'       => $job,
            'sample'    => $sample,
            'menu_active' => 'Bulanan'
        ]);
    }

    public function yearly_graph()
    {
        $current = Carbon::now();
        $current_year  = $current->year;
        $year[4] = $current_year;
        $total[4]  =  Job::where('date_recent','like',$current_year.'%')->sum('price');
        $job[4]  =  Job::where('date_recent','like',$current_year.'%')->count();
        $sample[4] = Sample::whereHas('job', function ($query) use($current_year) {
                $query->where('date_recent', 'like', $current_year.'%');
            })->count();

        for($i=3 ; $i>=0; $i--)
        {
            $year[$i] = $current->subYears(1)->year;
            $total[$i] = Job::where('date_recent','like',$year[$i].'%')->sum('price');
            $job[$i]   = Job::where('date_recent','like',$year[$i].'%')->count();
            $deday = $year[$i];
            $sample[$i] = Sample::whereHas('job', function ($query) use($deday) {
                $query->where('date_recent', 'like', $deday.'%');
            })->count();
        }

        return view('head.yearly_graph')->with([
            'price' => $total,
            'date' => $year,
            'job'   => $job,
            'sample' => $sample,
            'menu_active' => 'Tahunan'
        ]);
    }

    public function yearly()
    {
        $current    = Carbon::now()->year;

        $total  = array();
        $job    = array();
        $sample = array();
        $date = Job::distinct('date_recent')->pluck('date_recent');

        foreach ($date as $key => $value) {
            $dt = new Carbon( $value );
            $year[$key] = $dt->year;
        }

        $years = array_unique($year);

        foreach ($years as $key => $value) {
            $total[$key]    = Job::whereYear('date_recent', '=', $value)->sum('price');
            $job[$key]      = Job::whereYear('date_recent', '=', $value)->count();
            $sample[$key]   =  Sample::whereHas('job', function ($query) use($value) {
                                    $query->whereYear('date_recent', '=', $value);
            })->count();
        }
       
        return view('head.yearly')->with([
            'price' => $total,
            'date'  => $years,
            'job'   => $job,
            'sample'=> $sample,
            'menu_active' => 'Tahunan'
        ]);
    }
}
