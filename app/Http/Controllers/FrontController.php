<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sampling_procedure;
use App\sample_matrix;
use App\Ref_test;
use App\Hierarchy_ref_test;
use App\Ref_param;
use App\Parameter;
use App\Customer;
use App\Job;
use App\Log;
use App\Result;
use App\Parameter_sample;
use App\Assignment;
use App\Sample;
use Auth;
use Response;
use DB;
use Carbon\Carbon;
use App\Http\Requests\OrderRequest;
use Illuminate\Database\Eloquent\Collection;
use Validator, Input, Redirect; 
use Exception;

class FrontController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customerID = Customer::pluck('id_customer', 'id_customer');
        $customer = Customer::pluck('name_customer', 'id_customer');
        $job = Job::pluck('id')->last();
        $id_job = $job+1;
        $dt = Carbon::now()->format('H:i');
        $tgl = Carbon::now()->toDateString(); 
        $itgl = Carbon::now()->addDays(6)->toDateString(); 
        
        return view('frontOffice.registrasi_1')->with([
            'customer' => $customer,
            'idcustomer' => $customerID,
            'job'   => $id_job,
            'dt'    => ([
                'jam' => $dt,
                'tgl'   => $tgl,
                'itgl'  => $itgl
            ]),
            'menu_active' => 'registrasi'
        ]);
    }

    public function registrasi_2(Request $request)
    {
        $sm= sample_matrix::where('nonaktif','=',0)->get();
        $this->validate($request, [
            'number_sample' => 'numeric',
            'datepicker' => 'date',
            'interval' => 'date'
        ]);
        $data = ([
            'id'    => $request->input('job_number'),
            'customer_id'   => $request->input('customer'),
            'sampled_by'    => $request->input('by'),
            'attention'     =>$request->input('attention'),
            'location'      =>$request->input('location'),
            'date_recent'   =>$request->input('datepicker'),
            'time_recent'   =>$request->input('time'),
            'number_samples'=>$request->input('number_sample'),
            'interval'      =>$request->input('interval'),
            'address'       =>$request->input('address'),
            'city'          =>$request->input('city'),
            'phone'         =>$request->input('phone'),
            'fax'           =>$request->input('fax')
        ]);

        for($i=0; $i<$data['number_samples'];$i++){
            $kode[$i] = $data['id'].'.'.($i+1);
        }

        return view('frontOffice.registrasi_2')->with([
            'kode' => $kode,
            'sm'  => $sm,
            'dt'    => $data,
            'menu_active' => 'registrasi'
        ]);
    }

    public function registrasi_3(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date.*' => 'date',
            's1.*' => 'nullable|regex:/^-?\s?[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
            's2.*' => 'nullable|regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
            's3.*' => 'nullable|regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
            'e1.*' => 'nullable|regex:/^-?\s?[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
            'e2.*' => 'nullable|regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
            'e3.*' => 'nullable|regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/'
        ])->validate();

        $name = $request->input('cus_sample');
        $time = $request->input('time'); 
        $data = $request->input('data');
        $kode_sample = $request->input('kode_sample');
        $date = $request->input('date');
        $matrix = $request->input('sample_matrix');
        $procedure = $request->input('procedure');
        $ref_test = $request->input('ref_test');
        $sub_ref = $request->input('industry_type');
        $s1 = $request->input('s1');
        $s2 = $request->input('s2');
        $s3 = $request->input('s3');
        $e1 = $request->input('e1');
        $e2 = $request->input('e2');
        $e3 = $request->input('e3');


        $param = array();
        foreach ($name as $key => $value) {
            $name_matrix = sample_matrix::where('id','=',$matrix[$key])->first();
            $name_ref = Ref_test::where('id','=',$ref_test[$key])->first();
            $name_procedure = Sampling_procedure::where('id','=',$procedure[$key])->first();
            $s[$key] = $s1[$key].'°'.$s2[$key]."'".$s3[$key].'"';
            $e[$key] = $e1[$key].'°'.$e2[$key]."'".$e3[$key].'"';

            $data_sample[$key] = ([
            'kode_sample' => $kode_sample[$key],
            'name' => $name[$key],
            'matrix_id' => $matrix[$key],
            'name_matrix' => $name_matrix->alias,
            'status' => 'onprogress',
            'price' => '0',
            'date_sampled' => $date[$key],
            'time_sampled' => $time[$key],
            'procedure_id'  => $procedure[$key],
            'ref_test_id'   => $ref_test[$key],
            'name_ref' => $name_ref->name,
            'name_procedure' => $name_procedure->name_procedure,
            'coordinat_S' => $s[$key],
            'coordinat_E' => $e[$key]
            ]);

            $param[$key] = Parameter::get();
            $ref_param[$key] = Ref_param::where('ref_test_id','=', $ref_test[$key])->get();

            foreach ($param[$key] as $val) {
                foreach ($ref_param[$key] as $val2) {
                    if($val->id == $val2->parameter_id){
                        $val->check = 1;
                    }
                }
            }
        }
        
        // $sorted = $collection->sortBy('kode_sample');
        // $sorted->values()->all();
        $mp= Sampling_procedure::get();
        $rt = Ref_test::get();
        $sm= sample_matrix::where('nonaktif','=',0)->pluck('name', 'id');

        return view('frontOffice.registrasi_3')->with([
            'mp' => $mp,
            'rt'    => $rt,
            'param'     => $param,
            'dt'    => $data,
            'sm'    => $sm,
            'dt_sample' => $data_sample,
            'ref_param' => $ref_param,
            'menu_active' => 'registrasi'
        ]);
    }

    public function review(Request $request){
        $validator = Validator::make($request->all(), [
            'ph.*'      => 'nullable|regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
            'dlh.*'     => 'nullable|regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
            'do.*'      => 'nullable|regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
            'sal.*'     => 'nullable|regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
            'suhu.*'    => 'nullable|regex:/^-?\s?[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
            'debit.*'   => 'nullable|regex:/^[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/',
            'kp.*'      => 'nullable|regex:/^-?\s?[0-9]{1,4}([,.]{1}[0-9]{1,4})?$/'
        ])->validate();

        $data = $request->input('data');
        $data_sample = $request->input('data_sample');
        $param_sampling = $request->input('param');
        $ph = $request->input('ph');
        $suhu = $request->input('suhu');
        $dhl = $request->input('dlh');
        $do = $request->input('do');
        $sal = $request->input('sal');
        $debit = $request->input('debit');
        $kp = $request->input('kp');
        $ket = $request->input('ket');

        $price_sample = array();
        $price_job = 0;
        foreach ($param_sampling as $keys => $val) {
            $harga = 0;
            $nama_param[$keys] = Parameter::find($val);
            foreach ($nama_param[$keys] as $valu) {
                $harga = $harga + $valu->price;
                $price_job = $price_job + $valu->price;   
            }
            $price_sample[$keys] = $harga;
        }

        $job_array = array_merge($data, ['price_job'=>$price_job]);

        foreach ($data_sample as $key => $value) {
            $data_sample2[$key] = ([
            'param_sampling' => $param_sampling[$key],
            'ph'  => $ph[$key],
            'suhu'  => $suhu[$key],
            'dhl'  => $dhl[$key],
            'do'  => $do[$key],
            'sal'  => $sal[$key],
            'debit'  => $debit[$key],
            'kp'  => $kp[$key],
            'ket'  => $ket[$key],
            'price_sample' => $price_sample[$key]
            ]);

            $sample_array[$key] = array_merge($value, $data_sample2[$key]);            
        }

        $sample_array = array_values(array_sort($sample_array, function ($value) {
            return $value['kode_sample'];
        }));

        $cus= Customer::find($data['customer_id']);

        $pesanAlert = "Berhasil";

        return view('frontOffice.review')->with([
            'data' => $job_array,
            'cus'   => $cus,
            'param' => $nama_param,
            'sample_array' => $sample_array,
            'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert],
            'menu_active' => 'registrasi'
        ]);

    }

    public function save_registrasi(Request $request){
        $data = $request->input('data');
        $data_sample = $request->input('data_sample');

        $pesanAlert=".";

        try{
            $job = new Job;
            $job->id = $data['id'];
            $job->customer_id = $data['customer_id'];
            $job->sampled_by = $data['sampled_by'];
            $job->attention = $data['attention'];
            $job->location = $data['location'];
            $job->date_recent =  $data['date_recent'];
            $job->time_recent = $data['time_recent'];
            $job->number_samples =$data['number_samples'];
            $job->interval = $data['interval'];
            $job->status = 'Onprogress';
            $job->nonaktif = 0;
            $job->price = $data['price_job'];
            $job->save();
            $log1 = new Log;
            $log1->info = "INSERT ID ".$data['id'];
            $log1->table_name = "JOB";
            $log1->user_id = Auth::user()->id;
            $log1->save();

            Customer::where('id_customer','=',$data['customer_id'])->update([
                'address'   => $data['address'],
                'city'      => $data['city'],
                'phone'     => $data['phone'],
                'fax'       => $data['fax']
            ]);

            foreach ($data_sample as $key => $value) {
                $sample = new Sample;
                $sample->kode_sample = $value['kode_sample'];
                $sample->job_id = $data['id'];
                $sample->ref_test_id = $value['ref_test_id'];
                $sample->procedure_id = $value['procedure_id'];
                $sample->matrix_id = $value['matrix_id'];
                $sample->name = $value['name'];
                $sample->date_sampled = $value['date_sampled'];
                $sample->time_sampled = $value['time_sampled'];
                $sample->status = 'Onprogress (AN)';
                $sample->price = $value['price_sample'];
                $sample->coordinat_S = $value['coordinat_S'];
                $sample->coordinat_E = $value['coordinat_E'];
                $sample->ph = $value['ph'];
                $sample->suhu = $value['suhu'];
                $sample->sal = $value['sal'];
                $sample->dhl = $value['dhl'];
                $sample->do = $value['do'];
                $sample->debit = $value['debit'];
                $sample->kappro = $value['kp'];
                $sample->info = $value['ket'];
                $sample->nonaktif = 0;
                $sample->save();
                $log2 = new Log;
                $log2->info = "INSERT Kode ".$value['kode_sample'];
                $log2->table_name = "SAMPLE";
                $log2->user_id = Auth::user()->id;
                $log2->save();

                foreach ($value['param_sampling'] as $keys => $val) {
                    $analis[$key][$keys] = Assignment::where('parameter_id','=',$val)->pluck('user_id');
                    $temp=1000000;
                    $analis_id = 0;
                    foreach ($analis[$key][$keys] as $keyss => $valu) {
                        $analis_sample[$keyss] = Parameter_sample::where('user_id','=',$valu)->where('status_parameter','!=','OK (TM)')->pluck('user_id');
                        $count[$keyss] = $analis_sample[$keyss]->count();
                        if($count[$keyss] <= $temp){
                            $temp = $count[$keyss];
                            $analis_id = $analis_sample[$keyss]->first();
                        }
                    }
                    if ($temp == 0){
                        $analis_id = Assignment::where('parameter_id','=',$val)->pluck('user_id')->sortByDesc('id')->first();
                    }
                    $insert = $sample->parameter_samples()->create([
                        'parameter_id'      => $val,
                        'user_id'           => $analis_id,
                        'nonaktif'          => 0,
                        'status_parameter'  => 'Onprogress (AN)'
                    ]);
                    $log3 = new Log; 
                    $log3->info = "INSERT ID ".$insert->id;
                    $log3->table_name = "Parameter_sample";
                    $log3->user_id = Auth::user()->id;
                    $log3->save();
                }
            }
            $pesanAlert = 'Insert Job berhasil!';

        }catch(Exception $exception)
        {
            $pesanAlert = 'Database error! ' . $exception->getCode();
        }
        
        $cus= Customer::find($data['customer_id']);

        return view('frontOffice.tt')->with([
            'data' => $data,
            'cus'   => $cus,
            'sample_array' => $data_sample,
            'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert],
            'menu_active' => 'registrasi'
        ]);

    }    

    public function procedure(Request $request){
        $code=$request->input('option');
        $sp = Sampling_procedure::where('matrix_id','=', $code)
                        ->get();
        foreach ($sp as $key => $value) {
            $data[] = array('id' => $value->id, 'text' => $value->name_procedure);
        }
        return Response::json( $data );
    }

    public function ref(Request $request){
        $code=$request->input('option');
        $ref = Ref_test::where('matrix_id','=', $code)
                        ->where('level','=', 0)
                        ->get();
        
        foreach ($ref as $key => $value) {
            $data[] = array('id' => $value->id, 'text' => $value->name);
        }
        return Response::json( $data );
    }

    public function ref_sub(Request $request){
        $code=$request->input('option');
        $data = Ref_test::where('kode','like',$code.'%')
                        ->where('level', '=', 1)
                        ->pluck('name', 'id');
        // $data = DB::table('hierarchy_ref_test')
        //     ->join('ref_test', 'hierarchy_ref_test.id_child', '=', 'ref_test.id')
        //     ->select('ref_test.*')
        //     ->where('hierarchy_ref_test.id_parent','=', $code)
        //     ->where('level', '=', 1)
        //     ->pluck('name', 'id');
        return Response::json( $data );
    }

    public function ref_sub2(Request $request){
        $code=$request->input('option');
        $data = Ref_test::where('kode','like',$code.'%')
                        ->where('level', '=', 2)
                        ->pluck('name', 'id');
        // $data = DB::table('hierarchy_ref_test')
        //     ->join('ref_test', 'hierarchy_ref_test.id_child', '=', 'ref_test.id')
        //     ->select('ref_test.*')
        //     ->where('hierarchy_ref_test.id_parent','=', $code)
        //     ->where('level', '=', 2)
        //     ->pluck('name', 'id');
        return Response::json( $data );
    }

    public function ref_param()
    {
        $data = Parameter::pluck('name', 'id');
    }

    public function dataCustom(Request $request)
    {
        $code=$request->input('option');
        $data = Customer::where('id_customer','=', $code)
                            ->pluck('id', 'name_customer');
        return Response::json( $data );
    }

    public function dataCustom2(Request $request)
    {
        $code=$request->input('option');
        $data = Customer::where('id_customer','=', $code)
                            ->first();
        return Response::json( $data );
    }

    public function list_job()
    {
        $job = Job::where('nonaktif','=',0)->get();
        foreach ($job as $key => $value) {
            $value->customer['name_customer'];
        }

        return view('frontOffice.list_job')->with([
            'job' => $job,
            'menu_active' => 'job'
        ]);
    }

    public function list_sample($id)
    {
        $sample = Sample::where('job_id','=',$id)->where('nonaktif','=',0)->get();
        $job = Job::find($id);
        foreach ($sample as $key => $value) {
             $value->matrix->name;
             $value->ref->name;
             $value->procedure->name_procedure;
             $param[$key]=Parameter_sample::where('sample_id','=', $value->id)->get();
        }

        return view('frontOffice.sample_list')->with([
            'sample' => $sample,
            'job'   => $job,
            'menu_active' => 'job',
            'param' => $param
        ]);
    }

    // public function detail_sample($id)
    // {
    //     $sample = find($id);
    //     $param = Parameter_sample::where('sample_id','=',$id)->get();
    //     $i = 0;
    //     foreach ($param as $key => $value) {
    //          $value->parameter->name;
    //          if($key < 17){
    //             $param1[$key] = $value;
    //          }
    //          else{
    //             $param2[$i] = $value;
    //             $i++;
    //          }
    //     }

    //     return view('frontOffice.detail_sample')->with([
    //         'sample' => $sample,
    //         'param' => $param,
    //         'menu_active' => 'job'
    //     ]);
    // }

    public function add_customer(Request $request)
    {
        $fax = $request->input('fax');
        if($fax == null){$fax = '-';}
        $customer = new Customer;
        $customer->name_customer = $request->input('name');
        $customer->address = $request->input('address');
        $customer->city = $request->input('city');
        $customer->phone = $request->input('phone');
        $customer->fax = $fax;
        $customer->save();
        $log4 = new Log;
        $log4->info = "INSERT ID ".$customer->id_customer;
        $log4->table_name = "CUSTOMER";
        $log4->user_id = Auth::user()->id;
        $log4->save();
        $pesanAlert = 'Insert Customer berhasil!';
        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert]
        ]);
    }

    public function interval_date(Request $request)
    {
        $date=$request->input('option');
        $date2 = Carbon::createFromFormat("Y-m-d", $date);
        $data = $date2->addDays(6)->toDateString(); 
        return Response::json( $data );
    }

    public function edit_job($id)
    {
        $job = Job::find($id);
        $customerID = Customer::pluck('id_customer');
        $customer = Customer::pluck('name_customer', 'id_customer');
        return view('frontOffice.edit_job')->with([
            'job' => $job,
            'customer2' => $customerID,
            'customer' => $customer,
            'menu_active' => 'job'
        ]);
    }

    public function save_edit(Request $request)
    {
        Job::where('id','=',$request->input('job_number'))->update([
            'customer_id' => $request->input('customer'),
            'sampled_by'    => $request->input('by'),
            'attention' => $request->input('attention'),
            'date_recent' =>$request->input('datepicker'),
            'time_recent' =>$request->input('time'),
            'interval'  =>$request->input('interval'),
            'location' => $request->input('location')
            ]);
        $pesanAlert = "Data barhasil diedit!";
        $log5 = new Log;
        $log5->info = "UPDATE ID ".$request->input('job_number');
        $log5->table_name = "JOB";
        $log5->user_id = Auth::user()->id;
        $log5->save();
        
       return redirect('/front/joblist')->with([
            'alert' => [    'type'      => 'success', 
                        'message'   => $pesanAlert]
        ]);
    }

    public function customer_list(Request $request)
    {
        $cus = Customer::where('nonaktif','=','0')->get();
        
        return view('frontOffice.customer_list')->with([
            'customer' => $cus,
            'menu_active' => 'customer'
        ]);
    }

    public function edit_customer($id)
    {
        $cus = Customer::where('id_customer','=',$id)->first();

        return view('frontOffice.customer_edit')->with([
            'customer' => $cus,
            'menu_active' => 'customer',
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Customer berhasil diupdate']
        ]);
    }

    public function nonactive_customer($id)
    {
        Customer::where('id_customer','=',$id)->update(['nonaktif'=>1]);
        $log4 = new Log;
        $log4->info = "UPDATE ID ".$id;
        $log4->table_name = "CUSTOMER";
        $log4->user_id = Auth::user()->id;
        $log4->save();

        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Customer berhasil dihapus']
        ]);
    }

    public function customer_save($id, Request $request)
    {
        Customer::where('id_customer','=',$id)->update([
            'name_customer' => $request->input('name_customer'),
            'phone'    => $request->input('phone'),
            'fax' => $request->input('fax'),
            'address' =>$request->input('address'),
            'city' =>$request->input('city')
            ]);

        $log4 = new Log;
        $log4->info = "UPDATE ID ".$id;
        $log4->table_name = "CUSTOMER";
        $log4->user_id = Auth::user()->id;
        $log4->save();

        $pesanAlert = "Data barhasil diedit!";
        
       return redirect('/front/customerlist')->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert]
        ]);
    }

    public function certificate()
    {
        $job = Job::where('status','=','OK')->where('nonaktif','=',0)->get();

        return view('frontOffice.certificate_page')->with([
            'job' => $job,
            'menu_active' => 'sertif'
        ]);
    }

    public function certificate_sample($id)
    {
        $sample = Sample::where('job_id','=', $id)->where('nonaktif','=',0)->get();

        return view('frontOffice.certificate_sample')->with([
            'sample' => $sample,
            'menu_active' => 'sertif'
        ]);
    }

    public function certificate_per_sample($id)
    {
        $sample = Sample::find($id);
        $param = Parameter_sample::where('sample_id','=',$id)->get();
        $baku = Ref_param::where('ref_test_id','=',$sample->ref_test_id)->get();
        $status = array();
        $baku2 = array();
        $result = array();

        
        foreach ($param as $keys => $val) {
            $result[$keys] = Result::where('para_sample_id','=',$val->id)->where('status_result','=','OK (TM)')->first();
            $standard = $baku->where('parameter_id',$val->parameter_id);
            if($standard->count() != null){
                array_push($baku2, $standard[$keys]->dosage_maksimum);
                if($standard[$keys]->sign != null){
                    if($standard[$keys]->sign == '>'){
                        if($standard[$keys]->dosage_maksimum > $result[$keys]->value) $status[$keys] = 'melebihi';
                        else $status[$keys] = "-";
                    }
                }
                else{
                    if($standard[$keys]->dosage_maksimum < $result[$keys]->value){
                        $status[$keys] = 'melebihi';
                    }
                    else{
                        $status[$keys] = '-';
                    }
                }
             }
             else{
                array_push($baku2, 'nothing');
                $status[$keys] = '-'; //perlu diperbaikiiiiiii dan perlu ditanyakan!

            }
        }
        
        return view('frontOffice.testingsertif')->with([
            'param'     => $param,
            'baku'      => $baku2,
            'result'    => $result,
            'status'    => $status,
            'menu_active' => 'sertif'
        ]);
    }

    public function pricelist(){
        $job = Job::where('nonaktif','=',0)->get();
        return view('frontOffice.pricelist')->with([
            'job'           => $job,
            'menu_active'   => 'price'
        ]);
    }

    public function detailprice(Request $request)
    {
        $code=$request->input('option');
        $data = Sample::where('job_id','=', $code)->where('nonaktif','=',0)
                            ->get();
        return Response::json( $data );
    }
}
