<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Late_record;

class SectionheadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('frontOffice.registrasi');
    }

    public function latelist()
    {
        $late = Late_record::get();

        return view('sectionHead.latelist')->with([
            'late'  => $late
        ]);
    }
}
