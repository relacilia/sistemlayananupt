<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sample;
use App\Sample_matrix;
use App\Sampling_procedure;
use App\Job;
use App\Notification;
use App\Customer;
use App\Role;
use App\Log;
use App\Role_user;
use App\User;
use App\Ref_param;
use App\Ref_test;
use App\Result;
use App\Parameter;
use App\Assignment;
use App\Parameter_sample;
use Auth;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.home')->with([
        ]);
    }

    public function register()
    {
        $role = Role::get();
        return view('auth.register')->with([
            'role' => $role
        ]);
    }

    public function userlist()
    {
        $user = User::where('nonaktif','=',0)->get();
        $data[]=0;
        foreach ($user as $key => $value) {
           $data[$key] = Role_user::where('user_id','=', $value->id)->get();
           foreach ($data[$key] as $keys => $val) {
               $val->roles->display_name;
           }
        }

        return view('admin.userlist')->with([
            'users' => $user,
            'data'  => $data
        ]);
    }

    //Perlu diperbaiki codingnya
    public function registration(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);

        $user = new User;
        $user->name = $request->input('name');
        $user->address = $request->input('address');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->password = bcrypt($request->input('password'));
        $user->remember_token = Str::random(60);
        $user->save();
        $log4 = new Log;
        $log4->info = "INSERT ID ".$user->id;
        $log4->table_name = "USERS";
        $log4->user_id = Auth::user()->id;
        $log4->save();

        foreach ($request->input('role') as $key => $value) {
            $user->role()->attach($value);            
        }

        $users = User::where('nonaktif','=',0)->orderBy('created_at','desc')->get();
        $data[]=0;
        foreach ($users as $key => $value) {
           $data[$key] = Role_user::where('user_id','=', $value->id)->get();
           foreach ($data[$key] as $keys => $val) {
               $val->roles->display_name;
           }
        }
        $pesanAlert = 'User '.$request->input("name").' berhasil ditambahkan';
        return view('admin.userlist')->with([
            'users' => $users,
            'data'  => $data,
            'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert],
        ]);
    }

    public function user_edit($id)
    {
        $user   = User::find($id);
        $role   = Role::get();
        $role_user = Role_user::where('user_id','=',$id)->get();
        foreach ($role as $key => $value) {
            foreach ($role_user as $keys => $val) {
                if ($value->id == $val->role_id) {
                    $value->check = 1;
                }
            }
        }

        return view('admin.user_edit')->with([
            'users' => $user,
            'role'  => $role
        ]);
    }

    //perbaiki ini
    public function user_edit_save($id, Request $request)
    {

        $this->validate($request, [
            'name'      => 'required|max:255',
        ]);

        User::where('id','=',$id)->update([
            'name'      => $request->input('name'),
            'address'   => $request->input('address'),
            'phone'     => $request->input('phone')
        ]);

        $log4 = new Log;
        $log4->info = "UPDATE ID ".$id;
        $log4->table_name = "USERS";
        $log4->user_id = Auth::user()->id;
        $log4->save();

        $user = User::find($id);

        $role_user = Role_user::where('user_id','=',$id)->delete();

        foreach ($request->input('role') as $key => $value) {
            $user->role()->attach($value);            
        }

        return $this->userlist()->with([
            'alert' => [    'type'      => 'success', 
                            'message'   => 'User berhasil diupdate']
        ]);
    }

    public function analyst()
    {
        $user = User::where('nonaktif','=',0)->get();
        $analyst = collect();
        foreach ($user as $key => $value) {
            $users = $value->role_user;
            foreach ($users as $keys => $val) {
                if($val->id == 2)
                    $analyst = $analyst->prepend($value);
            }
        }

        $analyst = $analyst->sortBy('id');
        $param = array();
        $param_list = array();
        foreach ($analyst as $key => $value) {
            $param[$key] = Assignment::where('user_id','=',$value->id)->get();
            $param_list[$key] = Parameter::get();
            foreach ($param_list[$key] as $val1) {
                foreach ($param[$key] as $val2) {
                    if($val1->id == $val2->parameter_id)
                        $val1->check = 1;
                    else
                        $val->check = 0;
                }
            }
        }

        return view('admin.analyst_list')->with([
            'analyst'   => $analyst,
            'param'     => $param,
            'parameter' => $param_list 
        ]);  
    }

    public function analyst_update_save($id, Request $request){
        Assignment::where('user_id','=',$id)->delete();
        foreach ($request->input('parameter') as $key => $value) {
            $assignment                 = new Assignment;
            $assignment->user_id        = $id;
            $assignment->parameter_id   = $value;
            $assignment->save();

            $log4 = new Log;
            $log4->info = "UPDATE ID ".$assignment->id;
            $log4->table_name = "Assignment";
            $log4->user_id = Auth::user()->id;
            $log4->save();
        }
        $pesanAlert = 'Berhasil diupdate';
        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert],
        ]);
    }

    public function matrix(){
        $matrix = Sample_matrix::where('nonaktif','=',0)->get();

        return view('admin.matrix_list')->with([
            'matrix'    => $matrix
        ]); 
    }

    public function matrix_new_save(Request $request){
        $matrix         = new Sample_matrix;
        $matrix->alias  = $request->input('alias');
        $matrix->name   = $request->input('name');
        $matrix->save();
        $log = new Log;
        $log->info = "INSERT ID ".$matrix->id;
        $log->table_name = "Sample_matrix";
        $log->user_id = Auth::user()->id;
        $log->save();

        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Matrix berhasil ditambahkan']
        ]);
    }

    public function matrix_update_save($id, Request $request)
    {
        Sample_matrix::where('id','=',$id)->update([
            'alias' => $request->input('alias'),
            'name' => $request->input('name')
        ]);

        $log = new Log;
        $log->info = "UPDATE ID ".$id;
        $log->table_name = "Sample_matrix";
        $log->user_id = Auth::user()->id;
        $log->save();

        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Matrix berhasil diupdate']
        ]);
    }

    public function procedure(){
        $procedure = Sampling_procedure::get();
        $matrix     = Sample_matrix::where('nonaktif','=',0)->get();
        return view('admin.procedure_list')->with([
            'procedure'     => $procedure,
            'matrixlist'    => $matrix
        ]);

    }

    public function procedure_new_save(Request $request){
        $procedure = new Sampling_procedure;
        $procedure->name_procedure  = $request->input('name');
        $procedure->matrix_id       = $request->input('matrix');
        $procedure->save();

        $log = new Log;
        $log->info = "INSERT ID ".$procedure->id;
        $log->table_name = "Sampling_procedure";
        $log->user_id = Auth::user()->id;
        $log->save();

        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Procedure berhasil ditambahkan']
        ]);
    }

    public function procedure_save($id, Request $request){
        Sampling_procedure::where('id','=',$id)->update([
            'name_procedure'    => $request->input('name'),
            'matrix_id'         => $request->input('matrix')
        ]);

        $log = new Log;
        $log->info = "UPDATE ID ".$id;
        $log->table_name = "Sampling_procedure";
        $log->user_id = Auth::user()->id;
        $log->save();

        return $this->procedure()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Metode berhasil diubah']
        ]);
    }

    public function parameter(){
        $parameter = Parameter::get();
        $procedure = Sampling_procedure::pluck('name_procedure','id');
        $analis = array();
        foreach ($parameter as $key => $value) {
            $analis[$key] = Assignment::where('parameter_id','=',$value->id)->get();
        }

        return view('admin.parameter_list')->with([
            'parameter'     => $parameter,
            'procedure'     => $procedure,
            'analyst'        => $analis
        ]);

    }

    public function parameter_new_save(Request $request){
        // $this->validate($request, [
        //     'name'      => 'required|max:255',
        // ]);

        $param = new Parameter;
        $param->procedure_id = $request->input('procedure');
        $param->name = $request->input('name');
        $param->standard = $request->input('standard');
        $param->price = $request->input('price');
        $param->accreditation = $request->input('accreditation');
        $param->mdl = $request->input('mdl');
        $param->save();
        $log = new Log;
        $log->info = "INSERT ID ".$param->id;
        $log->table_name = "Parameter";
        $log->user_id = Auth::user()->id;
        $log->save();

        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Customer berhasil ditambahkan']
        ]);
    }

    public function parameter_edit($id){
        $parameter = Parameter::find(1);
        $procedure = Sampling_procedure::pluck('name_procedure','id');
        $analis = Assignment::where('parameter_id','=',$parameter->id)->get();
        $allAnalis = Role_user::where('role_id','=',2)->get();
        foreach ($allAnalis as $key => $value) {
            foreach ($analis as $val) {
                if($value->user_id == $val->user_id) $value->check = 1;
            }
        }

        return view('admin.parameter_edit')->with([
            'parameter'     => $parameter,
            'procedure'     => $procedure,
            'analyst'       => $analis,
            'allAnalis'     => $allAnalis
        ]);

    }

    public function parameter_edit_save($id, Request $request){
        $new =  $request->input('analyst');
        $old = Assignment::where('parameter_id','=',$id)->get();
        
        foreach ($old as $key => $value) {
            $check = 0;
            foreach ($new as $keys => $val) {
                if($value->user_id == $val) $check++;
            }
            if($check==0){
                Assignment::where('user_id','=',$value->user_id)->where('parameter_id','=',$id)->delete();
                $log = new Log;
                $log->info = "DELETE ID ".$value->id;
                $log->table_name = "Assignment";
                $log->user_id = Auth::user()->id;
                $log->save();
            }
        }

        foreach ($new as $keys => $val) {
            $check = 0;
            foreach ($old as $key => $value) {
               if($value->user_id != $val) $check++;
            }
            if($check == $old->count()){
                $new = new Assignment;
                $new->parameter_id = $id;
                $new->user_id = $val;
                $new->save();

                $log = new Log;
                $log->info = "INSERT ID ".$new->id;
                $log->table_name = "Assignment";
                $log->user_id = Auth::user()->id;
                $log->save();
            }
        }

        Parameter::where('id','=',$id)->update([
            'procedure_id'  => $request->input('procedure'),
            'name'          => $request->input('name'),
            'standard'      => $request->input('standard'),
            'price'         => $request->input('price'),
            'accreditation' => $request->input('accreditation'),
            'mdl'           => $request->input('mdl')
        ]);

        $log = new Log;
        $log->info = "UPDATE ID ".$id;
        $log->table_name = "Parameter";
        $log->user_id = Auth::user()->id;
        $log->save();

        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Customer berhasil diupdate']
        ]);
    }

    public function customer_list()
    {
        $cus = Customer::where('nonaktif','=','0')->get();
        
        return view('frontOffice.customer_list')->with([
            'customer' => $cus
        ]);
    }

    public function edit_customer($id)
    {
        $cus = Customer::where('id_customer','=',$id)->first();

        return view('frontOffice.customer_edit')->with([
            'customer' => $cus,
            'menu_active' => 'customer',
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Customer berhasil diupdate']
        ]);
    }

    public function nonactive_customer($id)
    {
        Customer::where('id_customer','=',$id)->update(['nonaktif'=>1]);
        $log = new Log;
        $log->info = "update ID ".$id;
        $log->table_name = "CUSTOMER";
        $log->user_id = Auth::user()->id;
        $log->save();

        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Customer berhasil dihapus']
        ]);
    }

    public function customer_save($id, Request $request)
    {
        Customer::where('id_customer','=',$id)->update([
            'name_customer' => $request->input('name_customer'),
            'phone'    => $request->input('phone'),
            'fax' => $request->input('fax'),
            'address' =>$request->input('address'),
            'city' =>$request->input('city')
            ]);

        $log = new Log;
        $log->info = "UPDATE ID ".$id;
        $log->table_name = "CUSTOMER";
        $log->user_id = Auth::user()->id;
        $log->save();

        $pesanAlert = "Data barhasil diedit!";
        
       return redirect('/admin/customerlist')->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert]
        ]);
    }

    public function reftest_list()
    {
        $reftest = Ref_test::get();
        $refparam = array();
        foreach ($reftest as $key => $value) {
            $refparam[$key] = Ref_param::where('ref_test_id','=',$value->id)->get();
        }
        
        return view('admin.reftest_list')->with([
            'reftest'   => $reftest,
            'refparam'  => $refparam
        ]);
    }

    public function reftest_add()
    {
        $kode = Ref_test::where('level','=','0')->orderBy('id', 'desc')->pluck('kode')->first();
        $parameter = Parameter::get();
        $matrix = Sample_matrix::where('nonaktif','=',0)->get();
        return view('admin.reftest_add')->with([
            'matrix'    => $matrix,
            'kode'      => $kode,
            'parameter' => $parameter
        ]);
    }

    public function reftest_add_save(Request $request)
    {
        $kode0 = $request->input('kode');
        $name = $request->input('name');
        $matrix = $request->input('matrix');
        $subname = $request->input('subname');
        $parameter = $request->input('parameter');
        $sign  = $request->input('sign');
        $nilai  = $request->input('nilai');
        $parameter2 = $request->input('parameter2');
        $sign2  = $request->input('sign2');
        $nilai2  = $request->input('nilai2');

        try{
        $ref = new Ref_test;
        $ref->kode = $kode0;
        $ref->name = $name;
        $ref->matrix_id = $matrix;
        $ref->level = '0';
        $ref->save();
        $log = new Log;
        $log->info = "INSERT ID ".$ref->id;
        $log->table_name = "Ref_test";
        $log->user_id = Auth::user()->id;
        $log->save();
        if($subname == null){
            foreach ($sign2 as $keys => $value) {
                if($value == 'no') $value = null;
                $insert = $ref->ref_parameter()->create([
                        'parameter_id'  => $parameter2[$keys],
                        'sign'          => $value,
                        'dosage_maksimum'   => $nilai2[$keys]
                ]);
                $log2 = new Log;
                $log2->info = "INSERT ID ".$insert->id;
                $log2->table_name = "Ref_param";
                $log2->user_id = Auth::user()->id;
                $log2->save();
            }
        }
        else{
            foreach ($subname as $key => $value) {
                $sref = new Ref_test;
                $sref->kode = $kode0.'.'.($key+1);
                $sref->name = $value;
                $sref->matrix_id = $matrix;
                $sref->level = '1';
                $sref->save();
                $log = new Log;
                $log->info = "INSERT ID ".$sref->id;
                $log->table_name = "Ref_test";
                $log->user_id = Auth::user()->id;
                $log->save();
                foreach ($parameter[$key] as $keys => $val) {
                    if($sign[$key][$keys] == 'no') $sign[$key][$keys] = null;
                    $insert = $sref->ref_parameter()->create([
                        'parameter_id'  => $val,
                        'sign'          => $sign[$key][$keys],
                        'dosage_maksimum'   => $nilai[$key][$keys]
                    ]);
                    $log2 = new Log;
                    $log2->info = "INSERT ID ".$insert->id;
                    $log2->table_name = "Ref_param";
                    $log2->user_id = Auth::user()->id;
                    $log2->save();
                }
            }
        }
        }catch(Exception $exception)
        {
            $pesanAlert = 'Database error! ' . $exception->getCode();
        }

        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Baku mutu berhasil ditambahkan']
        ]);

    } 

    public function reftest_edit($id){
        $ref = Ref_test::find($id);
        $kode = Ref_test::where('level','=','0')->orderBy('id', 'desc')->pluck('kode')->first();
        $parameter = Parameter::get();
        $matrix = Sample_matrix::where('nonaktif','=',0)->get();
        $param = array();
        $param2 = array();
        $sub = array();
        if($ref->level == 0){
           $sub = Ref_test::where('kode','LIKE',$id.'.%')->get();
           foreach ($sub as $key => $value) {
               $param[$key] = Ref_param::where('ref_test_id','=',$value->id)->get();
           }
        }
        $param2 = Ref_param::where('ref_test_id','=',$id)->get(); 

        return view('admin.reftest_edit')->with([
            'ref'       => $ref,
            'matrix'    => $matrix,
            'kode'      => $kode,
            'parameter' => $parameter,
            'sub'       => $sub,
            'param'     => $param,
            'param2'    => $param2,
            'count'     => count($sub)
        ]);
    }

    public function reftest_edit_save(Request $request)
    {
        $kode0 = $request->input('kode');
        $name = $request->input('name');
        $matrix = $request->input('matrix');
        $subname = $request->input('subname');
        $parameter = $request->input('parameter');
        $sign  = $request->input('sign');
        $nilai  = $request->input('nilai');
        $parameter2 = $request->input('parameter2');
        $sign2  = $request->input('sign2');
        $nilai2  = $request->input('nilai2');

         try{
         $ref = new Ref_test;
        $ref->kode = $kode0;
        $ref->name = $name;
        $ref->matrix_id = $matrix;
        $ref->level = '0';
        dd($ref);
        // $ref->save();
        // if($subname == null){
        //     foreach ($sign2 as $keys => $value) {
        //         if($value == 'no') $value = null;
        //         $ref->ref_parameter()->create([
        //                 'parameter_id'  => $parameter2[$keys],
        //                 'sign'          => $value,
        //                 'dosage_maksimum'   => $nilai2[$keys]
        //         ]);
        //     }
        // }
        // else{
        //     foreach ($subname as $key => $value) {
        //         $sref = new Ref_test;
        //         $sref->kode = $kode0.'.'.($key+1);
        //         $sref->name = $value;
        //         $sref->matrix_id = $matrix;
        //         $sref->level = '1';
        //         $sref->save();
        //         foreach ($parameter[$key] as $keys => $val) {
        //             if($sign[$key][$keys] == 'no') $sign[$key][$keys] = null;
        //             $sref->ref_parameter()->create([
        //                 'parameter_id'  => $val,
        //                 'sign'          => $sign[$key][$keys],
        //                 'dosage_maksimum'   => $nilai[$key][$keys]
        //             ]);
        //         }
        //     }
        // }
        }catch(Exception $exception)
        {
            $pesanAlert = 'Database error! ' . $exception->getCode();
        }

        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Customer berhasil dihapus']
        ]);

    } 

    public function reftest_delete($id){
        dd($id);
    }

    public function joblist(){
        $job = Job::where('nonaktif','=',0)->get();
        foreach ($job as $key => $value) {
            $value->customer['name_customer'];
        }

        return view('admin.joblist')->with([
            'job' => $job,
        ]);
    }

    public function list_sample($id)
    {
        $sample = Sample::where('job_id','=',$id)->where('nonaktif','=',0)->get();
        $job = Job::find($id);
        $param = array();
        foreach ($sample as $key => $value) {
             $value->matrix->name;
             $value->ref->name;
             $value->procedure->name_procedure;
             $param[$key]=Parameter_sample::where('sample_id','=', $value->id)->get();
        }

        return view('admin.samplelist')->with([
            'sample' => $sample,
            'job'   => $job,
            'menu_active' => 'job',
            'param' => $param
        ]);
    }  

    public function save_edit(Request $request)
    {
        Job::where('id','=',$request->input('job_number'))->update([
            'customer_id' => $request->input('customer'),
            'sampled_by'    => $request->input('by'),
            'attention' => $request->input('attention'),
            'date_recent' =>$request->input('datepicker'),
            'time_recent' =>$request->input('time'),
            'interval'  =>$request->input('interval'),
            'location' => $request->input('location')
            ]);
        $log = new Log;
        $log->info = "update ID ".$request->input('job_number');
        $log->table_name = "JOB";
        $log->user_id = Auth::user()->id;
        $log->save();
        $pesanAlert = "Data barhasil diedit!";
        
       return redirect('/admin/joblist')->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert]
        ]);
    }

    public function sample_edit($id)
    {
        $sample = Sample::find($id);
        $param  = Parameter_sample::where('sample_id','=',$id)->get();
        $parameter = Parameter::get();
        foreach ($parameter as $keys => $val) {
             foreach ($param as $key => $value) {
                $assignment[$key] = Assignment::where('parameter_id','=',$value->parameter_id)->get();
                if($val->id == $value->parameter_id){
                    $val->check = 1;
                }
            }
        }
        return view('admin.sample_edit')->with([
            'sample'    => $sample,
            'param'     => $param,
            'assignment'=> $assignment,
            'parameter' => $parameter
        ]);
    } 

    public function save_parameter_sample($id, Request $request){
        $new = $request->input('newparam');
        $param = Parameter_sample::where('sample_id','=',$id)->get();
        foreach ($param as $key => $value) {
            $check = 0;
            foreach ($new as $keys => $val) {
                if($value->parameter_id == $val) $check++;
            }
            if($check==0){
                Parameter_sample::where('sample_id','=',$id)->where('parameter_id','=',$value->parameter_id)->delete();
                $log = new Log;
                $log->info = "DELETE ID ".$value->id;
                $log->table_name = "Parameter_sample";
                $log->user_id = Auth::user()->id;
                $log->save();
            }
        }

        foreach ($new as $keys => $val) {
            $check = 0;
            foreach ($param as $key => $value) {
               if($value->parameter_id != $val) $check++;
            }
            if($check == $param->count() ){
                $analis = Assignment::where('parameter_id','=',$val)->pluck('user_id');
                $temp=1000000;
                $analis_id = 0;
                foreach ($analis as $keyss => $valu) {
                    $analis_sample[$keyss] = Parameter_sample::where('user_id','=',$valu)->where('status_parameter','!=','OK (TM)')->pluck('user_id');
                    $count[$keyss] = $analis_sample[$keyss]->count();
                    if($count[$keyss] <= $temp){
                        $temp = $count[$keyss];
                        $analis_id = $analis_sample[$keyss]->first();
                    }
                }
                if ($temp == 0){
                    $analis_id = Assignment::where('parameter_id','=',$val)->pluck('user_id')->sortByDesc('id')->first();
                }
                $new = new Parameter_sample;
                $new->sample_id = $id;
                $new->parameter_id = $val;
                $new->user_id = $analis_id;
                $new->status_parameter = 'Onprogress (AN)';
                $new->save();
                $log = new Log;
                $log->info = "INSERT ID ".$new->id;
                $log->table_name = "Parameter_sample";
                $log->user_id = Auth::user()->id;
                $log->save();
            }
        }
        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Data berhasil disimpan']
        ]);
    }

    public function edit_matrix($id){
        $sample = Sample::find($id);
        $matrix = Sample_matrix::where('nonaktif','=',0)->get();
        return view('admin.matrix_edit')->with([
            'sample'    => $sample,
            'matrix'    => $matrix
        ]);
    }

    public function save_edit_sample($id, Request $request){
        $name = $request->input('name');
        $date = $request->input('date');
        $time = $request->input('time');
        $coordinat_s = $request->input('coordinat_s');
        $coordinat_e = $request->input('coordinat_e');
        $price = $request->input('price');
        $status = $request->input('status');
        $matrix = $request->input('matrix');
        $procedure = $request->input('procedure');
        $ref = $request->input('ref');

        $ph = $request->input('ph');
        $suhu = $request->input('suhu');
        $dhl = $request->input('dhl');
        $do = $request->input('do');
        $sal = $request->input('sal');
        $debit = $request->input('debit');
        $kappro = $request->input('kappro');
        $info = $request->input('info');

        $user = $request->input('user');

        $parameter_sample = Parameter_sample::where('sample_id','=',$id)->get();
        foreach ($parameter_sample as $key => $val) {
            $val->update([
                'user_id' => $user[$key]
            ]);

            $log = new Log;
            $log->info = "UPDATE ID ".$val->id;
            $log->table_name = "Parameter_sample";
            $log->user_id = Auth::user()->id;
            $log->save();
        }

        Sample::where('id','=',$id)->update([
            'name'          =>  $name,
            'date_sampled'  =>  $date,
            'time_sampled'  =>  $time,
            'coordinat_S'   =>  $coordinat_s,
            'coordinat_E'   =>  $coordinat_e,
            'price'         =>  $price,
            'status'        =>  $status,
            'matrix_id'     =>  $matrix,
            'procedure_id'  =>  $procedure,
            'ref_test_id'   =>  $ref,
            'ph'            =>  $ph,
            'suhu'          =>  $suhu,
            'dhl'           =>  $dhl,
            'do'            =>  $do,
            'sal'           =>  $sal,
            'debit'         =>  $debit,
            'kappro'        =>  $kappro,
            'info'          =>  $info
        ]);

        $log2 = new Log;
        $log2->info = "UPDATE ID ".$id;
        $log2->table_name = "Sample";
        $log2->user_id = Auth::user()->id;
        $log2->save();

        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Data berhasil diupdate']
        ]);
    } 

    public function job_delete($id){
        Job::where('id','=',$id)->update([
            'nonaktif' => 1
        ]);

        $log = new Log;
        $log->info = "NONAKTIF ID ".$id;
        $log->table_name = "JOB";
        $log->user_id = Auth::user()->id;
        $log->save();

        $sample = Sample::where('job_id','=',$id)->get();
        foreach ($sample as $key => $value) {
            $value->nonaktif    = 1;
            $value->save();

            $log2 = new Log;
            $log2->info = "NONAKTIF ID = ".$value->id;
            $log2->table_name = "SAMPLE";
            $log2->user_id = Auth::user()->id;
            $log2->save();
        }

        return $this->joblist()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Data berhasil dihapus']
        ]);
    }

    public function sample_delete($id, $sample){
        Sample::where('id','=',$sample)->update([
            'nonaktif' => 1
        ]);

        $log = new Log;
        $log->info = "NONAKTIF ID = ".$sample;
        $log->table_name = "SAMPLE";
        $log->user_id = Auth::user()->id;
        $log->save();

        $sample = Sample::find($sample);
        $sample->job->price = $sample->job->price - $sample->price;
        Job::where('id','=',$id)->update([
            'price' => $sample->job->price
        ]);

        $log2 = new Log;
        $log2->info = "UPDATE ID = ".$id;
        $log2->table_name = "JOB";
        $log2->user_id = Auth::user()->id;
        $log2->save();

        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Data berhasil dihapus']
        ]);
    }

    public function matrix_delete($id){
        Sample_matrix::where('id','=',$id)->update([
            'nonaktif' => 1
        ]);

        $log = new Log;
        $log->info = "NONAKTIF ID = ".$id;
        $log->table_name = "Sample_matrix";
        $log->user_id = Auth::user()->id;
        $log->save();

        $ref = Ref_test::where('matrix_id','=',$id)->get();
        foreach ($ref as $key => $value) {
            $value->nonaktif = 1;
            $value->save();

            $log2 = new Log;
            $log2->info = "NONAKTIF ID = ".$value->id;
            $log2->table_name = "Ref_test";
            $log2->user_id = Auth::user()->id;
            $log2->save();
        }

        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Data berhasil dihapus']
        ]);
    }

    public function user_delete($id){
        $user= USer::find($id);
        $param_user = Parameter_sample::where('user_id','=',$id)->get();
        foreach ($param_user as $key => $value) {
            $analis = Assignment::where('parameter_id','=',$value->parameter_id)->where('user_id','!=',$id)->pluck('user_id');
                $temp=1000000;
                $analis_id = 0;
                foreach ($analis as $keyss => $valu) {
                    $analis_sample[$keyss] = Parameter_sample::where('user_id','=',$valu)->where('status_parameter','!=','OK (TM)')->pluck('user_id');
                    $count[$keyss] = $analis_sample[$keyss]->count();
                    if($count[$keyss] <= $temp){
                        $temp = $count[$keyss];
                        $analis_id = $analis_sample[$keyss]->first();
                    }
                }
                if ($temp == 0){
                    $analis_id = Assignment::where('parameter_id','=',$value->parameter_id)->pluck('user_id')->sortByDesc('id')->first();
                }

                $value->user_id = $analis_id;
                $value->save();
        }

        User::where('id','=',$id)->update([
            'nonaktif' => 1
        ]);

        $log = new Log;
        $log->info = "NONAKTIF ID = ".$id;
        $log->table_name = "USERS";
        $log->user_id = Auth::user()->id;
        $log->save();

        $notif = Notification::where('destination_id','=',$id)->delete();

        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'User berhasil dihapus']
        ]);
    }

    public function user_edit_password($id){
        return view('admin.reset_password')->with([
            'id'    => $id
        ]);
    }

    public function user_edit_password_save($id, Request $request){
        $this->validate($request, [
            'password' => 'required|min:6|confirmed'
        ]);

        $user = User::find($id);
        $user->password = bcrypt($request->input('password'));
        $user->save();

        $log = new Log;
        $log->info = "Change Password ID = ".$id;
        $log->table_name = "USERS";
        $log->user_id = Auth::user()->id;
        $log->save();
        
        return $this->user_edit($id)->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => 'Password berhasil direset']
        ]);
    }
}
