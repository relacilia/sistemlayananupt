<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sample;
use App\Job;
use App\Log;
use App\Notification;
use App\Result;
use App\Parameter;
use App\Assignment;
use App\Parameter_sample;
use Auth;
use DB;
use Carbon\Carbon;

class SupervisorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notif = Notification::where('destination_id','=',Auth::user()->id)->get(); 
        $param = Assignment::where('user_id','=',Auth::user()->id)->orderBy('parameter_id', 'asc')->get();
        $sample = Sample::where('status','=','Onprogress (AN)')
                            ->Orwhere('status','=','Onprogress (SV)')
                            ->get();
        $sample = $sample->where('nonaktif',0);
        foreach ($sample as $value) {
            $value->job->date_recent;
        }

        return view('supervisor.home')->with([
            'sample'        => $sample,
            'param'         => $param,
            'menu_active'   => null,
            'notif'         => $notif
        ]);
    }

    public function para_sample_list($id)
    {
        $notif = Notification::where('destination_id','=',Auth::user()->id)->get();
        $paras = Parameter_sample::where('sample_id','=',$id)->get();
        $param_analist = Assignment::where('user_id','=',Auth::user()->id)->orderBy('parameter_id', 'asc')->get();
        $hasil[] = null;
        foreach ($paras as $key => $value) {
            $value->parameter->name;
            $value->user->name;
            $hasil[$key] = Result::where('para_sample_id','=', $value->id)->get();
        }

        return view('supervisor.parasamplelist')->with([
            'paras'    => $paras,
            'hasil'     => $hasil,
            'param'     => $param_analist,
            'menu_active' => null,
            'notif'         => $notif
        ]);
    }

    public function acceptation($id)
    {
        $paras = Parameter_sample::where('id','=',$id)->update(['status_parameter' => 'Onprogress (TM)']);

        $log = new Log;
        $log->info = "UPDATE ID ".$id;
        $log->table_name = "Parameter_sample";
        $log->user_id = Auth::user()->id;
        $log->save();

        $result = Result::where('para_sample_id','=', $id)->orderBy('created_at','desc')->first();
        $result->status_result = 'OK (SV)';
        $result->save();

        $log2 = new Log;
        $log2->info = "UPDATE ID ".$result->id;
        $log2->table_name = "Result";
        $log2->user_id = Auth::user()->id;
        $log2->save();

        $pesanAlert = 'Hasil analisis diterima';
        return back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert],
        ]);
    }

    public function revision($id)
    {
        $parameter  = Parameter_sample::find($id);
        $pid        = $parameter->parameter_id;
        $date       = $parameter->sample->job->date_recent;
        $notif      = new Notification;
        $notif->para_sample_id = $parameter->id;
        $notif->parameter_id = $pid;
        $notif->source_id = Auth::user()->id;
        $notif->destination_id = $parameter->user_id;
        $notif->link = ($pid.'/'.$date);
        $notif->save();
        $paras = Parameter_sample::where('id','=',$id)->update(['status_parameter' => 'Revision (SV)']);
        $log = new Log;
        $log->info = "UPDATE ID ".$id;
        $log->table_name = "Parameter_sample";
        $log->user_id = Auth::user()->id;
        $log->save();

        $result = Result::where('para_sample_id','=', $id)->orderBy('created_at','desc')->first();
        $result->status_result = 'revision (SV)';
        $result->save();

        $log2 = new Log;
        $log2->info = "UPDATE ID ".$result->id;
        $log2->table_name = "Result";
        $log2->user_id = Auth::user()->id;
        $log2->save();
        
        $pesanAlert = 'Hasil analisis tidak diterima';
        return back()->with([
            'alert'         => [    'type'      => 'warning', 
                                    'message'   => $pesanAlert],
        ]);
    }
}
