<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sample_matrix extends Model
{
    protected $table = 'sample_matrix';

    protected $fillable = [
        'name'
    ];
}
