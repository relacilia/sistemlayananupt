<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $table = 'assignment';

    protected $fillable = [
        'user_id', 'parameter_id'
    ];

    public function parameter() {
        return $this->belongsTo('App\Parameter','parameter_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
