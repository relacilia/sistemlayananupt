<div class="modal fade" id="myUpload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Upload Result</h4>
      </div>
      <div class="modal-body">
         <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/analyst/uploadExcelAnalis') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
            <div class="row">
                <p style="text-align:center;padding:5px; background:azure"><input type="file" name="result" required>
                <button type="submit" class="btn btn-primary">UPLOAD</button></p>
            </div>
      </div>
    </div>
  </div>
</div>