<!-- Modal menghapus Job -->
<div class="modal fade" id="job_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p>Apakah Anda yakin ingin menghapus Job Number {{$job->id}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
        <a href="{{ url('/admin/joblist/samplelist/'.$job->id.'/delete') }}"><button type="button" class="btn btn-danger">Delete</button></a>
      </div>
    </div>
  </div>
</div>