<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Customer</h4>
      </div>
      <div class="modal-body">
         <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="customerlist/add" enctype="multipart/form-data">
          {{ csrf_field() }}
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                    <!-- part -->
                    <div class="col-md-3">
                        <p style="margin-top: 8px">Customer Name <span style="color:red">*</span></p>
                    </div>
                    <div class="col-md-9" >
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="" required>

                          @if ($errors->has('name'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('name') }}</strong>
                              </span>
                          @endif   
                        </div>
                    </div>
                    <!-- endpart -->
                </div>
                <div class="row">
                    <!-- part -->
                    <div class="col-md-3">
                        <p style="margin-top: 8px">Address <span style="color:red">*</span></p>
                    </div>
                    <div class="col-md-9" >
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                          <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" placeholder="" required>

                          @if ($errors->has('address'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('address') }}</strong>
                              </span>
                          @endif   
                        </div>
                    </div>
                    <!-- endpart -->
                </div>
                <div class="row">
                    <!-- part -->
                    <div class="col-md-3">
                        <p style="margin-top: 8px">City <span style="color:red">*</span></p>
                    </div>
                    <div class="col-md-9" >
                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                          <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}" placeholder="" required>

                          @if ($errors->has('city'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('city') }}</strong>
                              </span>
                          @endif   
                        </div>
                    </div>
                    <!-- endpart -->
                </div>
                 <div class="row">
                    <!-- part -->
                    <div class="col-md-3">
                        <p style="margin-top: 8px">Phone<span style="color:red">*</span></p>
                    </div>
                    <div class="col-md-9" >
                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                          <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="" required>

                          @if ($errors->has('phone'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('phone') }}</strong>
                              </span>
                          @endif   
                        </div>
                    </div>
                    <!-- endpart -->
                </div>
                <div class="row">
                    <!-- part -->
                    <div class="col-md-3">
                        <p style="margin-top: 8px">Fax Number</p>
                    </div>
                    <div class="col-md-9" >
                        <div class="form-group{{ $errors->has('fax') ? ' has-error' : '' }}">
                          <input id="fax" type="text" class="form-control" name="fax" value="{{ old('fax') }}" placeholder="">

                          @if ($errors->has('fax'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('fax') }}</strong>
                              </span>
                          @endif   
                        </div>
                    </div>
                    <!-- endpart -->
                </div>
              </div>
            </div>
      </div>
      <p style="padding-left:15px;font-weight:bold"><span style="color:red;">*</span> Harap diisi</p>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::button('<span class="middle"> Simpan</span>', array('class' => 'btn btn-primary','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
      </div>
    </div>
  </div>
</div>