<!-- Modal menghapus Job -->
<div class="modal fade" id="matrix_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p>Apakah Anda yakin ingin menghapus Matrix {{$value->name}} ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
        <a href="{{ url('/admin/matrix/delete/'.$value->id) }}"><button type="button" class="btn btn-danger">Delete</button></a>
      </div>
    </div>
  </div>
</div>