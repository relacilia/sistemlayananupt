<div class="modal fade" id="matrixModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Parameter</h4>
      </div>
      <div class="modal-body">
         <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/admin/parameterlist/new/save') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                    <!-- part -->
                    <div class="col-md-3">
                        <p style="margin-top: 8px">Parameter Name <span style="color:red">*</span></p>
                    </div>
                    <div class="col-md-9" >
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="" required>

                          @if ($errors->has('name'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('name') }}</strong>
                              </span>
                          @endif   
                        </div>
                    </div>
                    <!-- endpart -->
                </div>
                <div class="row">
                    <!-- part -->
                    <div class="col-md-3">
                        <p style="margin-top: 8px">Sampling Procedure<span style="color:red">*</span></p>
                    </div>
                    <div class="col-md-9" >
                        <div class="form-group{{ $errors->has('procedure') ? ' has-error' : '' }}">
                          {{Form::select('procedure',$procedure, null, ['class' => 'form-control', 'id' => 'procedure'])}}

                          @if ($errors->has('procedure'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('procedure') }}</strong>
                              </span>
                          @endif   
                        </div>
                    </div>
                    <!-- endpart -->
                </div>
                <div class="row">
                    <!-- part -->
                    <div class="col-md-3">
                        <p style="margin-top: 8px">Standard<span style="color:red">*</span></p>
                    </div>
                    <div class="col-md-9" >
                        <div class="form-group{{ $errors->has('standard') ? ' has-error' : '' }}">
                          <input id="standard" type="text" class="form-control" name="standard" value="{{ old('standard') }}" placeholder="" required>

                          @if ($errors->has('standard'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('standard') }}</strong>
                              </span>
                          @endif   
                        </div>
                    </div>
                    <!-- endpart -->
                </div>
                <div class="row">
                    <!-- part -->
                    <div class="col-md-3">
                        <p style="margin-top: 8px">Price<span style="color:red">*</span></p>
                    </div>
                    <div class="col-md-9" >
                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                          <input id="price" type="text" class="form-control" name="price" value="{{ old('price') }}" placeholder="" required>

                          @if ($errors->has('price'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('price') }}</strong>
                              </span>
                          @endif   
                        </div>
                    </div>
                    <!-- endpart -->
                </div>
                <div class="row">
                    <!-- part -->
                    <div class="col-md-3">
                        <p style="margin-top: 8px">Accreditation<span style="color:red">*</span></p>
                    </div>
                    <div class="col-md-9" >
                        <div class="form-group{{ $errors->has('accreditation') ? ' has-error' : '' }}">
                          <select name="accreditation" id="accreditation">
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                          </select>

                          @if ($errors->has('accreditation'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('accreditation') }}</strong>
                              </span>
                          @endif   
                        </div>
                    </div>
                    <!-- endpart -->
                </div>
                <div class="row">
                    <!-- part -->
                    <div class="col-md-3">
                        <p style="margin-top: 8px">MDL<span style="color:red">*</span></p>
                    </div>
                    <div class="col-md-9" >
                        <div class="form-group{{ $errors->has('mdl') ? ' has-error' : '' }}">
                          <input id="mdl" type="text" class="form-control" name="mdl" value="{{ old('mdl') }}" placeholder="" required>

                          @if ($errors->has('mdl'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('mdl') }}</strong>
                              </span>
                          @endif   
                        </div>
                    </div>
                    <!-- endpart -->
                </div>
              </div>
            </div>
      </div>
      <p style="padding-left:15px;font-weight:bold"><span style="color:red;">*</span> Harap diisi</p>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        {!! Form::button('<span class="middle"> Simpan</span>', array('class' => 'btn btn-default','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#' + 'procedure').select2({
        placeholder: 'Choose One',
    });
  $('#' + 'accreditation').select2({
        placeholder: 'Choose One',
    });
</script>