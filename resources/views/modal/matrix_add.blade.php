<div class="modal fade" id="matrixModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Matrix</h4>
      </div>
      <div class="modal-body">
         <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/admin/matrix/new/save') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                    <!-- part -->
                    <div class="col-md-3">
                        <p style="margin-top: 8px">Matrix Name <span style="color:red">*</span></p>
                    </div>
                    <div class="col-md-9" >
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="" required>

                          @if ($errors->has('name'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('name') }}</strong>
                              </span>
                          @endif   
                        </div>
                    </div>
                    <!-- endpart -->
                </div>
                <div class="row">
                    <!-- part -->
                    <div class="col-md-3">
                        <p style="margin-top: 8px">Alias Name<span style="color:red">*</span></p>
                    </div>
                    <div class="col-md-9" >
                        <div class="form-group{{ $errors->has('alias') ? ' has-error' : '' }}">
                          <input id="alias" type="text" class="form-control" name="alias" value="{{ old('alias') }}" placeholder="" required>

                          @if ($errors->has('alias'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('alias') }}</strong>
                              </span>
                          @endif   
                        </div>
                    </div>
                    <!-- endpart -->
                </div>
              </div>
            </div>
      </div>
      <p style="padding-left:15px;font-weight:bold"><span style="color:red;">*</span> Harap diisi</p>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        {!! Form::button('<span class="middle"> Simpan</span>', array('class' => 'btn btn-default','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
      </div>
    </div>
  </div>
</div>