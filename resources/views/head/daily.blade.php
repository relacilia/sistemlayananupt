@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading center"><h3>Laporan Harian</h3></div>
                <div class="panel-body">
                  <div class="row">
                    @if(Auth::user()->hasRole('Head_of_unit'))
                      <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/head/harian/detail') }}" enctype="multipart/form-data">
                    @elseif(Auth::user()->hasRole('Section_head'))
                    <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/kasi/harian/detail') }}" enctype="multipart/form-data">
                    @elseif(Auth::user()->hasRole('Viewer'))
                    <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/viewer/harian/detail') }}" enctype="multipart/form-data">
                    @endif
                      {{ csrf_field() }}
                        <div class="col-md-6" style="margin-bottom:10px">{{ Form::select('month', $month,$curr_month, []) }} {{ Form::selectYear('year', 2017, \Carbon\Carbon::now()->year) }} <button>CARI</button></div>
                      </form>
                    <div class="col-md-6 right"> <button>DOWNLOAD PDF</button></div>
                  </div>
                  <table id="harian" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Date Recent</th>
                        <th>Jobs</th>
                        <th>Samples</th>
                        <th>Total Harga</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i = 1; ?>
                      @foreach($date as $key => $value)
                      <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $value }}</td>
                        <td>{{ $job[$key] }}</td>
                        <td>{{ $sample[$key] }}</td>
                        <td>{{ $price[$key] }}</td>
                      </tr>
                      <?php $i++; ?>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('#harian').DataTable();
} );
</script>
@endsection
