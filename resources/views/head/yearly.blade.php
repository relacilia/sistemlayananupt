@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading center"><h3>Laporan Harian</h3></div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6 right"  style="margin-bottom:10px"> <button>DOWNLOAD PDF</button></div>
                  </div>
                  <table id="harian" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tahun</th>
                        <th>Jumlah Job</th>
                        <th>Jumlah Sample</th>
                        <th>Total Harga</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i = 1; ?>
                      @foreach($date as $key => $value)
                      <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $value }}</td>
                        <td>{{ $job[$key] }}</td>
                        <td>{{ $sample[$key] }}</td>
                        <td>{{ $price[$key] }}</td>
                      </tr>
                      <?php $i++; ?>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('#harian').DataTable();
} );
</script>
@endsection
