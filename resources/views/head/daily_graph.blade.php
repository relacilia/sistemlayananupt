@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
              @if(Auth::user()->hasRole('Head_of_unit'))
                <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/head/harian/detail') }}" enctype="multipart/form-data">
              @elseif(Auth::user()->hasRole('Section_head'))
                <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/kasi/harian/detail') }}" enctype="multipart/form-data">
              @elseif(Auth::user()->hasRole('Viewer'))
                <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/viewer/harian/detail') }}" enctype="multipart/form-data">
              @endif
                  {{ csrf_field() }}
                  <div class="right" style="margin:10px"><button type="submit" class="btn btn-primary">Lihat Detail</button></div>
                  <input type="hidden" name="month" value="{{ \Carbon\Carbon::now()->month }}">
                  <input type="hidden" name="year" value="{{ \Carbon\Carbon::now()->year }}">
                </form>
                <div class="panel-body center">
                  <h3>Taksiran Pemasukan Harian</h3> 
                  <div id="price"></div>
                  <h3>Jumlah Job dan Sample Harian</h3>
                  <div id="job"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
Morris.Area({
  element: 'price',
  data: [
    @foreach($date as $key => $value)
    { y: '{{$value}}', a: {{$price[$key]}}},
    @endforeach
  ],
  xkey: 'y',
  ykeys: ['a'],
  labels: ['Total Harian (Rupiah)']
});

Morris.Area({
  element: 'job',
  data: [
    @foreach($date as $key => $value)
    { y: '{{$value}}', a: {{$sample[$key]}}, b: {{$job[$key]}}},
    @endforeach
  ],
  xkey: 'y',
  ykeys: ['a', 'b'],
  labels: ['Jumlah Sample', 'Jumlah Job']
});
</script>
@endsection
