@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="right" style="margin:10px">
                  @if(Auth::user()->hasRole('Head_of_unit'))
                    <a href="{{ url('/head/tahunan/detail') }}">
                  @elseif(Auth::user()->hasRole('Section_head'))
                    <a href="{{ url('/kasi/tahunan/detail') }}">
                  @elseif(Auth::user()->hasRole('Viewer'))
                    <a href="{{ url('/viewer/tahunan/detail') }}">
                  @endif
                      <button type="submit" class="btn btn-primary">Lihat Detail</button>
                    </a>
                </div>
                <div class="panel-body center">
                  <h3>Taksiran Pemasukan Tahunan</h3> 
                  <div id="price"></div>
                  <h3>Jumlah Job dan Sample Tahunan</h3>
                  <div id="job"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
Morris.Area({
  element: 'price',
  data: [
    @foreach($date as $key => $value)
    { y: '{{$value}}', a: {{$price[$key]}}},
    @endforeach
  ],
  xkey: 'y',
  ykeys: ['a'],
  labels: ['Total Harian (Rupiah)']
});

Morris.Area({
  element: 'job',
  data: [
    @foreach($date as $key => $value)
    { y: '{{$value}}', a: {{$sample[$key]}}, b: {{$job[$key]}}},
    @endforeach
  ],
  xkey: 'y',
  ykeys: ['a', 'b'],
  labels: ['Jumlah Sample', 'Jumlah Job']
});
</script>
@endsection
