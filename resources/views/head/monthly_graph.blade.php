@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
              @if(Auth::user()->hasRole('Head_of_unit'))
                <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/head/bulanan/detail') }}" enctype="multipart/form-data">
              @elseif(Auth::user()->hasRole('Section_head'))
              <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/kasi/bulanan/detail') }}" enctype="multipart/form-data">
              @elseif(Auth::user()->hasRole('Viewer'))
              <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/viewer/bulanan/detail') }}" enctype="multipart/form-data">
              @endif
                  {{ csrf_field() }}
                  <div class="right" style="margin:10px"><button type="submit" class="btn btn-primary">Lihat Detail</button></div>
                  <input type="hidden" name="year" value="{{ \Carbon\Carbon::now()->year }}">
                </form>
                <div class="panel-body center">
                  <h3>Taksiran Pemasukan Per Bulan</h3> 
                  <div id="price"></div>
                  <h3>Jumlah Job dan Sample Per Bulan</h3>
                  <div id="job"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
Morris.Area({
  element: 'price',
  data: [
    { y: '{{$year}}-01', a: {{$price[0]}}, },
    { y: '{{$year}}-02', a: {{$price[1]}}, },
    { y: '{{$year}}-03', a: {{$price[2]}}, },
    { y: '{{$year}}-04', a: {{$price[3]}}, },
    { y: '{{$year}}-05', a: {{$price[4]}}, },
    { y: '{{$year}}-06', a: {{$price[5]}}, },
    { y: '{{$year}}-07', a: {{$price[6]}}, },
    { y: '{{$year}}-08', a: {{$price[7]}}, },
    { y: '{{$year}}-09', a: {{$price[8]}}, },
    { y: '{{$year}}-10', a: {{$price[9]}}, },
    { y: '{{$year}}-11', a: {{$price[10]}}, },
    { y: '{{$year}}-12', a: {{$price[11]}}, },
  ],
  xkey: 'y',
  ykeys: ['a'],
  labels: ['Total Per Bulan (Rupiah)']
});

Morris.Area({
  element: 'job',
  data: [
    { y: '{{$year}}-01', a: {{$sample[0]}}, b: {{$job[0]}} },
    { y: '{{$year}}-02', a: {{$sample[1]}}, b: {{$job[1]}} },
    { y: '{{$year}}-03', a: {{$sample[2]}}, b: {{$job[2]}} },
    { y: '{{$year}}-04', a: {{$sample[3]}}, b: {{$job[3]}} },
    { y: '{{$year}}-05', a: {{$sample[4]}}, b: {{$job[4]}} },
    { y: '{{$year}}-06', a: {{$sample[5]}}, b: {{$job[5]}} },
    { y: '{{$year}}-07', a: {{$sample[6]}}, b: {{$job[6]}} },
    { y: '{{$year}}-08', a: {{$sample[7]}}, b: {{$job[7]}} },
    { y: '{{$year}}-09', a: {{$sample[8]}}, b: {{$job[8]}} },
    { y: '{{$year}}-10', a: {{$sample[9]}}, b: {{$job[9]}} },
    { y: '{{$year}}-11', a: {{$sample[10]}}, b: {{$job[10]}} },
    { y: '{{$year}}-12', a: {{$sample[11]}}, b: {{$job[11]}} },
  ],
  xkey: 'y',
  ykeys: ['a', 'b'],
  labels: ['Total Sample', 'Total Job']
});
</script>
@endsection
