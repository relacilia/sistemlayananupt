@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                    <div class="col-md-6">
                        <h3>Registration</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        <nav>
                          <ol class="cd-multi-steps text-bottom count">
                            <li class="current"><a href="#0"></a></li>
                            <li><em></em></a></li>
                            <li><em></em></li>
                            <li><em></em></li>
                        </ol>
                        </nav>
                    </div>
                    </div>
                </div>
                <div class="panel-body">
                <form role="form" data-toggle="validator" class="form-horizontal" method="GET" action="{{ url('/front/step2') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    <div class="row">
                         <div class="col-md-6">
                            <div class="row">
                                <!-- part -->
                                <div class="col-md-3">
                                    <p style="margin-top: 8px">ID Customer</p>
                                </div>
                                <div class="col-md-8" style="padding-right:0px">
                                    <div class="form-group{{ $errors->has('id_customer') ? ' has-error' : '' }}">
                                            {{Form::select('id_customer',$idcustomer, null, ['class' => 'form-control', 'id' => 'id_customer'])}}
                                            @if ($errors->has('id_customer'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('id_customer') }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </div>
                                <div class="col-md-1" style="padding-left:0px">
                                    <div class="form-group{{ $errors->has('new') ? ' has-error' : '' }}">
                                             <button type="button" style="height: 36px;width: 32px;" data-toggle="modal" data-target="#myModal" title="Add Customer">+</button>
                                    </div>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                <!-- part -->
                                <div class="col-md-3">
                                    <p style="margin-top: 8px">Customer</p>
                                </div>
                                <div class="col-md-9" >
                                    <div class="form-group{{ $errors->has('customer') ? ' has-error' : '' }}" id="name_cust">
                                             {{Form::select('customer',$customer, null, ['class' => 'form-control', 'id' => 'customer'])}}

                                            @if ($errors->has('customer'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('customer') }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-3">
                                    <p style="margin-top: 8px">Address</p>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                            <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" placeholder="" required>

                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                <!-- part -->
                                <div class="col-md-3">
                                    <p style="margin-top: 8px">City</p>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                            <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}" placeholder="" required>

                                            @if ($errors->has('city'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-3">
                                    <p style="margin-top: 8px">Phone Number</p>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                            <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="" required>

                                            @if ($errors->has('phone'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-3">
                                    <p style="margin-top: 8px">Fax Number</p>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group{{ $errors->has('fax') ? ' has-error' : '' }}">
                                            <input id="fax" type="text" class="form-control" name="fax" value="{{ old('fax') }}" placeholder="" required>

                                            @if ($errors->has('fax'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('fax') }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-3">
                                    <p style="margin-top: 8px">Attention</p>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group{{ $errors->has('attention') ? ' has-error' : '' }}">
                                            <input id="attention" type="text" class="form-control" name="attention" value="{{ old('attention') }}" placeholder="" required>

                                            @if ($errors->has('attention'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('attention') }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </div>
                                <!-- endpart -->
                            </div>
                         </div>
                         <div class="col-md-6">
                             <div class="row">
                                <!-- part -->
                                <div class="col-md-4">
                                    <p style="margin-top: 8px">Job Number</p>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group{{ $errors->has('job_number') ? ' has-error' : '' }}">
                                            <input id="job_number" type="text" class="form-control" value="{{ $job }}" placeholder="{{ $job }}" disabled>
                                             <input type="hidden" name="job_number" value="{{ $job }}" placeholder="{{ $job }}">

                                            @if ($errors->has('job_number'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('job_number') }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-4">
                                    <p style="margin-top: 8px">Number of Sample</p>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group{{ $errors->has('number_sample') ? ' has-error' : '' }}">
                                            <input id="number_sample" type="text" class="form-control" name="number_sample" value="{{ old('number_sample') }}" placeholder="" required>

                                            @if ($errors->has('number_sample'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('number_sample') }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-4">
                                    <p style="margin-top: 8px">Date Received</p>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group{{ $errors->has('datepicker') ? ' has-error' : '' }}">
                                            <input id="datepicker" type="text" class="form-control" name="datepicker" value="{{ old('datepicker',$dt['tgl'] ) }}" placeholder="" required>

                                            @if ($errors->has('datepicker'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('datepicker') }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-4">
                                    <p style="margin-top: 8px">Time Received</p>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
                                            <input id="time" type="text" class="form-control" name="time" value="{{ old('time',$dt['jam'] ) }}" placeholder="" required>

                                            @if ($errors->has('time'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('time') }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-4">
                                    <p style="margin-top: 8px">Interval Analysis</p>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group{{ $errors->has('interval') ? ' has-error' : '' }}">
                                            <input id="interval" type="text" class="form-control" name="interval" value="{{ old('interval',$dt['itgl']) }}" placeholder="" required>

                                            @if ($errors->has('interval'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('interval') }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-4">
                                    <p style="margin-top: 8px">Sampled by</p>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group{{ $errors->has('by') ? ' has-error' : '' }}">
                                            <input id="by" type="text" class="form-control" name="by" value="{{ old('by') }}" placeholder="" required>

                                            @if ($errors->has('by'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('by') }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-4">
                                    <p style="margin-top: 8px">Location</p>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                            <input id="location" type="text" class="form-control" name="location" value="{{ old('location') }}" placeholder="" required>

                                            @if ($errors->has('location'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('location') }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </div>
                                <!-- endpart -->
                            </div>
                         </div>
                    </div>
                    <br>
                     <div class="col-md-12" style="text-align:right">
                     {!! Form::button('<span class="middle"> Next >> </span>', array('class' => 'btn btn-default','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('modal.customer_add')

<script type="text/javascript">
  $('#' + 'customer').select2({
        placeholder: '--',
    });
</script>
<script>
   $( function() {
    $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );
   $( function() {
    $( "#interval" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );
</script>

<script type="text/javascript">
    // If I use POST from the ajax call, I got a TokenMismatch Exception, so if you add the below script and also the meta tag at the head of your view, it will work.
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $(document).ready(function($)
    {
       $('#customer').change(function()
       {
        $.get("{!! route('dataCustom2') !!}", { option: $(this).val() }, 
          function(data) {  
            $.each(data, function(key, value) {   
                 $('#' + 'address').val(data.address);
                 $('#' + 'phone').val(data.phone);
                 $('#' + 'city').val(data.city);
                 $('#' + 'fax').val(data.fax);
                 $('#' + 'id_customer').val(data.id_customer-1).prop('selected', true);
            });
         });
      });

       $('#id_customer').change(function()
       {
        $.get("{!! route('dataCustom2') !!}", { option: +$(this).val()+1 }, 
          function(data) {  
            $.each(data, function(key, value) {   
                 $('#' + 'address').val(data.address);
                 $('#' + 'phone').val(data.phone);
                 $('#' + 'city').val(data.city);
                 $('#' + 'fax').val(data.fax);
                 $('#' + 'customer').val(data.id_customer).change();
            });
         });
      });

        $('#datepicker').change(function()
       {
            $.get("{!! route('getDate') !!}", { option: $(this).val() }, 
              function(data) {    
                     $('#' + 'interval').val(data);
         });
      });

    });
</script>
@endsection
