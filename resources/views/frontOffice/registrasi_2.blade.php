@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="row">
                    <div class="col-md-6">
                        <h3>Registrasi</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        <nav>
                          <ol class="cd-multi-steps text-bottom count">
                            <li class="visited"><a href="#0"></a></li>
                            <li class="current"><em></em></a></li>
                            <li><em></em></li>
                            <li><em></em></li>
                        </ol>
                        </nav>
                    </div>
                    </div>
                </div>

                <div class="panel-body">
                  <form role="form" data-toggle="validator" class="form-horizontal" method="GET" action="{{ url('/front/step3') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    <div class="row">
                     <div class="col-md-12">
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th rowspan="2" id="lab">Lab Sample ID</th>
                                <th rowspan="2">Customer Sample ID</th>
                                <th rowspan="2">Date Sampled</th>
                                <th rowspan="2">Time Sampled</th>
                                <th rowspan="2">Sample Matrix</th>
                                <th rowspan="2">Ref Procedur</th>
                                <th rowspan="2">Ref Test</th>
                                <th rowspan="2" id="sub" class="none">Sub Ref Test</th>
                              <!--   <th rowspan="2">Detail Ref Test</th> -->
                                <th colspan='6'>Coordinate</th>
                            </tr>
                            <tr>
                              <th colspan='3'>Coordinate S</th>
                              <th colspan='3'>Coordinate E</th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($kode as $key => $value)
                            <tr>
                                <td style="width:6%">{{$value}}</td><input type="hidden" name="kode_sample[]" value="{{$value}}">
                                <td>
                                   <div class="form-group{{ $errors->has('cus_sample') ? ' has-error' : '' }}">
                                    <input id="cus_sample" type="text" class="form-control" name="cus_sample[{{$key}}]" value="{{ old('cus_sample.'.$key) }}" placeholder="" required>

                                    @if ($errors->has('cus_sample'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('cus_sample') }}</strong>
                                        </span>
                                    @endif   
                                    </div>
                                </td>
                                <td style="width:5%"> 
                                  <div class="form-group{{ $errors->has('date.'.$key) ? ' has-error' : '' }}">
                                   <input class="datepicker form-control" type="text" name="date[{{$key}}]" value="{{ old('date.'.$key) }}" style="height:36px;width:90px" placeholder="" required>
                                    @if ($errors->has('date.'.$key))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('date.'.$key) }}</strong>
                                        </span>
                                    @endif   
                                </td>
                                <td style="width:4%">
                                   <div class="form-group{{ $errors->has('time.'.$key) ? ' has-error' : '' }}">
                                            <input id="time" type="text" class="form-control" name="time[{{$key}}]" value="{{ old('time.'.$key) }}" placeholder="00:00" required>

                                            @if ($errors->has('time.'.$key))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('time.'.$key) }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </td>
                                <td style="width:6%">
                                  <div class="form-group{{ $errors->has('sample_matrix') ? ' has-error' : '' }}">
                                      <select class="form-control matrix" name="sample_matrix[{{$key}}]" id="matrix{{$key}}" required>
                                        <option value=""  selected>Choose One</option>
                                        @foreach($sm as $value)
                                          <option value="{{$value->id}}" @if (old('sample_matrix.'.$key) == $value->id) selected="selected" @endif>{{$value->alias}}</option>
                                        @endforeach
                                      </select>
                                      @if ($errors->has('sample_matrix.'.$key))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('sample_matrix.'.$key) }}</strong>
                                          </span>
                                      @endif   
                                    </div>
                                </td>
                                <td style="width:12%">
                                  <div class="form-group{{ $errors->has('procedure') ? ' has-error' : '' }}">
                                    <select class="form-control procedure" name="procedure[]" id="procedure{{$key}}">
                                      <option value="{{ old('procedure.'.$key) }}" selected>{{ old('procedure.'.$key) }}</option>
                                    </select>
                                     @if ($errors->has('procedure'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('procedure') }}</strong>
                                                </span>
                                            @endif 
                                </td>
                                 <td style="width:15%">
                                    <div class="form-group{{ $errors->has('ref_test') ? ' has-error' : '' }}">
                                    <select class="form-control ref" name="ref_test[]" id="ref{{$key}}">
                                      <option value="{{ old('ref_test.'.$key) }}" selected>{{ old('ref_test.'.$key) }}</option>
                                    </select>
                                     @if ($errors->has('ref_test'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('ref_test') }}</strong>
                                                </span>
                                            @endif 
                                </td>
                                
                                <td class="suby none">
                                    <div class="form-group{{ $errors->has('sub_type') ? ' has-error' : '' }}">
                                    {{Form::select('sub_type[]',['-'=>'Pilih Sub Baku Mutu'], null, ['class' => 'form-control', 'id' => 'sub_type'.$key, 'class' => 'sub_type'])}}
                                     @if ($errors->has('sub_type'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('sub_type') }}</strong>
                                                </span>
                                            @endif 
                                </td>
                                <!-- <td>
                                   <div class="form-group{{ $errors->has('detail_type') ? ' has-error' : '' }}">
                                    {{Form::select('detail_type[]',[], null, ['class' => 'form-control', 'id' => 'detail_type'.$key, 'class' => 'detail_type'])}}
                                     @if ($errors->has('detail_type'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('detail_type') }}</strong>
                                                </span>
                                            @endif 
                                </td> -->

                                <td style="width:4.5%; padding: 8px 2px">
                                  <div class="form-group{{ $errors->has('s1.'.$key) ? ' has-error' : '' }}">
                                          <input id="s1" type="text" class="form-control center pressed" name="s1[{{$key}}]" value="{{ old('s1.'.$key) }}" placeholder="...°" required>
                                          @if ($errors->has('s1.'.$key))
                                              <span class="help-block">
                                                  <strong>{{ $errors->first('s1.'.$key) }}</strong>
                                              </span>
                                          @endif   
                                  </div>
                                </td>
                                <td style="width:4.5%; padding: 8px 2px">
                                   <div class="form-group{{ $errors->has('s2.'.$key) ? ' has-error' : '' }}">
                                            <input id="s2" type="text" class="form-control center" name="s2[{{$key}}]" value="{{ old('s2.'.$key) }}" placeholder="...'" required>

                                            @if ($errors->has('s2.'.$key))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('s2.'.$key) }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </td>
                                <td style="width:4.5%; padding: 8px 2px">
                                   <div class="form-group{{ $errors->has('s3.'.$key) ? ' has-error' : '' }}">
                                            <input id="s3" type="text" class="form-control center" name="s3[{{$key}}]" value="{{ old('s3.'.$key) }}" placeholder='..."' required>

                                            @if ($errors->has('s3.'.$key))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('s3.'.$key) }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </td>
                                <td style="width:4.5%; padding: 8px 2px">
                                   <div class="form-group{{ $errors->has('e1.'.$key) ? ' has-error' : '' }}">
                                            <input id="e1" type="text" class="form-control center" name="e1[{{$key}}]" value="{{ old('e1.'.$key) }}" placeholder="...°" required>

                                            @if ($errors->has('e1.'.$key))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('e1.'.$key) }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </td>
                                <td style="width:4.5%; padding: 8px 2px">
                                   <div class="form-group{{ $errors->has('e2.'.$key) ? ' has-error' : '' }}">
                                            <input id="e2" type="text" class="form-control center" name="e2[{{$key}}]" value="{{ old('e2.'.$key) }}" placeholder="...'" required>

                                            @if ($errors->has('e2.'.$key))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('e2.'.$key) }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </td>
                                <td style="width:4.5%; padding: 8px 2px">
                                   <div class="form-group{{ $errors->has('e3.'.$key) ? ' has-error' : '' }}">
                                            <input id="e3" type="text" class="form-control center" name="e3[{{$key}}]" value="{{ old('e3.'.$key) }}" placeholder='..."' required>

                                            @if ($errors->has('e3.'.$key))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('e3.'.$key) }}</strong>
                                                </span>
                                            @endif   
                                    </div>
                                </td>
                            
                            
                            </tr>
                            @endforeach
                           
                        </tbody>
                    </table>
                </div>
                @foreach($dt as $key => $value)
                <input type="hidden" name="data[{{$key}}]" value="{{$value}}">
                @endforeach
            </div>
            <div class="col-md-12" style="text-align:right">
            {!! Form::button('<span class="middle"> Next >> </span>', array('class' => 'btn btn-default','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
                 </div>
            </div>
            </form>
            </div>
        </div>
    </div>
</div>


<script>
  $( function() {
    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );

</script>

<script type="text/javascript">
  $('.' + 'matrix').select2({
        placeholder: '--',
    });

  $('.' + 'ref').select2({
        placeholder: '--',
    });
  $('.' + 'procedure').select2({
        placeholder: '--',
    });
  $('.' + 'sub_type').select2({
        placeholder: '--',
    });
  $('.' + 'detail_type').select2({
        placeholder: '--',
    });

</script>

@for($i = 0; $i < $dt['number_samples']; $i++)
    <script type="text/javascript">
    // If I use POST from the ajax call, I got a TokenMismatch Exception, so if you add the below script and also the meta tag at the head of your view, it will work.
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    // Script para el dropdown dinámico
    $(document).ready(function($)
    {
      $('#matrix<?php echo ($i) ?>' ).change(function()
      {
        var params = {
          option: $(this).val()
        }

        $('#' + 'ref<?php echo ($i) ?>').val(null);
        $('#' + 'procedure<?php echo ($i) ?>').val(null);

        if($(this).val() == 1){
            $("#sub").show();
            $(".suby").show();
          }

        url  = "{!! route('dropdown') !!}"; 
         $('#' + 'ref<?php echo ($i) ?>').select2({
            placeholder: '--',
            ajax: {
              url: url,
              type: 'GET',
              datatype: 'JSON',
              data: params,
              processResults: function (data) {
                return {
                    results: data
              };
            }
          }
        });

         url2  = "{!! route('dropdown_procedure') !!}"; 
         $('#' + 'procedure<?php echo ($i) ?>').select2({
            placeholder: '--',
            ajax: {
              url: url2,
              type: 'GET',
              datatype: 'JSON',
              data: params,
              processResults: function (data) {
                return {
                    results: data
              };
            }
          }
        });
      });

      $('#ref<?php echo ($i) ?>').change(function(){
        
        
      });

    function unEntity(str){
        return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">");
    }
    });

   
      $('#matrix<?php echo ($i) ?>').focus(function()
                        {
                            //store old value
                          $(this).data('oldValue',$(this).val());}
                        );
      $('#matrix<?php echo ($i) ?>').change(function()
                         {
                            //do something
                          $('#span').text($(this).data('oldValue'));
                            //trigger focus to set new oldValue
                          $(this).trigger('focus');
                         });

</script>
@endfor
@endsection
