@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading center">
                    <h3>Job List</h3>
                </div>
                <div class="panel-body">
                     <div class="col-md-12">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Laboratory ID</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($job as $key => $value)
                                          <tr>
                                            <td>{{$value->id}}</td>
                                            <td><a href="{{ url('/front/certificatelist/detail/'.$value->id)}}"><button>Detail</button></a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
