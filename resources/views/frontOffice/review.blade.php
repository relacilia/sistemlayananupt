@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                   <div class="row">
                    <div class="col-md-6">
                        <h3>Review</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        <nav>
                          <ol class="cd-multi-steps text-bottom count">
                            <li class="visited"><a href="#0"></a></li>
                            <li class="visited"><a></a></li>
                            <li class="visited"><a></a></li>
                            <li class="current"><em></em></li>
                        </ol>
                        </nav>
                    </div>
                    </div>
                </div>
                <div class="panel-body">
                    <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/front/tt') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    <div class="row">
                         <div class="col-md-5">
                            <div class="row">
                                <!-- part -->
                                <div class="col-md-4">
                                    <p >Job Number</p>
                                </div>
                                <div class="col-md-8" style="padding-right:0px">
                                    <p>: {{$data['id']}}</p>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                <!-- part -->
                                <div class="col-md-4">
                                    <p>Customer</p>
                                </div>
                                <div class="col-md-8" style="padding-right:0px">
                                    <p>: {{$cus->name_customer}}</p>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-4">
                                    <p>Attention</p>
                                </div>
                                <div class="col-md-8">
                                     <p>: {{$data['attention']}}</p>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-4">
                                    <p>Sampled by</p>
                                </div>
                                <div class="col-md-8">
                                     <p>: {{$data['sampled_by']}}</p>
                                </div>
                                <!-- endpart -->
                            </div>
                         </div>
                         <div class="col-md-3">
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-6">
                                    <p>Date Received</p>
                                </div>
                                <div class="col-md-6">
                                    <p>: {{$data['date_recent']}}</p>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-6">
                                    <p>Time Received</p>
                                </div>
                                 <div class="col-md-6">
                                    <p>: {{$data['time_recent']}}</p>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-6">
                                    <p>Interval Analysis</p>
                                </div>
                                <div class="col-md-6">
                                    <p>: {{$data['interval']}}</p>
                                </div>
                                <!-- endpart -->
                            </div>
                         </div>
                         <div class="col-md-4">
                            <div class="row">
                                <!-- part -->
                               <!--  <div class="col-md-12" style="text-align:right">
                                    <p><a href="{{ url('/front/downloadPdf/'.$data['id']) }}"><button class="btn btn-danger">Download pdf </button></a>
                                    <a href="{{ url('/front/downloadExcel/'.$data['id']) }}"><button class="btn btn-success">Download Excel xls</button></a></p>
                                </div> -->
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                <!-- part -->
                                <div class="col-md-12" style="text-align:right">
                                    <p><a href="{{ url('/front/home') }}"><button type="button" class="btn ">CANCEL </button></a>
                                    <button type="submit" class="btn btn-default" style="width:100px">SAVE</button></a>
                                </div>
                                <!-- endpart -->
                            </div>
                         </div>
                    </div>
                    <br>
                     <div class="col-md-12" style="text-align:center">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th rowspan="2">Lab Id Sample</th>
                                    <th rowspan="2">Customer Sample ID</th>
                                    <th rowspan="2">Sample Matrix</th>
                                    <th rowspan="2">Date Sample</th>
                                    <th rowspan="2">Time Sample</th>
                                    <th colspan='2'>Coordinate</th>
                                    <th rowspan="2">Ref Test</th>
                                    <th rowspan="2">Parameter</th>
                                    <th colspan='5'>Field Parameters</th>
                                </tr>
                                <tr>
                                    <th>S</th>
                                    <th>E</th>
                                    <th>pH</th>
                                    <th>Suhu</th>
                                    <th>DHL</th>
                                    <th>DO</th>
                                    <th>Sal</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sample_array as $key => $value)
                                <tr>
                                    <td>{{$value['kode_sample']}}</td>
                                    <td>{{$value['name']}}</td>
                                    <td>{{$value['name_matrix']}}</td>
                                    <td>{{$value['date_sampled']}}</td>
                                    <td>{{$value['time_sampled']}}</td>
                                    <td>{{$value['coordinat_S']}}</td>
                                    <td>{{$value['coordinat_E']}}</td>
                                    <td>{{$value['name_ref']}}</td>
                                    <td>
                                        <?php $i = $param[$key]->count(); ?>
                                        @foreach($param[$key] as $keys => $val)
                                            @if($keys == $i-1)
                                            {{$val->name}}
                                            @else
                                            {{$val->name}},
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{$value['ph']}}</td>
                                    <td>{{$value['suhu']}}</td>
                                    <td>{{$value['dhl']}}</td>
                                    <td>{{$value['do']}}</td>
                                    <td>{{$value['sal']}}</td>
                                </tr>
                                    @foreach($value as $keys => $val)
                                        @if($keys != 'param_sampling')
                                        <input type="hidden" name="data_sample[{{$key}}][{{$keys}}]" value="{{$val}}">
                                        @else
                                            @foreach($val as $keyss => $v)
                                                <input type="hidden" name="data_sample[{{$key}}][{{$keys}}][{{$keyss}}]" value="{{$v}}">
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                        @foreach($data as $key => $value)

                          <input type="hidden" name="data[{{$key}}]" value="{{$value}}">
                        @endforeach
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $('#' + 'id_customer').select2({
        placeholder: '--',
    });

  $('#' + 'customer').select2({
        placeholder: '--',
    });
</script>
<script>
   $( function() {
    $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );

  </script>
<script type="text/javascript">
    // If I use POST from the ajax call, I got a TokenMismatch Exception, so if you add the below script and also the meta tag at the head of your view, it will work.
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    // Script para el dropdown dinámico
    $(document).ready(function($)
    {
       $('#customer').change(function()
       {
        $.get("{!! route('dataCustom2') !!}", { option: $(this).val() }, 
          function(data) {  
            var ciudades = $('#id_customer');
            ciudades.empty();
            $.each(data, function(key, value) {   
                 $('#' + 'address').val(data.address);
                 $('#' + 'phone').val(data.phone);
                 ciudades.append($("<option></option>").attr("value.id",key).text(value)); 
            });
         });
      });
    });
</script>
@endsection
