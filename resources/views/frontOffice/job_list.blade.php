@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                    <div class="col-md-6">
                        <h3 style="padding-left:18px">Job List</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        <nav>
                          <ol class="cd-multi-steps text-top">
                        </ol>
                        </nav>
                    </div>
                    </div>
                </div>
                <div class="panel-body">
                     <div class="col-md-12">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Job Number</th>
                                            <th>Customer</th>
                                            <th>Sampled by</th>
                                            <th>Attention</th>
                                            <th>Location</th>
                                            <th>Date Received</th>
                                            <th>Number Samples</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($job as $key => $value)
                                        <tr>
                                          <td>{{$value->id}}</td>
                                          <td>{{$value->customer['name_customer']}}</td>
                                          <td>{{$value->sampled_by}}</td>
                                          <td>{{$value->attention}}</td>
                                          <td>{{$value->location}}</td>
                                          <td style="text-align:center">{{$value->date_recent}}</td>
                                          <td style="text-align:center">{{$value->number_samples}}</td>
                                          <td style="text-align:center">{{$value->status}}</td>
                                          <td style="text-align:center" ><a href="joblist/samplelist/{{$value->id}}"><button>Lihat</button></a></td>
                                        </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
