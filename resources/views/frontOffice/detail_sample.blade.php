@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                    <div class="col-md-6">
                        <h3 style="padding-left:18px">Sample Detail</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        <nav>
                          <ol class="cd-multi-steps text-top">
                            <li class="visited"><a href="{{url('/front/joblist')}}">Jobs</a></li>
                            <li class="visited"><a href="{{url('/front/joblist/samplelist/'.$sample->job_id)}}">Samples</a></li>
                            <li class="current"><a href="#0">Detail</a></li>
                        </ol>
                        </nav>
                    </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table style="width:100%">
                       <tr style="background:aliceblue">
                            <td class="col-md-2">Laboratory ID</td>
                            <td class="col-md-2">: {{$sample->id}}</td>
                            <td class="col-md-2">Sampling Procedure</td>
                            <td class="col-md-2">: {{$sample->procedure_id}}</td>
                            <td class="col-md-2">Coordinat S</td>
                            <td class="col-md-2">: {{$sample->coordinat_S}}</td>
                        </tr>
                        <tr style="background:white">
                            <td class="col-md-2">Cust. Sample ID</td>
                            <td class="col-md-2">: {{$sample->name}}</td>
                            <td class="col-md-2">Preservative</td>
                            <td class="col-md-2">: {{$sample->preservative}}</td>
                            <td class="col-md-2">Coordinat E</td>
                            <td class="col-md-2">: {{$sample->coordinat_E}}</td>
                        </tr>
                        <tr style="background:aliceblue">
                            <td class="col-md-2">Matrix Sample</td>
                            <td class="col-md-2">: {{$sample->matrix_id}}</td>
                            <td class="col-md-2">Data Sampled</td>
                            <td class="col-md-2">: {{$sample->date_sampled}}</td>
                            <td class="col-md-2">Status</td>
                            <td class="col-md-2">: {{$sample->status}}</td>
                        </tr>
                         <tr style="background:white">
                            <td class="col-md-2">Ref Test</td>
                            <td class="col-md-2">: {{$sample->ref_test_id}}</td>
                            <td class="col-md-2">Time Sampled</td>
                            <td class="col-md-2">: {{$sample->time_sampled}}</td>
                            <td class="col-md-2">Price</td>
                            <td class="col-md-2">: Rp.{{$sample->price}},00</td>
                        </tr>
                    </table>
                     <div class="row">
                        <div class="col-md-12" style="text-align:right">
                            @if($sample->status == 'daftar')
                            <a href="{{ url('/front/joblist/edit/'.$sample->id)}}"><button class="btn btn-warning">Edit</button></a>
                            <button class="btn btn-danger" data-toggle="modal" data-target="#myModal">Delete</button>
                            @else
                            <button class="btn btn-warning" disabled>Edit</button>
                            <button class="btn btn-danger" disabled>Delete</button>
                            @endif
                        </div>
                    </div>

                     <div class="col-md-12">
                    <hr>
                    <h4>Parameter</h4>
                        <table>
                            <tbody>
                                <tr>
                                    @foreach($param as $key => $value)
                                       <td style="width:20%">{{$value->parameter->name}}</td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
@endsection
