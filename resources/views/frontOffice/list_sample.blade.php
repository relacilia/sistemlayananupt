@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <div class="row">
                    <div class="col-md-6">
                        <h3 style="padding-left:18px">Samples List</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        <nav>
                          <ol class="cd-multi-steps text-top">
                            <li class="visited"><a href="{{url('/front/joblist')}}">Jobs</a></li>
                            <li class="current"><a href="#0">Samples</a></li>
                        </ol>
                        </nav>
                    </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table style="width:100%">
                        <tr style="background:aliceblue">
                            <td class="col-md-2">Job Number</td>
                            <td class="col-md-3">: {{$job->id}}</td>
                            <td class="col-md-2">Date Received</td>
                            <td class="col-md-2">: {{$job->date_recent}}</td>
                            <td class="col-md-1">Status</td>
                            <td class="col-md-2">: {{$job->status}}</td>
                        </tr>
                        <tr style="background:white">
                            <td class="col-md-2">Customer</td>
                            <td class="col-md-3">: {{$job->customer_id }}</td>
                            <td class="col-md-2">Time Received</td>
                            <td class="col-md-2">: {{$job->time_recent}}</td>
                            <td class="col-md-1">Samples</td>
                            <td class="col-md-2">: {{$job->number_samples}}</td>
                        </tr>
                         <tr style="background:aliceblue">
                            <td class="col-md-2">Attention</td>
                            <td class="col-md-3">: {{$job->attention }}</td>
                            <td class="col-md-2">Interval</td>
                            <td class="col-md-2">: {{$job->interval}}</td>
                            <td class="col-md-1">Price</td>
                            <td class="col-md-2">: Rp.{{$job->price}},00</td>
                        </tr>
                        <tr style="background:white">
                            <td class="col-md-2">Sampled by</td>
                            <td class="col-md-3">: {{$job->sampled_by }}</td>
                            <td class="col-md-2">Location</td>
                            <td class="col-md-2">: {{$job->location}}</td>
                            <td class="col-md-1"></td>
                            <td class="col-md-2"></td>
                        </tr>
                    </table>
                    <div class="row">
                        <div class="col-md-12" style="text-align:right">
                            @if($job->status == 'daftar')
                            <a href="{{ url('/front/joblist/edit/'.$job->id)}}"><button class="btn btn-warning">Edit</button></a>
                            <button class="btn btn-danger" data-toggle="modal" data-target="#myModal">Delete</button>
                            @else
                            <button class="btn btn-warning" disabled>Edit</button>
                            <button class="btn btn-danger" disabled>Delete</button>
                            @endif
                        </div>
                    </div>
                     <div class="col-md-12">
                    <hr>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Laboratory ID</th>
                                            <th>Customer Sample</th>
                                            <th>Matrix</th>
                                            <th>Date Sampled</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($sample as $key => $value)
                                      <tr>
                                        <td style="text-align:center">{{$value->kode_sample}}</td>
                                        <td>{{$value->name}}</td>
                                        <td style="text-align:center">{{$value->matrix->alias}}</td>
                                        <td style="text-align:center">{{$value->date_sampled}}</td>
                                        <td style="text-align:center">{{$value->status}}</td>
                                        <td style="text-align:center"><a href="sample/{{$value->id}}"><button>Lihat</button></a></td>
                                      </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p>Apakah Anda yakin ingin menghapus Job Number {{$job->id}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
        <a href="{{ url('#') }}"><button type="button" class="btn btn-danger">Delete</button></a>
      </div>
    </div>
  </div>
</div>






<script >
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
