@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                   <div class="row">
                    <div class="col-md-6">
                        <h3>Registrasi</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        <nav>
                          <ol class="cd-multi-steps text-bottom count">
                            <li class="visited"><a href="#0"></a></li>
                            <li class="visited"><a></a></li>
                            <li class="current"><em></em></li>
                            <li><em></em></li>
                        </ol>
                        </nav>
                    </div>
                    </div>
                </div>

                <div class="panel-body">
                   <form role="form" data-toggle="validator" class="form-horizontal" method="GET" action="{{ url('/front/review') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    <div class="row">
                         <div class="col-md-12">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">Lab Id Sample</th>
                                            <th rowspan="2">Customer Sample ID</th>
                                            <th rowspan="2">Sample Matrix</th>
                                            <th rowspan="2" style="width:20%">Parameter</th>
                                            <th colspan='7'>Field Parameters</th>
                                            <th rowspan="2" style="width:20%">Keterangan</th>
                                        </tr>
                                        <tr>
                                            <th>pH</th>
                                            <th>Suhu</th>
                                            <th>DHL</th>
                                            <th>DO</th>
                                            <th>Sal</th>
                                            <th>Debit</th>
                                            <th>Kap.Pro</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($dt_sample as $key => $value)
                                        <tr>
                                            <td>{{$value['kode_sample']}}</td>
                                            <td>{{$value['name']}}</td>
                                            <td>{{$value['name_matrix']}}</td>
                                            <td>
                                              <select class="js-example-tokenizer form-control" name="param[{{$key}}][]" multiple tabindex="-1" aria-hidden="true">
                                                @foreach ($param[$key] as $keys => $value)
                                                  @if($value->check == 1)
                                                    <option value="{{$value->id}}" selected>{{$value->name}}</option>
                                                  @else
                                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                                  @endif
                                                @endforeach
                                              </select>
                                            </td>
                                            <td>
                                               <div class="form-group{{ $errors->has('ph.'.$key) ? ' has-error' : '' }}">
                                                        <input id="ph" type="text" class="form-control" name="ph[{{$key}}]" value="{{ old('ph.'.$key) }}" placeholder="" required>

                                                        @if ($errors->has('ph.'.$key))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('ph.'.$key) }}</strong>
                                                            </span>
                                                        @endif   
                                                </div>
                                            </td>
                                            <td>
                                               <div class="form-group{{ $errors->has('suhu.'.$key) ? ' has-error' : '' }}">
                                                        <input id="suhu" type="text" class="form-control" name="suhu[{{$key}}]" value="{{ old('suhu.'.$key) }}" placeholder="" required>

                                                        @if ($errors->has('suhu.'.$key))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('suhu.'.$key) }}</strong>
                                                            </span>
                                                        @endif   
                                                </div>
                                            </td>
                                            <td>
                                               <div class="form-group{{ $errors->has('dlh.'.$key) ? ' has-error' : '' }}">
                                                        <input id="dlh" type="text" class="form-control" name="dlh[{{$key}}]" value="{{ old('dlh.'.$key) }}" placeholder="" required>

                                                        @if ($errors->has('dlh.'.$key))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('dlh.'.$key) }}</strong>
                                                            </span>
                                                        @endif   
                                                </div>
                                            </td>
                                            <td>
                                               <div class="form-group{{ $errors->has('do.'.$key) ? ' has-error' : '' }}">
                                                        <input id="do" type="text" class="form-control" name="do[{{$key}}]" value="{{ old('do.'.$key) }}" placeholder="" required>

                                                        @if ($errors->has('do.'.$key))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('do.'.$key) }}</strong>
                                                            </span>
                                                        @endif   
                                                </div>
                                            </td>
                                            <td>
                                               <div class="form-group{{ $errors->has('sal.'.$key) ? ' has-error' : '' }}">
                                                        <input id="sal" type="text" class="form-control" name="sal[{{$key}}]" value="{{ old('sal.'.$key) }}" placeholder="" required>

                                                        @if ($errors->has('sal.'.$key))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('sal.'.$key) }}</strong>
                                                            </span>
                                                        @endif   
                                                </div>
                                            </td>
                                            <td>
                                               <div class="form-group{{ $errors->has('debit.'.$key) ? ' has-error' : '' }}">
                                                        <input id="debit" type="text" class="form-control" name="debit[{{$key}}]" value="{{ old('debit.'.$key) }}" placeholder="" required>

                                                        @if ($errors->has('debit.'.$key))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('debit.'.$key) }}</strong>
                                                            </span>
                                                        @endif   
                                                </div>
                                            </td>
                                            <td>
                                               <div class="form-group{{ $errors->has('kp.'.$key) ? ' has-error' : '' }}">
                                                        <input id="kp" type="text" class="form-control" name="kp[{{$key}}]" value="{{ old('kp.'.$key) }}" placeholder="" required>

                                                        @if ($errors->has('kp.'.$key))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('kp.'.$key) }}</strong>
                                                            </span>
                                                        @endif   
                                                </div>
                                            </td>
                                            <td>
                                               <div class="form-group{{ $errors->has('ket.'.$key) ? ' has-error' : '' }}">
                                                        <textarea id="ket" type="text" class="form-control" name="ket[{{$key}}]" value="{{ old('ket.'.$key) }}" placeholder="Keadaan lapangan dan abnormalisasi (jika ada)" required></textarea>

                                                        @if ($errors->has('ket.'.$key))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('ket.'.$key) }}</strong>
                                                            </span>
                                                        @endif   
                                                </div>
                                            </td>
                                        </tr>
                                            @foreach($dt_sample[$key] as $keys => $value)
                                              <input type="hidden" name="data_sample[{{$key}}][{{$keys}}]" value="{{$value}}">
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                                @foreach($dt as $key => $value)
                                  <input type="hidden" name="data[{{$key}}]" value="{{$value}}">
                                @endforeach
                        </div>
                    </div>
                    <div class="col-md-12" style="text-align:right">
                      {!! Form::button('<span class="middle"> Next >> </span>', array('class' => 'btn btn-default','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
                    </div>
                  </div>
               </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $('.' + 'industry_type').select2({
        placeholder: '--',
    });

  $('.' + 'ref').select2({
        placeholder: '--',
    });

  $('.' + 'matrix').select2({
        placeholder: '--',
    });
$(".js-example-tokenizer").select2({
  tags: true,
  tokenSeparators: [',', ' ']
})
</script>

@foreach($dt_sample as $key => $value)
    <script type="text/javascript">
    // If I use POST from the ajax call, I got a TokenMismatch Exception, so if you add the below script and also the meta tag at the head of your view, it will work.
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    // Script para el dropdown dinámico
    $(document).ready(function($)
    {
       $('#matrix<?php echo ($key) ?>' ).change(function()
       {
        $.get("{!! route('dropdown') !!}", { option: $(this).val() }, 
          function(data) {
            var ciudades = $('#ref<?php echo ($key) ?>');
            ciudades.empty();
            $.each(data, function(key, value) {   
             ciudades
             .append($("<option></option>")
              .attr("value",key)
              .text(value)); 
           });
          });
      });

       $('#ref<?php echo ($key) ?>').change(function(){
        $.get("{!! route('dropdown2') !!}", { option: $(this).val()}, 
          function(data) {
            var ciudades = $('#industry_type<?php echo ($key) ?>');
            ciudades.empty();
            $.each(data, function(key, value) {   
             ciudades
             .append($("<option></option>")
              .attr("value",key)
              .text(value)); 
           });
          });
      });
       $('#ref<?php echo ($key) ?>').change(function(){
        $.get("{!! route('dropdown3') !!}", { option: $(this).val()}, 
          function(data) {
            var ciudades = $('#param');
            ciudades.empty();
            $.each(data, function(key, value) {   
             ciudades
             .append($("<option></option>")
              .attr("value",key)
              .text(value)); 
           });
          });
      });
    });
    </script>
@endforeach
@endsection
