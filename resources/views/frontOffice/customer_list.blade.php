@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                    <div class="col-md-6">
                        <h3 style="padding-left:18px">Customer List</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        <nav>
                          <ol class="cd-multi-steps text-top">
                            <button class="btn btn-default dropdown-toggle" data-toggle="modal" data-target="#myModal"> Add Customer</button>
                        </ol>
                        </nav>
                    </div>
                    </div>
                </div>
                <div class="panel-body">
                     <div class="col-md-12">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Customer ID</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Fax</th>
                                            <th>Address</th>
                                            <th>City</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($customer as $key => $value)
                                        <tr>
                                          <td style="text-align:center">{{$value->id_customer}}</td>
                                          <td>{{$value->name_customer}}</td>
                                          <td>{{$value->phone}}</td>
                                          <td>{{$value->fax}}</td>
                                          <td>{{$value->address}}</td>
                                          <td >{{$value->city}}</td>
                                          <td style="text-align:center">
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
                                              <ul class="dropdown-menu">
                                                @if(Auth::user()->hasRole('Front_Office'))
                                                <li><a href="{{ url('/front/customerlist/edit/'.$value->id_customer) }}"> Edit</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="{{ url('/front/customerlist/nonactive/'.$value->id_customer) }}"> Delete</a></li>
                                                @else
                                                <li><a href="{{ url('/admin/customerlist/edit/'.$value->id_customer) }}"> Edit</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="{{ url('/admin/customerlist/nonactive/'.$value->id_customer) }}"> Delete</a></li>
                                                @endif
                                              </ul>
                                            </div>
                                        </td>
                                        </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                    </div>
                 </form>
                 <div class="col-md-12 left"><a href="{{ url('/admin/home')}}"><button class=" btn btn-primary"><< Back</button></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('modal.customer_add')

<script >
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
