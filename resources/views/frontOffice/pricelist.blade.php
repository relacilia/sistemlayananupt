@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading center">
                    <h3>Job List</h3>
                </div>
                <div class="panel-body">
                     <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Laboratory ID</th>
                                            <th>Customer</th>
                                            <th>Harga Total</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($job as $key => $value)
                                          <tr>
                                            <td>{{$value->id}}</td>
                                            <td class="left">{{$value->customer->name_customer}}</td>
                                            <td><b>Rp {{$value->price}}</b></td>
                                            <td><button id='detail{{$key}}' value='{{$value->id}}'>Detail</button></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-4 kotak">
                                <h4>Detail Harga Job <b><span id="job"></span></b></h4>
                                <div class="row">
                                    <div class="col-md-3">
                                        <p>Kode</p>
                                        <hr>
                                        <span id='kode'></span>
                                    </div>
                                    <div class="col-md-5">
                                        <p>Customer ID</p>
                                        <hr>
                                        <span id='cid'></span>
                                    </div>
                                    <div class="col-md-4 right">
                                        <p>Harga</p>
                                        <hr>
                                        <span id='harga'></span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row bold">
                                    <div class="col-md-6">
                                        <p id='totaltxt'></p>
                                    </div>
                                    <div class="col-md-6 right">
                                        <p id='total'></p>
                                    </div>
                                </div>
                                <div class="right"><p id='total'></p></div>
                            </div>
                        </div>
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

@if($job->count() != 0)
<?php $count = $job->count() ?>
@for($i=0; $i<$count; $i++)
<script >
$(document).ready(function() {
    $('#detail<?php echo ($i) ?>').click(function()
       {
        $.get("{!! route('detailprice') !!}", { option: $(this).val() }, 
          function(data) {  
            $('#' + 'kode').empty(); 
            $('#' + 'cid').empty(); 
            $('#' + 'harga').empty();
            $('#' + 'job').text(': '+data[0].job_id);
            var total = 0;
            $.each(data, function(key, value) {  
                $('#' + 'kode').append('<p>'+value.kode_sample+'</p>');
                $('#' + 'cid').append('<p>'+value.name+'</p>');
                $('#' + 'harga').append('<p>'+value.price+'</p>');
                total = total + value.price;
            });
            $('#' + 'totaltxt').text('Total :');
            $('#' + 'total').text('Rp'+total);
         });
      });
} );
</script>
@endfor
@endif
@endsection
