@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading center" >
                    <h3>Preview Tanda Terima</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                         <div class="col-md-5" style="padding-left:35px">
                            <div class="row">
                                <!-- part -->
                                <div class="col-md-4">
                                    <p >Job Number</p>
                                </div>
                                <div class="col-md-8" style="padding-right:0px">
                                    <p>: {{$data['id']}}</p>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                <!-- part -->
                                <div class="col-md-4">
                                    <p>Customer</p>
                                </div>
                                <div class="col-md-8" style="padding-right:0px">
                                    <p>: {{$cus->name_customer}}</p>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-4">
                                    <p>Attention</p>
                                </div>
                                <div class="col-md-8">
                                     <p>: {{$data['attention']}}</p>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-4">
                                    <p>Sampled by</p>
                                </div>
                                <div class="col-md-8">
                                     <p>: {{$data['sampled_by']}}</p>
                                </div>
                                <!-- endpart -->
                            </div>
                         </div>
                         <div class="col-md-3">
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-6">
                                    <p>Date Received</p>
                                </div>
                                <div class="col-md-6">
                                    <p>: {{$data['date_recent']}}</p>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-6">
                                    <p>Time Received</p>
                                </div>
                                 <div class="col-md-6">
                                    <p>: {{$data['time_recent']}}</p>
                                </div>
                                <!-- endpart -->
                            </div>
                            <div class="row">
                                 <!-- part -->
                                <div class="col-md-6">
                                    <p>Interval Analysis</p>
                                </div>
                                <div class="col-md-6">
                                    <p>: {{$data['interval']}}</p>
                                </div>
                                <!-- endpart -->
                            </div>
                         </div>
                         <div class="col-md-4">
                            <div class="row">
                                <!-- part -->
                                <div class="col-md-12" style="text-align:right">
                                    <p><a href="{{ url('/front/downloadPdf/'.$data['id']) }}"><button class="btn btn-danger">Download pdf </button></a>
                                    <a href="{{ url('/front/downloadExcel/'.$data['id']) }}"><button class="btn btn-success">Download Excel xls</button></a></p>
                                </div>
                                <!-- endpart -->
                            </div>
                         </div>
                    </div>
                    <br>
                     <div class="col-md-12" style="text-align:center">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th rowspan="2">Lab Id Sample</th>
                                    <th rowspan="2">Customer Sample ID</th>
                                    <th rowspan="2">Sample Matrix</th>
                                    <th rowspan="2">Date Sample</th>
                                    <th rowspan="2">Time Sample</th>
                                    <th colspan='2'>Coordinate</th>
                                </tr>
                                <tr>
                                    <th>S</th>
                                    <th>E</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sample_array as $key => $value)
                                <tr>
                                    <td>{{$value['kode_sample']}}</td>
                                    <td>{{$value['name']}}</td>
                                    <td>{{$value['name_matrix']}}</td>
                                    <td>{{$value['date_sampled']}}</td>
                                    <td>{{$value['time_sampled']}}</td>
                                    <td>{{$value['coordinat_S']}}</td>
                                    <td>{{$value['coordinat_E']}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $('#' + 'id_customer').select2({
        placeholder: '--',
    });

  $('#' + 'customer').select2({
        placeholder: '--',
    });
</script>
<script>
   $( function() {
    $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );

  </script>
<script type="text/javascript">
    // If I use POST from the ajax call, I got a TokenMismatch Exception, so if you add the below script and also the meta tag at the head of your view, it will work.
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    // Script para el dropdown dinámico
    $(document).ready(function($)
    {
       $('#customer').change(function()
       {
        $.get("{!! route('dataCustom2') !!}", { option: $(this).val() }, 
          function(data) {  
            var ciudades = $('#id_customer');
            ciudades.empty();
            $.each(data, function(key, value) {   
                 $('#' + 'address').val(data.address);
                 $('#' + 'phone').val(data.phone);
                 ciudades.append($("<option></option>").attr("value.id",key).text(value)); 
            });
         });
      });
    });
</script>
@endsection
