@extends('layouts.app_login')

@section('content')
    <!-- <div class="col-md-12" style="position: absolute;"> -->
    <div class="row center">
        <!-- <div class="col-md-6"  style="text-align:left"> -->
            <img src="{{asset('images/logojatim.png')}}" style="height:130px">
        <!-- </div> -->
        <!-- <div class="col-md-6" style="text-align:right"> -->
            <!-- <img src="{{asset('images/kan1.png')}}" style="height: 80px;margin-top: 20px;"> -->
        <!-- </div> -->
    </div>
    <!-- </div> -->
    <div id="logo" class="row mr-top1">
        <h3 class="no-margin mr-bottom1">SISTEM PELAYANAN</h3>
        <h3 class="no-margin mr-bottom1">UNIT PELAKSANA TEKNIS  (UPT) LABORATORIUM LINGKUNGAN</h3>
        <h3 class="no-margin mr-bottom1">DINAS LINGKUNGAN HIDUP PROVINSI JAWA TIMUR</h3>
    </div>
    
    <section>
        <section class="row">
            <form role="login" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                 <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                   <input id="email" type="email" class="form-control" placeholder="Enter your email" name="email" value="{{ old('email') }}" required autofocus>
                    <span class="glyphicon glyphicon-user"></span>
                      @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                </div>
                
                 <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" placeholder="Enter your password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                    <span class="glyphicon glyphicon-lock"></span>
                </div>
            
                <div class="form-group" style="color:darkslategray">
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                </div>
                
                <button type="submit" name="go" class="btn btn-block btn-primary" style="background: darkslategray">Login Now</button>
                
                <section>
                    <a href="{{ url('/password/reset') }}" style="color:darkslategray">Forgot your password ?</a>
                </section>
            </form>
        </section>
    </section>
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
@endsection
