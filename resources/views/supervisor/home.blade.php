@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading center"><h3>Task List</h3></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Job ID</th>
                                            <th>Laboratory Sample ID</th>
                                            <th>Customer Sample ID</th>
                                            <th>Deadline</th>
                                            <th>Status Sample</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($sample as $key => $value)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$value->job_id}}</td>
                                                <td>{{$value->kode_sample}}</td>
                                                <td>{{$value->name}}</td>
                                                <td><?php $date2 = \Carbon\Carbon::createFromFormat("Y-m-d", $value->job->date_recent); $data = $date2->addDays(4)->toDateString(); ?>
                                                   {{$data}}</td>
                                                <td>{{$value->status}}</td>
                                                <td><a href="{{url('/supervisor/parameterlist/'.$value->id)}}"><button>LIHAT</button></a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </div>
                        <!-- <div class="col-md-6" style="background:azure" >
                            <p>Job ID : {{$sample[0]->job_id}}</p>
                            <p>Lab ID : {{$sample[0]->kode_sample}}</p>
                            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Parameter</th>
                                            <th>Hasil</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($sample as $key => $value)
                                          <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$value->job_id}}</td>
                                            <td>{{$value->status}}</td>
                                            <td><button>ACCEPT</button><button>REVISI</button></td>
                                          </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
