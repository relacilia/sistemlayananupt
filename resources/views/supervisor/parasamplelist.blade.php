@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                    <div class="col-md-6">
                        <h3 style="padding-left:18px">Lab Sample ID : {{$paras[0]->sample->kode_sample}}</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        <h3>Deadline : <?php $date2 = \Carbon\Carbon::createFromFormat("Y-m-d", $paras[0]->sample->job->date_recent); $data = $date2->addDays(4)->toDateString(); ?>
                                               {{$data}}</h3>
                        <h4>{{$paras[0]->sample->matrix->name}} || {{$paras[0]->sample->ref->name}}</h4>
                    </div>
                    </div>
                </div>

                <div class="panel-body">
                            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Parameter</th>
                                            <th>Analyst</th>
                                            <th>Status</th>
                                            <th>Result</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($paras as $key => $value)
                                          <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$value->parameter->name}}</td>
                                            <td>{{$value->user->name}}</td>
                                            <td>{{$value->status_parameter}}</td>
                                            <td>
                                                <?php $i = $hasil[$key]->count(); ?>
                                                @foreach($hasil[$key] as $keys => $val)
                                                    @if($keys == $i-1)
                                                        <b>{{$val->value}}</b>
                                                    @else
                                                        {{$val->value}} <span style="color:red">(Rev)</span><br>
                                                    @endif
                                                @endforeach
                                            </td>
                                            <td>
                                                @if($value->status_parameter == 'Onprogress (SV)')
                                                <a href="{{url('/supervisor/parameterlist/acceptation/'.$value->id)}}"><button>ACCEPT</button></a> <a href="{{url('/supervisor/parameterlist/revision/'.$value->id)}}"><button>REVISI</button></a>
                                                @endif
                                            </td>
                                          </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                        <a href="{{url('/supervisor/home')}}"><button class="btn btn-default"> << Back</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
