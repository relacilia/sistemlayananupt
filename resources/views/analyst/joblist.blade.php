@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading center">
                    <h3>Job List of {{$menu_active->name}}</h3>
                </div>
                <div class="panel-body">
                     <div class="col-md-12">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Date Recent</th>
                                            <th>Deadline</th>
                                            <th>Total Samples</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1 ?>
                                        @foreach($parameter as $key => $value)
                                          <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$value->date_recent}}</td>
                                            <td>
                                               <?php $date2 = \Carbon\Carbon::createFromFormat("Y-m-d", $value->date_recent); $data = $date2->addDays(4)->toDateString(); ?>
                                               {{$data}}
                                            </td>
                                            <td>{{$total[$key]}}</td>
                                            <td><a href="{{ url('/analyst/samplelist/'.$menu_active->id.'/'.$value->date_recent) }}"><button>Action</button></a></td>
                                            </tr>
                                            <?php $i++ ?>
                                        @endforeach
                                    </tbody>
                                </table>
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
