@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                    <div class="col-md-6">
                        <h3 style="padding-left:18px">Job List</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        @if($parameter->count() != 0)
                        <h4>Date Recent : {{$parameter[0]->date_recent}}</h4>
                        <h4>Deadline : <?php $date2 = \Carbon\Carbon::createFromFormat("Y-m-d", $parameter[0]->date_recent); $data = $date2->addDays(4)->toDateString(); ?>
                                               {{$data}}</h4>
                        @endif
                    </div>
                    </div>
                </div>
                <div class="panel-body">
                     <div class="col-md-12">
                        @if($parameter->count() != 0)
                        <p style="text-align:right"><a href="{{ url('/analyst/downloadExcelAnalis/'.$menu_active->id.'/'.$parameter[0]->date_recent) }}"><button class="btn btn-success" type="button">Download Excel xls</button></a>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#myUpload">Upload Excel xls</button></p>
                        <br>
                        @endif
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Laboratory ID</th>
                                            <th>Customer Sample ID</th>
                                            <th>Status</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($parameter->count() != 0)
                                        @foreach($parameter as $key => $value)
                                          <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$value->kode_sample}}</td>
                                            <td>{{$value->name}}</td>
                                            <td>{{$value->status_parameter}}</td>
                                            <td>
                                                @if($value->status_parameter != 'Onprogress (AN)')
                                                    @foreach($result[$key] as $val)
                                                        {{$val->value}}
                                                        <br>
                                                    @endforeach
                                                @endif
                                                @if($value->status_parameter == 'Onprogress (AN)' || $value->status_parameter == 'Revision (SV)' || $value->status_parameter == 'Revision (TM)')
                                                    <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/analyst/result/add/'.$value->id) }}" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                        <button type="button" id="hide{{$key}}" style="display:inline">Insert Result</button>
                                                        <input id="lolo{{$key}}" style="display:none;" name="result" required>
                                                        <button type="submit" id="lulu{{$key}}" style="display:none;">Save</button>
                                                        <button type="button" id="lala{{$key}}" style="display:none;">Cancel</button>
                                                    </form>
                                                @endif
                                            </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('modal.upload')

@if($parameter->count() != 0)
<?php $count = $parameter->count() ?>
@for($i=0; $i<$count; $i++)
<script>
$(document).ready(function(){
    $("#hide<?php echo ($i) ?>").click(function(){
        $("#hide<?php echo ($i) ?>").hide();
        document.getElementById("lolo<?php echo ($i) ?>").style.display = "inline";
        document.getElementById("lulu<?php echo ($i) ?>").style.display = "inline";
        document.getElementById("lala<?php echo ($i) ?>").style.display = "inline";
    });
    $("#lala<?php echo ($i) ?>").click(function(){
        $("#hide<?php echo ($i) ?>").show();
        document.getElementById("lolo<?php echo ($i) ?>").style.display = "none";
        document.getElementById("lulu<?php echo ($i) ?>").style.display = "none";
        document.getElementById("lala<?php echo ($i) ?>").style.display = "none";
    });
});
</script>
@endfor
@endif
<script >
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

@endsection
