@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading center">
                    <h3>Job List of {{$menu_active->name}}</h3>
                </div>
                <div class="panel-body">
                     <div class="col-md-12">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Laboratory ID</th>
                                            <th>Customer Sample ID</th>
                                            <th>Deadline</th>
                                            <th>Status</th>
                                            <th>Dowload</th>
                                            <th>Upload</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($parameter as $key => $value)
                                          <tr>
                                            <td>{{$value->kode_sample}}</td>
                                            <td>{{$value->name}}</td>
                                            <td>
                                               <?php $date2 = \Carbon\Carbon::createFromFormat("Y-m-d", $value->date_recent); $data = $date2->addDays(4)->toDateString(); ?>
                                               {{$data}}
                                            </td>
                                            <td>{{$value->status}}</td>
                                            <td><button>Download</button></td>
                                            <td><button>Upload</button></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
