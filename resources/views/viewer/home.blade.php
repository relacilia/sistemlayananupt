@extends('layouts.app_login')

@section('content')
    <!-- <div class="col-md-12" style="position: absolute;"> -->
    <div class="row center">
        <!-- <div class="col-md-6"  style="text-align:left"> -->
            <img src="{{asset('images/logojatim.png')}}" style="height:130px">
        <!-- </div> -->
        <!-- <div class="col-md-6" style="text-align:right"> -->
            <!-- <img src="{{asset('images/kan1.png')}}" style="height: 80px;margin-top: 20px;"> -->
        <!-- </div> -->
    </div>
    <!-- </div> -->
    <div id="logo" class="row mr-top1">
        <h3 class="no-margin mr-bottom1">SISTEM PELAYANAN</h3>
        <h3 class="no-margin mr-bottom1">UNIT PELAKSANA TEKNIS  (UPT) LABORATORIUM LINGKUNGAN</h3>
        <h3 class="no-margin mr-bottom1">DINAS LINGKUNGAN HIDUP PROVINSI JAWA TIMUR</h3>
    </div>
    
    <section>
        <section class="row center">
            @if(!Auth::guest())
                @if(Auth::user()->hasRole('Front_Office'))
                    <a href="{{ url('/front/home') }}">
                @elseif(Auth::user()->hasRole('Analyst'))
                    <a href="{{ url('/analyst/home') }}">
                @elseif(Auth::user()->hasRole('Supervisor'))
                    <a href="{{ url('/supervisor/home') }}">
                @endif
                <button class="btn btn-primary" style="background: darkslategray; padding:10px"><h5>DASHBOARD</h5></button></a>
            @else
                <a href="{{ url('/login') }}"> <button class="btn btn-primary" style="background: darkslategray; padding:10px"><h5>LOGIN</h5></button></a>
            @endif
        </section>
    </section>
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
@endsection
