@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading center">
                    <h3>Catatan Keterlambatan Analis</h3>
                </div>
                <div class="panel-body">
                     <div class="col-md-12">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Analis</th>
                                            <th>Deadline</th>
                                            <th>Tanggal Input</th>
                                            <th>Lama Keterlambatan (hari)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($late as $key => $value)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $value->user->name}}</td>
                                            <td>
                                                <?php $date2 = new \Carbon\Carbon ($value->date_input); $date = $date2->subDays($value->duration)->toDateString();  ?>
                                               {{$date}}
                                            </td>
                                            <td>{{$value->date_input}}</td>
                                            <td>{{$value->duration}}</td>
                                           
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
