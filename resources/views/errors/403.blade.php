<html>
	<head>
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
		<link rel="icon" type="image/png" href="{{ URL::asset('favicon.png') }}">
		<title>Not Found</title>
		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #B0BEC5;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 72px;
				margin-bottom: 40px;
				font-weight: 600;
			}

			.quote {
				font-size 	: 1.3rem;
				font-weight : 600;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<div class="title">NOT FOUND</div>
				<div class="quote">Maaf, halaman yang anda cari tidak ditemukan.</div>
			</div>
		</div>
	</body>
</html>
