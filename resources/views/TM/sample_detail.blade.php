@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <!-- <div class="panel-heading">
                     <div class="row">
                    <div class="col-md-6">
                        <h3 style="padding-left:18px">Samples List</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        <nav>
                          <ol class="cd-multi-steps text-top">
                            <li class="visited"><a href="{{url('/front/joblist')}}">Jobs</a></li>
                            <li class="current"><a href="#0">Samples</a></li>
                        </ol>
                        </nav>
                    </div>
                    </div>
                </div> -->
                <div class="panel-body">
                    <h4>Info Job <b>{{$job->id}}</b></h4>
                    <table style="width:100%">
                        <tr style="background:aliceblue">
                            <td class="col-md-2">Job Number</td>
                            <td class="col-md-3">: {{$job->id}}</td>
                            <td class="col-md-2">Date Received</td>
                            <td class="col-md-2">: {{$job->date_recent}}</td>
                            <td class="col-md-1">Status</td>
                            <td class="col-md-2">: {{$job->status}}</td>
                        </tr>
                        <tr style="background:white">
                            <td class="col-md-2">Customer</td>
                            <td class="col-md-3">: {{$job->customer->name_customer }}</td>
                            <td class="col-md-2">Time Received</td>
                            <td class="col-md-2">: {{$job->time_recent}}</td>
                            <td class="col-md-1">Sampled by</td>
                            <td class="col-md-2">: {{$job->sampled_by }}</td>
                        </tr>
                         <tr style="background:aliceblue">
                            <td class="col-md-2">Attention</td>
                            <td class="col-md-3">: {{$job->attention }}</td>
                            <td class="col-md-2">Interval</td>
                            <td class="col-md-2">: {{$job->interval}}</td>
                            <td class="col-md-1">Location</td>
                            <td class="col-md-2">: {{$job->location}}</td>
                        </tr>
                    </table>
                    <hr>
                    <br>
                    <h4>Info Sample <b>{{$cur_sample->kode_sample}}</b></h4>
                    <table style="width:100%">
                        <tr style="background:aliceblue">
                            <td class="narrow">pH</td>
                            <td class="col-md-1">: {{$cur_sample->ph}}</td>
                            <td class="narrow">Suhu</td>
                            <td class="col-md-1">: {{$cur_sample->suhu}}</td>
                            <td class="narrow">Sal</td>
                            <td class="col-md-1">: {{$cur_sample->sal}}</td>
                            <td class="narrow">DHL</td>
                            <td class="col-md-1">: {{$cur_sample->dhl}}</td>
                            <td class="narrow">DO</td>
                            <td class="col-md-1">: {{$cur_sample->do}}</td>
                            <td class="narrow">Debit</td>
                            <td class="col-md-1">: {{$cur_sample->debit}}</td>
                            <td class="narrow">K. Pro</td>
                            <td class="col-md-1">: {{$cur_sample->kappro}}</td>
                        </tr>
                    </table>
                    <hr>
                    <div style="background:aliceblue; padding:10px"><p>Keterangan : {{$cur_sample->info}} </p></div>
                     <div class="col-md-12">
                    @include('partials.status-alert')
                    <hr>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <tbody>
                                        <tr >
                                            <td class="middle">Parameter</td>
                                            @if ($jobs)
                                                @foreach($jobs as $job_history)
                                                    <td class="left middle" style="font-size: 1rem">
                                                        History date: <br>
                                                        {{ $job_history->date_recent }}
                                                    </td>
                                                @endforeach
                                            @endif
                                            <th class="middle">Current Result</th>
                                            <td class="middle">Action</td>
                                        </tr>

                                        @foreach($param as $key => $value)
                                        <tr>
                                            <th>{{$value->parameter->name}}</th>
                                            @if ($jobs)
                                                @foreach($jobs as $job_history)
                                                    <td class="left middle" style="font-size: 1.3rem">
                                                        <?php 
                                                            $sample = App\Sample::where('job_id', $job_history->id)->where('matrix_id', $cur_sample->matrix_id)->first();
                                                            if($sample != null){
                                                            $cur_parameter_sample = $sample->parameter_samples->where('parameter_id', $value->parameter_id)->first();
                                                            $cur_result = $cur_parameter_sample->results()->where('status_result', '=', 'OK (TM)')->first();
                                                            echo $cur_result->value;
                                                            }
                                                            else echo '0';

                                                        ?>
                                                    </td>
                                                @endforeach
                                            @endif
                                            <td>
                                            <?php $i = $result[$key]->count(); $j=$i-1 ?>
                                            @foreach($result[$key] as $keys => $val)
                                                @if($keys == $j)
                                                    <h4 class='bold'>{{$val->value}}</h4>
                                                @else
                                                    {{$val->value}} <span style="color:red">(Rev)</span>  <hr>
                                                @endif
                                            @endforeach
                                            </td>
                                            <td style="text-align:center">
                                                @if($value->status_parameter == 'Onprogress (TM)')
                                                 <a href="{{url('/tm/parameterlist/acceptation/'.$value->id)}}"><button>ACCEPT</button></a> <a href="{{url('/tm/parameterlist/revision/'.$value->id)}}"><button>REVISI</button></a>
                                                @else
                                                 <p>-</p>
                                                @endif
                                            </td>
                                      
                                        </tr>
                                       @endforeach
                                    </tbody>
                                </table>
                    </div>
                 </form>
                 <a href="{{url('/tm/samplelist/'.$job->id)}}"><button class="btn btn-default"> << Back</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
