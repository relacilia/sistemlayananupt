@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                     <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Job Number</th>
                                            <th>Customer</th>
                                            <th>Deadline</th>
                                            <th>Number Samples</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($job as $key => $value)
                                        <tr>
                                          <td>{{$value->id}}</td>
                                          <td>{{$value->customer->name_customer}}</td>
                                          <td><?php $date2 = \Carbon\Carbon::createFromFormat("Y-m-d", $value->date_recent); $data = $date2->addDays(4)->toDateString(); ?>
                                               {{$data}}</td>
                                          <td style="text-align:center">{{$value->number_samples}}</td>
                                          <td style="text-align:center">{{$value->status}}</td>
                                          <td style="text-align:center" ><a href="samplelist/{{$value->id}}"><button>Lihat</button></a></td>
                                        </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
