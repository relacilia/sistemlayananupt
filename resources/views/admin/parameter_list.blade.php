@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                    <div class="col-md-6">
                        <h3 style="padding-left:18px">Parameter List</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#matrixModal">Add Parameter</button>
                    </div>
                    </div>
                </div>
                <div class="panel-body">
                     <div class="col-md-12">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Nama Parameter</th>
                                    <th>Procedure</th>
                                    <th>Satuan</th>
                                    <th>Harga</th>
                                    <th>Akreditasi</th>
                                    <th>MDL</th>
                                    <th>Analis</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($parameter as $key => $value)
                                <tr>
                                <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/admin/matrix/save/'.$value->id) }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                  <td>
                                    <div id="alias{{$key}}" class="col-md-12">
                                      {{$value->name}}
                                    </div>
                                    <div id="a_edit{{$key}}" class="col-md-12 none">
                                      <input name="alias" value="{{$value->alias}}" required>
                                    </div>
                                  </td>
                                  <td>
                                    <div id="name{{$key}}" class="col-md-12">
                                      {{$value->procedure_id}}
                                    </div>
                                    <div id="n_edit{{$key}}" class="col-md-12 none">
                                      <input name="name" value="{{$value->name}}" required>
                                    </div>
                                  </td>
                                  <td>{{$value->standard}}</td>
                                  <td>{{$value->price}}</td>
                                  <td>{{$value->accreditation}}</td>
                                  <td>{{$value->MDL}}</td>
                                  <td>
                                    @foreach($analyst[$key] as $k => $val)
                                      {{$val->user->name}} <br>
                                    @endforeach
                                  </td>
                                  <td style="text-align:center">
                                    <div class="btn-group">
                                      <a href="{{ url('/admin/parameterlist/edit/'.$value->id) }}"><button type="button" id="edit" class="btn btn-default"  aria-haspopup="true" aria-expanded="false">Edit</button></a>      
                                      <a href=""><button type="button" id="delete" class="btn btn-default" aria-haspopup="true" aria-expanded="false">Delete</button></a>     
                                    </div>
                                  </td>
                                 </form>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('modal.parameter_add')
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

@endsection
