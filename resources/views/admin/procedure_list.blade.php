@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                    <div class="col-md-6">
                        <h3 style="padding-left:18px">Sampling Procedure List</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#procedureModal">Add Procedure</button>
                    </div>
                    </div>
                </div>
                <div class="panel-body">
                     <div class="col-md-12">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Matrix</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($procedure as $key => $value)
                                <tr>
                                <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/admin/procedurelist/save/'.$value->id) }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                  <td>
                                    <div id="alias{{$key}}" class="col-md-12">
                                      {{$value->name_procedure}}
                                    </div>
                                    <div id="a_edit{{$key}}" class="col-md-12 none">
                                      <input name="name" value="{{$value->name_procedure}}" required>
                                    </div>
                                  </td>
                                  <td>
                                    <div id="name{{$key}}" class="col-md-12">
                                      {{$value->matrix->alias}}
                                    </div>
                                    <div id="n_edit{{$key}}" class="col-md-12 none">
                                      <select name="matrix" class="form-control" required>

                                        @foreach($matrixlist as $val1)
                                          @if($val1->id == $value->matrix_id)
                                            <option value="{{$val1->id}}" selected>{{$val1->alias}}</option>
                                          @else
                                            <option value="{{$val1->id}}">{{$val1->alias}}</option>
                                          @endif
                                        @endforeach
                                      </select>
                                    </div>
                                  </td>
                                  <td style="text-align:center">
                                    <div class="btn-group">
                                      <button type="button" id="edit{{$key}}" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Edit</button>       
                                      <button type="button" id="delete{{$key}}" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Delete</button>       
                                      <button type="submit" id="save{{$key}}" style="display:none;">Save</button>
                                      <button type="button" id="cancel{{$key}}" style="display:none;">Cancel</button>
                                    </div>
                                  </td>
                                 </form>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('modal.procedure_add')
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php $count = $procedure->count() ?>
@for($i=0; $i<$count; $i++)
<script>
$(document).ready(function(){
    $("#edit<?php echo ($i) ?>").click(function(){
        $("#edit<?php echo ($i) ?>").hide();
        $("#delete<?php echo ($i) ?>").hide();
        document.getElementById("save<?php echo ($i) ?>").style.display = "inline";
        document.getElementById("cancel<?php echo ($i) ?>").style.display = "inline";
        $("#a_edit<?php echo ($i) ?>").show();
        $("#n_edit<?php echo ($i) ?>").show();
        $("#alias<?php echo ($i) ?>").hide();
        $("#name<?php echo ($i) ?>").hide();
    });
    $("#cancel<?php echo ($i) ?>").click(function(){
        $("#edit<?php echo ($i) ?>").show();
        $("#delete<?php echo ($i) ?>").show();
        document.getElementById("save<?php echo ($i) ?>").style.display = "none";
        document.getElementById("cancel<?php echo ($i) ?>").style.display = "none";
        $("#a_edit<?php echo ($i) ?>").hide();
        $("#n_edit<?php echo ($i) ?>").hide();
        $("#alias<?php echo ($i) ?>").show();
        $("#name<?php echo ($i) ?>").show();
    });
});
</script>
@endfor
@endsection
