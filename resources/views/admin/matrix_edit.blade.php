@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading center">
                    <h3 style="padding-left:18px">Edit Sample {{$sample->kode_sample}} </h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/reftestlist/add/save') }}">
                        {{ csrf_field() }}                          

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-2">Jenis Matriks</label>

                            <div class="col-md-6">
                                 <select name="user" class="form-control analis" style="margin-left:5px;">
                                    @foreach($matrix as $keys => $val)
                                        <option @if($val->id == $sample->matrix_id) selected @endif>{{$val->alias}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('procedure') ? ' has-error' : '' }}">
                           <label for="name" class="col-md-2">Metode Pengujian</label>
                           <div class="col-md-6">
                              <select class="form-control procedure" name="procedure" id="procedure">
                                <option value="{{ old('procedure') }}" selected>{{ old('procedure') }}</option>
                              </select>
                               @if ($errors->has('procedure'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('procedure') }}</strong>
                                  </span>
                              @endif 
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $('.' + 'analis').select2({
        placeholder: '--',
    });
  $(".js-example-tokenizer").select2({
  tags: true,
  tokenSeparators: [',', ' ']
})
</script>
@endsection
