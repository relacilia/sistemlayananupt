@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading center">
                    <h3 style="padding-left:18px">Edit Sample {{$sample->kode_sample}} </h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/joblist/samplelist/'.$sample->id.'/edit/save') }}">
                        {{ csrf_field() }}                          

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-2">Customer Sample ID</label>

                            <div class="col-md-10">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $sample->name, old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                                    <label for="date" class="col-md-4">Date Sampled</label>

                                    <div class="col-md-8">
                                        <input id="date" type="text" class="form-control" name="date" value="{{$sample->date_sampled, old('date') }}" style="margin-left:5px;" required autofocus>

                                        @if ($errors->has('date'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('date') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
                                    <label for="time" class="col-md-3">Time Sampled</label>

                                    <div class="col-md-9">
                                        <input id="time" type="text" class="form-control" name="time" value="{{ $sample->time_sampled, old('time') }}" required autofocus>

                                        @if ($errors->has('time'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('time') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('coordinat_s') ? ' has-error' : '' }}">
                                    <label for="coordinat_s" class="col-md-4">Coordinat S</label>

                                    <div class="col-md-8">
                                        <input id="coordinat_s" type="text" class="form-control" name="coordinat_s" value="{{$sample->coordinat_S, old('coordinat_s') }}" style="margin-left:5px;" required autofocus>

                                        @if ($errors->has('coordinat_s'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('coordinat_s') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group{{ $errors->has('coordinat_e') ? ' has-error' : '' }}">
                                    <label for="coordinat_e" class="col-md-3">Coordinat E</label>

                                    <div class="col-md-9">
                                        <input id="coordinat_e" type="text" class="form-control" name="coordinat_e" value="{{ $sample->coordinat_E, old('coordinat_e') }}" required autofocus>

                                        @if ($errors->has('coordinat_e'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('coordinat_e') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price" class="col-md-2">Price</label>

                            <div class="col-md-4">
                                <input id="price" type="text" class="form-control" name="price" value="{{ $sample->price, old('price') }}" required autofocus>

                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-2">Status</label>
                            <div class="col-md-4">
                                <select class="form-control" name="status">
                                    <option value="Onprogress (AN)"> Onprogress (AN) </option>
                                    <option value="Onprogress (SV)"> Onprogress (SV) </option>
                                    <option value="Onprogress (TM)"> Onprogress (TM) </option>
                                    <option value="Onprogress (SV)"> OK (TM) </option>
                                </select>

                                @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <hr>
                        <div class="row">
                            <div class="col-md-6"><h4 class="padding">Data Pengujian</h4></div>
                            <div class="col-md-6 right"> <button type="button" data-toggle="modal" data-target="#matrix_modal" class="btn btn-warning margin">UBAH DATA PENGUJIAN</button> </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('matrix') ? ' has-error' : '' }}">
                            <label for="price" class="col-md-2">Matrix</label>

                            <div class="col-md-4">
                                <input id="price" type="text" class="form-control" value="{{ $sample->matrix->alias, old('matrix') }}" required disabled>
                                <input type="hidden" name="matrix" value="{{ $sample->matrix_id}}"> 

                                @if ($errors->has('matrix'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('matrix') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('matrix') ? ' has-error' : '' }}">
                            <label for="price" class="col-md-2">Procedure</label>

                            <div class="col-md-4">
                                <input id="price" type="text" class="form-control"  value="{{ $sample->procedure->name_procedure, old('price') }}" required disabled>
                                <input type="hidden" name="procedure" value="{{ $sample->procedure_id}}"> 
                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('matrix') ? ' has-error' : '' }}">
                            <label for="price" class="col-md-2">Ref Test</label>

                            <div class="col-md-4">
                                <input id="price" type="text" class="form-control" value="{{ $sample->ref->name, old('price') }}" required disabled>
                                <input type="hidden" name="ref" value="{{ $sample->ref_test_id}}"> 
                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <hr>

                        <h4 class="margin">Data Lapangan</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('ph') ? ' has-error' : '' }}">
                                    <label for="ph" class="col-md-4">pH</label>

                                    <div class="col-md-8">
                                        <input id="ph" type="text" class="form-control" name="ph" value="{{$sample->ph, old('ph') }}" style="margin-left:5px;" required autofocus>

                                        @if ($errors->has('ph'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('ph') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group{{ $errors->has('suhu') ? ' has-error' : '' }}">
                                    <label for="suhu" class="col-md-3">Suhu</label>

                                    <div class="col-md-9">
                                        <input id="suhu" type="text" class="form-control" name="suhu" value="{{ $sample->suhu, old('suhu') }}" required autofocus>

                                        @if ($errors->has('suhu'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('suhu') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('dhl') ? ' has-error' : '' }}">
                                    <label for="dhl" class="col-md-4">DHL</label>

                                    <div class="col-md-8">
                                        <input id="dhl" type="text" class="form-control" name="dhl" value="{{$sample->dhl, old('dhl') }}" style="margin-left:5px;" required autofocus>

                                        @if ($errors->has('dhl'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('dhl') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group{{ $errors->has('do') ? ' has-error' : '' }}">
                                    <label for="do" class="col-md-3">DO</label>

                                    <div class="col-md-9">
                                        <input id="do" type="text" class="form-control" name="do" value="{{ $sample->do, old('do') }}" required autofocus>

                                        @if ($errors->has('do'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('do') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('sal') ? ' has-error' : '' }}">
                                    <label for="sal" class="col-md-4">Sal</label>

                                    <div class="col-md-8">
                                        <input id="sal" type="text" class="form-control" name="sal" value="{{$sample->sal, old('sal') }}" style="margin-left:5px;" required autofocus>

                                        @if ($errors->has('sal'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('sal') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group{{ $errors->has('debit') ? ' has-error' : '' }}">
                                    <label for="debit" class="col-md-3">Debit</label>

                                    <div class="col-md-9">
                                        <input id="debit" type="text" class="form-control" name="debit" value="{{ $sample->debit, old('debit') }}" required autofocus>

                                        @if ($errors->has('debit'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('debit') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('kappro') ? ' has-error' : '' }}">
                                    <label for="kappro" class="col-md-4">Kappro</label>

                                    <div class="col-md-8">
                                        <input id="kappro" type="text" class="form-control" name="kappro" value="{{$sample->kappro, old('kappro') }}" style="margin-left:5px;" required autofocus>

                                        @if ($errors->has('kappro'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('kappro') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group{{ $errors->has('info') ? ' has-error' : '' }}">
                                    <label for="info" class="col-md-3">Keterangan Lain</label>

                                    <div class="col-md-9">
                                        <textarea id="info" type="text" class="form-control" name="info" value="{{ $sample->info, old('info') }}" required autofocus>{{$sample->info}}</textarea>

                                        @if ($errors->has('info'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('info') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <hr>
                        <div class="row">
                            <div class="col-md-6"><h4 class="margin">Parameter and Analis</h4></div>
                            <div class="col-md-6 right"> <button type="button" data-toggle="modal" data-target="#parameter_modal" class="btn btn-warning margin">TAMBAH ATAU HAPUS PARAMETER</button> </div>
                        </div>
                        @foreach($param as $key => $value)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('user') ? ' has-error' : '' }}">
                                    <label for="user" class="col-md-4">{{$value->parameter->name}}</label>

                                    <div class="col-md-8">
                                        <select name="user[]" class="form-control analis" style="margin-left:5px;">
                                            @foreach($assignment[$key] as $keys => $val)
                                                <option value="{{$val->user_id}}" @if($val->user_id == $value->user_id) selected="selected" @endif>{{$val->user->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('user'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('user') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                </div>
                            </div>
                        @endforeach
                        <hr>
                        <hr>
                        <div class="col-md-12 center">
                        {!! Form::button('<span class="middle"> SIMPAN PERUBAHAN </span>', array('class' => 'btn btn-primary margin','type' => 'submit')) !!}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Matrix -->
<div class="modal fade" id="matrix_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">WARNING!</h4>
      </div>
      <div class="modal-body margin">
        <p>Mengubah matrix, procedure, dan ref test dapat mengubah data parameter yang ada! </p> 
        <p>Apakah Anda yakin ingin mengubah matrix, procedure, dan ref test?</p>
        <button type="button" class="btn btn-primary margin" data-dismiss="modal">NO</button>
        <a href="{{ url('/admin/joblist/samplelist/'.$sample->id.'/edit/matrix/edit') }}"><button type="button" class="btn btn-primary margin">YA</button></a>
      </div>
    </div>
  </div>
</div>

<!-- Modal Parameter -->
<div class="modal fade" id="parameter_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah atau Hapus Parameter</h4>
      </div>
      <div class="modal-body margin">
         <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/admin/joblist/samplelist/'.$sample->id.'/edit/parameter/save') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div>
            <select class="js-example-tokenizer form-control" name="newparam[]" multiple tabindex="-1" aria-hidden="true">
                @foreach ($parameter as $keys => $value)
                    @if($value->check == 1)
                        <option value="{{$value->id}}" selected>{{$value->name}}</option>
                    @else
                        <option value="{{$value->id}}">{{$value->name}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="margin">
        <button type="button" class="btn btn-primary margin" data-dismiss="modal">Close</button>
        {!! Form::button('<span class="middle"> Simpan</span>', array('class' => 'btn btn-primary margin','type' => 'submit')) !!}
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('.' + 'analis').select2({
        placeholder: '--',
    });
  $(".js-example-tokenizer").select2({
  tags: true,
  tokenSeparators: [',', ' ']
})
</script>
@endsection
