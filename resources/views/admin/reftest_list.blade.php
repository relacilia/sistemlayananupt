@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                    <div class="col-md-6">
                        <h3 style="padding-left:18px">Ref test List</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        <a href="{{ url('/admin/reftestlist/add')}}"><button class="btn btn-primary">Add Reftest</button></a>
                    </div>
                    </div>
                </div>
                <div class="panel-body">
                     <div class="col-md-12">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Name</th>
                                    <th>Parameter</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($reftest as $key => $value)
                                <tr>
                                  <td class="narrow">
                                    <div id="alias{{$key}}" class="col-md-12 left">
                                      {{$value->kode}}
                                    </div>
                                  </td>
                                  <td>
                                    <div id="name{{$key}}" class="col-md-12 left">
                                      <p @if($value->level==1) style="margin-left:3%" @elseif ($value->level==2) style="margin-left:6%" @endif>{{$value->name}}</p>
                                    </div>
                                  </td>
                                  <td>
                                    <?php $i = $refparam[$key]->count(); ?>
                                    @foreach($refparam[$key] as $keys => $val)
                                      @if($keys == $i-1)
                                        {{$val->parameter->name}}
                                      @else
                                        {{$val->parameter->name}}, 
                                      @endif
                                    @endforeach
                                  </td>
                                  <td style="text-align:center">
                                    <a href="{{ url('/admin/reftestlist/edit/'.$value->id)}}"><button type="button">Edit</button></a>
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

@endsection
