@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading center">
                    <h3 style="padding-left:18px">Add Ref test</h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/reftestlist/add/save') }}">
                        {{ csrf_field() }}
            
                    <?php $i=0; ?>                            

                        <input id="kode" type="hidden" class="form-control" name="kode" value="{{$kode+1}}" required autofocus>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-1">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    
                        <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                            <label for="role" class="col-md-1 ">Jenis</label>

                            <div class="col-md-6">
                               <select class="matrix form-control" name="matrix" aria-hidden="true">
                                @foreach ($matrix as $key => $value)
                                    <option value="{{$value->id}}">{{$value->alias}}</option>
                                @endforeach
                              </select>
                                @if ($errors->has('role'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="col-md-12 input_fields_wrap">
                                <button type="button" class="btn btn-primary add_field_button">
                                    Tambah Sub Parameter
                                </button>
                                <button type="button" class="btn btn-primary" id="parameter_button">
                                    Tambah Parameter
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary none" id="register">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- <table class="table_wrap table table-striped table-bordered" cellspacing="0" width="100%"><tr><th rowspan="2">Parameter</th><th colspan="2">Nilai baku mutu</th></tr><tr><th>Tanda</th><th>Nilai</th></tr><tr><td><select class="table-wrapped form-control" name="parameter[]">@foreach ($parameter as $val1)<option value="{{$val1->id}}">{{$val1->name}}</option>@endforeach</select></td><td><select class="form-control" name="sign[]"><option value="no">-tidak bertanda-</option><option value="<"><</option><option vallue=">">></option><option value="=">=</option><option value="≤">≤</option><option value="≥">≥</option></select></td><td><input class="form-control" name="nilai[]"></td></tr></table> -->
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $('.' + 'matrix').select2({
        placeholder: '--',
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var max_fields      = 20; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
        
        var x = 0;
        <?php $keys = 0 ?>
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            <?php $keys++ ?>
            $("#parameter_button").hide();
            $("#register").show();
            if(x < max_fields){ //max input box allowed
                $(wrapper).append('<div><div style="margin-top: 15px;" class="form-group{{ $errors->has("subname") ? " has-error" : "" }}"><label for="name" class="col-md-2">Sub Name</label><div class="col-md-6"><input id="subname" type="text" class="form-control" name="subname[' + x + ']" value="{{ old("subname") }}">@if ($errors->has("subname"))<span class="help-block"><strong>{{ $errors->first("subname") }}</strong></span>@endif</div><div class="col-md-4"><button type="button" id="parameter_wrap'+x+'" class="btn btn-primary">Tambah Parameter</button></div></div><div class="col-md-12" style="margin-bottom:20px"><table id="table_wrap'+x+'" class="table table-striped table-bordered" cellspacing="0" width="100%"><tr><th rowspan="2">Parameter</th><th colspan="2">Nilai baku mutu</th></tr><tr><th>Tanda</th><th>Nilai</th></tr><tr><td><select class="table-wrapped form-control" name="parameter[' + x + '][]">@foreach ($parameter as $val1)<option value="{{$val1->id}}">{{$val1->name}}</option>@endforeach</select></td><td><select class="form-control" name="sign['+ x +'][]"><option value="no">-tidak bertanda-</option><option value="<"><</option><option vallue=">">></option><option value="=">=</option><option value="≤">≤</option><option value="≥">≥</option></select></td><td><input class="form-control" name="nilai['+ x +'][]"></td></tr></table><hr></div><a href="#" class="remove_field">Remove</a></div>');
                var add_parameter   = $("#parameter_wrap"+x); //Add button ID
                var table_wrapper   = $("#table_wrap"+x); //Fields wrapper
                var z               = x;
                $(add_parameter).click(function(e){ //on add input button click
                    e.preventDefault();
                        $(table_wrapper).append('<tr id="tr'+z+'"><td><select class="table-wrapped form-control" name="parameter[' + z + '][]">@foreach ($parameter as $val1)<option value="{{$val1->id}}">{{$val1->name}}</option>@endforeach</select></td><td><select class="form-control" name="sign['+ z +'][]"><option value="no">-tidak bertanda-</option><option value="<"><</option><option vallue=">">></option><option value="=">=</option><option value="≤">≤</option><option value="≥">≥</option></select></td><td><input class="form-control" name="nilai['+ z +'][]"></td><td><a href="#" id="remove_row'+z+'">Remove</a></td></tr>');
                });
                $(wrapper).on("click","#remove_row"+z, function(e){ //user click on remove text
                    e.preventDefault(); $('#tr'+z).remove();
                })
                 //text box increment
            }
            x++;
        });
        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        });

        $("#parameter_button").click(function(e){
            $(".add_field_button").hide();
            $("#register").show();
            $("#parameter_button").hide();
            $(wrapper).append('<div style="margin-bottom:15px" class="col-md-12 right"><button type="button" class="parameter_add btn btn-primary">Tambah Parameter</button></div><table id="table_wrap" class="table table-striped table-bordered" cellspacing="0" width="100%"><tr><th rowspan="2">Parameter</th><th colspan="2">Nilai baku mutu</th></tr><tr><th>Tanda</th><th>Nilai</th></tr><tr><td><select class="table-wrapped form-control" name="parameter2[]">@foreach ($parameter as $val1)<option value="{{$val1->id}}">{{$val1->name}}</option>@endforeach</select></td><td><select class="form-control" name="sign2[]"><option value="no">-tidak bertanda-</option><option value="<"><</option><option vallue=">">></option><option value="=">=</option><option value="≤">≤</option><option value="≥">≥</option></select></td><td><input class="form-control" name="nilai2[]"></td></tr></table>');
            $(".parameter_add").click(function(e){ //on add input button click
                e.preventDefault();
                    $("#table_wrap").append('<tr id="tr"><td><select class="table-wrapped form-control" name="parameter2[]">@foreach ($parameter as $val1)<option value="{{$val1->id}}">{{$val1->name}}</option>@endforeach</select></td><td><select class="form-control" name="sign2[]"><option value="no">-tidak bertanda-</option><option value="<"><</option><option vallue=">">></option><option value="=">=</option><option value="≤">≤</option><option value="≥">≥</option></select></td><td><input class="form-control" name="nilai2[]"></td><td><a href="#" id="remove_row">Remove</a></td></tr>');
            });
            $(wrapper).on("click","#remove_row", function(e){ //user click on remove text
                e.preventDefault(); $('#tr').remove();
            })
        });
    });
</script>
<script type="text/javascript">
     $('.' + 'table-wrapped').select2({
        placeholder: '--',
    });
</script>
@endsection
