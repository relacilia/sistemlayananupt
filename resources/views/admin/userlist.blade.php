@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                    <div class="col-md-6">
                        <h3 style="padding-left:18px">Users List</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        <a href="{{url('admin/register')}}"><button>Add User</button></a>
                    </div>
                    </div>
                </div>
                <div class="panel-body">
                     <div class="col-md-12">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>User ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>Role</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($users as $key => $value)
                                        <tr>
                                          <td style="text-align:center">{{$value->id}}</td>
                                          <td>{{$value->name}}</td>
                                          <td>{{$value->email}}</td>
                                          <td>{{$value->phone}}</td>
                                          <td>{{$value->address}}</td>
                                          <td>@foreach($data[$key] as $val)
                                                {{$val->roles->display_name}}<br>
                                            @endforeach
                                          </td>
                                          <td style="text-align:center">
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
                                              <ul class="dropdown-menu">
                                                <li><a href="{{ url('/admin/userlist/edit/'.$value->id) }}"><i class="fa fa-pen fa-fw" style="color:orange";></i> Edit</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="{{ url('/admin/userlist/delete/'.$value->id)}}"><i class="fa fa-trash fa-fw" style="color:red";></i> Delete</a></li>
                                              </ul>
                                            </div>
                                        </td>
                                        </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
