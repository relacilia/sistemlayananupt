@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                    <div class="col-md-12 center">
                        <h3 style="padding-left:18px">Analist List</h3>
                    </div>
                    </div>
                </div>
                <div class="panel-body">
                     <div class="col-md-12">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Parameter</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($analyst as $key => $value)
                                <tr>
                                <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/admin/analyst/save/'.$value->id) }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                  <td>{{$value->name}}</td>
                                  <td>
                                      <div id="para{{$key}}" class="col-md-12">
                                      <?php $count = $param[$key]->count(); ?>
                                      @foreach($param[$key] as $keys => $val)
                                        @if($keys != $count-1)
                                          {{$val->parameter->name}},
                                        @else
                                          {{$val->parameter->name}}
                                        @endif
                                      @endforeach
                                    </div>
                                      <div id="option{{$key}}" class="col-md-12 none">
                                      <select class="js-example-tokenizer form-control" name="parameter[]" multiple tabindex="-1" aria-hidden="true">
                                        @foreach ($parameter[$key] as $val1)
                                          @if($val1->check == 1)
                                            <option value="{{$val1->id}}" selected>{{$val1->name}}</option>
                                          @else
                                            <option value="{{$val1->id}}">{{$val1->name}}</option>
                                          @endif                                  
                                        @endforeach
                                      </select> 
                                      </div>                                  
                                  </td>
                                  <td style="text-align:center">
                                    <div class="btn-group">
                                      <button type="button" id="edit{{$key}}" style="display:inline">Edit</button>
                                      <button type="submit" id="save{{$key}}" style="display:none;">Save</button>
                                      <button type="button" id="cancel{{$key}}" style="display:none;">Cancel</button>
                                    </div>
                                  </td>
                                 </form>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                 </form>
                 <div class="col-md-12 left"><a href="{{ url('/admin/home') }}"><button class="btn btn-primary"><< Back</button></a></div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
$(".js-example-tokenizer").select2({
  tags: true,
  tokenSeparators: [',', ' ']
})
</script>
<?php $count = $analyst->count() ?>
@for($i=0; $i<$count; $i++)
<script>
$(document).ready(function(){
    $("#edit<?php echo ($i) ?>").click(function(){
        $("#edit<?php echo ($i) ?>").hide();
        document.getElementById("save<?php echo ($i) ?>").style.display = "inline";
        document.getElementById("cancel<?php echo ($i) ?>").style.display = "inline";
        $("#option<?php echo ($i) ?>").show();
        $("#para<?php echo ($i) ?>").hide();
    });
    $("#cancel<?php echo ($i) ?>").click(function(){
        $("#edit<?php echo ($i) ?>").show();
        document.getElementById("save<?php echo ($i) ?>").style.display = "none";
        document.getElementById("cancel<?php echo ($i) ?>").style.display = "none";
        $("#option<?php echo ($i) ?>").hide();
        $("#para<?php echo ($i) ?>").show();

    });
});
</script>
@endfor
@endsection
