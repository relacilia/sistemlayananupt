@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                    <div class="col-md-6">
                        <h3 style="padding-left:18px">Sample Matrix List</h3>
                    </div>
                     <div class="col-md-6" style="text-align:right">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#matrixModal">Add Matrix</button>
                    </div>
                    </div>
                </div>
                <div class="panel-body">
                     <div class="col-md-12">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Alias</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($matrix as $key => $value)
                                <!-- Modal menghapus Job -->
                                            <div class="modal fade" id="matrix_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-body">
                                                    <p>Apakah Anda yakin ingin menghapus Matrix {{$value->id}} ?</p>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                    <a href="{{ url('/admin/matrix/delete/'.$value->id) }}"><button type="button" class="btn btn-danger">Delete</button></a>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                <tr>
                                <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/admin/matrix/save/'.$value->id) }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                  <td>
                                    <div id="alias{{$key}}" class="col-md-12">
                                      {{$value->alias}}
                                    </div>
                                    <div id="a_edit{{$key}}" class="col-md-12 none">
                                      <input name="alias" value="{{$value->alias, old('alias')}}" required>
                                    </div>
                                  </td>
                                  <td>
                                    <div id="name{{$key}}" class="col-md-12">
                                      {{$value->name}}
                                    </div>
                                    <div id="n_edit{{$key}}" class="col-md-12 none">
                                      <input name="name" value="{{$value->name, old('name')}}" required>
                                    </div>
                                  </td>
                                  <td style="text-align:center">
                                    <div class="btn-group">
                                      <button type="button" id="edit{{$key}}" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Edit</button>       
                                      <a href="{{ url('/admin/matrix/delete/'.$value->id) }}"><button type="button" id="delete{{$key}}" class="btn btn-default" aria-haspopup="true" aria-expanded="false">Delete</button></a>     
                                      <button type="submit" id="save{{$key}}" style="display:none;">Save</button>
                                      <button type="button" id="cancel{{$key}}" style="display:none;">Cancel</button>
                                    </div>
                                  </td>
                                 </form>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('modal.matrix_add')
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php $count = $matrix->count() ?>
@for($i=0; $i<$count; $i++)
<script>
$(document).ready(function(){
    $("#edit<?php echo ($i) ?>").click(function(){
        $("#edit<?php echo ($i) ?>").hide();
        $("#delete<?php echo ($i) ?>").hide();
        document.getElementById("save<?php echo ($i) ?>").style.display = "inline";
        document.getElementById("cancel<?php echo ($i) ?>").style.display = "inline";
        $("#a_edit<?php echo ($i) ?>").show();
        $("#n_edit<?php echo ($i) ?>").show();
        $("#alias<?php echo ($i) ?>").hide();
        $("#name<?php echo ($i) ?>").hide();
    });
    $("#cancel<?php echo ($i) ?>").click(function(){
        $("#edit<?php echo ($i) ?>").show();
        $("#delete<?php echo ($i) ?>").show();
        document.getElementById("save<?php echo ($i) ?>").style.display = "none";
        document.getElementById("cancel<?php echo ($i) ?>").style.display = "none";
        $("#a_edit<?php echo ($i) ?>").hide();
        $("#n_edit<?php echo ($i) ?>").hide();
        $("#alias<?php echo ($i) ?>").show();
        $("#name<?php echo ($i) ?>").show();
    });
});
</script>
@endfor
@endsection

