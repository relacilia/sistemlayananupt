@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('partials.status-alert')
            <div class="panel panel-default">
                <div class="panel-body">
                     <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                               <a href="{{url('admin/joblist')}}"><div class="kotak">Job</div></a>
                            </div>
                            <div class="col-md-6">
                                <a href="{{url('admin/userlist')}}"><div class="kotak">User</div></a>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-6">
                                <a href="{{url('admin/analyst')}}"><div class="kotak">Analis</div></a>
                            </div>
                            <div class="col-md-6">
                                <a href="{{url('admin/customerlist')}}"><div class="kotak">Customer</div></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="{{url('admin/matrix')}}"><div class="kotak">Matrix</div></a>
                            </div>
                            <div class="col-md-6">
                                <a href="{{url('admin/procedurelist')}}"><div class="kotak">Sampling Procedure</div></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="{{url('admin/parameterlist')}}"><div class="kotak">Parameter</div></a>
                            </div>
                            <div class="col-md-6">
                                <a href="{{url('admin/reftestlist')}}"><div class="kotak">Baku Mutu</div></a>
                            </div>
                        </div>
                    </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
