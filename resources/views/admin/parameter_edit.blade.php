@extends('layouts.app')

@section('content')
<div class="container" style="width:1300px">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Edit Parameter</h4></div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/parameterlist/edit/'.$parameter->id.'/save') }}">
                        {{ csrf_field() }}

                        <div class="row">
                            <!-- part -->
                            <div class="col-md-3">
                                <p style="margin-top: 8px">Parameter Name <span style="color:red">*</span></p>
                            </div>
                            <div class="col-md-9" >
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                  <input id="name" type="text" class="form-control" name="name" value="{{ $parameter->name, old('name') }}" placeholder="" required>

                                  @if ($errors->has('name'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('name') }}</strong>
                                      </span>
                                  @endif   
                                </div>
                            </div>
                            <!-- endpart -->
                        </div>
                        <div class="row">
                            <!-- part -->
                            <div class="col-md-3">
                                <p style="margin-top: 8px">Sampling Procedure<span style="color:red">*</span></p>
                            </div>
                            <div class="col-md-9" >
                                <div class="form-group{{ $errors->has('procedure') ? ' has-error' : '' }}">
                                  {{Form::select('procedure',$procedure, $parameter->procedure_id, ['class' => 'form-control', 'id' => 'procedure'])}}

                                  @if ($errors->has('procedure'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('procedure') }}</strong>
                                      </span>
                                  @endif   
                                </div>
                            </div>
                            <!-- endpart -->
                        </div>
                        <div class="row">
                            <!-- part -->
                            <div class="col-md-3">
                                <p style="margin-top: 8px">Standard<span style="color:red">*</span></p>
                            </div>
                            <div class="col-md-9" >
                                <div class="form-group{{ $errors->has('standard') ? ' has-error' : '' }}">
                                  <input id="standard" type="text" class="form-control" name="standard" value="{{ $parameter->standard, old('standard') }}" placeholder="" required>

                                  @if ($errors->has('standard'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('standard') }}</strong>
                                      </span>
                                  @endif   
                                </div>
                            </div>
                            <!-- endpart -->
                        </div>
                        <div class="row">
                            <!-- part -->
                            <div class="col-md-3">
                                <p style="margin-top: 8px">Price<span style="color:red">*</span></p>
                            </div>
                            <div class="col-md-9" >
                                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                  <input id="price" type="text" class="form-control" name="price" value="{{ $parameter->price, old('price') }}" placeholder="" required>

                                  @if ($errors->has('price'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('price') }}</strong>
                                      </span>
                                  @endif   
                                </div>
                            </div>
                            <!-- endpart -->
                        </div>
                        <div class="row">
                            <!-- part -->
                            <div class="col-md-3">
                                <p style="margin-top: 8px">Accreditation<span style="color:red">*</span></p>
                            </div>
                            <div class="col-md-9" >
                                <div class="form-group{{ $errors->has('accreditation') ? ' has-error' : '' }}">
                                  <select name="accreditation" id="accreditation" class="form-control" required>
                                    <option @if($parameter->accreditation == 1) selected @endif value="1">Yes</option>
                                    <option @if($parameter->accreditation == 0) selected @endif value="0">No</option>
                                  </select>

                                  @if ($errors->has('accreditation'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('accreditation') }}</strong>
                                      </span>
                                  @endif   
                                </div>
                            </div>
                            <!-- endpart -->
                        </div>
                        <div class="row">
                            <!-- part -->
                            <div class="col-md-3">
                                <p style="margin-top: 8px">MDL<span style="color:red">*</span></p>
                            </div>
                            <div class="col-md-9" >
                                <div class="form-group{{ $errors->has('mdl') ? ' has-error' : '' }}">
                                  <input id="mdl" type="text" class="form-control" name="mdl" value="{{ $parameter->MDL, old('mdl') }}" placeholder="" required>

                                  @if ($errors->has('mdl'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('mdl') }}</strong>
                                      </span>
                                  @endif   
                                </div>
                            </div>
                            <!-- endpart -->
                        </div>
                         <div class="row">
                            <!-- part -->
                            <div class="col-md-3">
                                <p style="margin-top: 8px">Analyst<span style="color:red">*</span></p>
                            </div>
                            <div class="col-md-9" >
                                <div class="form-group{{ $errors->has('analyst') ? ' has-error' : '' }}">
                                    <select class="js-example-tokenizer form-control" name="analyst[]" multiple tabindex="-1" aria-hidden="true">
                                        @foreach ($allAnalis as $val1)
                                          @if($val1->check == 1)
                                            <option value="{{$val1->user_id}}" selected>{{$val1->users->name}}</option>
                                          @else
                                            <option value="{{$val1->user_id}}">{{$val1->users->name}}</option>
                                          @endif                                  
                                        @endforeach
                                    </select> 
                                  @if ($errors->has('analyst'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('analyst') }}</strong>
                                      </span>
                                  @endif   
                                </div>
                            </div>
                            <!-- endpart -->
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    SIMPAN
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".js-example-tokenizer").select2({
      tags: true,
      tokenSeparators: [',', ' ']
    });
    $('#' + 'procedure').select2({
        placeholder: '--',
    });
</script>
@endsection