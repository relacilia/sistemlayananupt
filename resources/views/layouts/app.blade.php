<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Sistem Layanan UPT LabLing DLH Prov Jatim') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dataTable/bootstrap.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dataTable/dataTables.bootstrap.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.css') }}"/>
    <link href="{{ asset('css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.css') }}" rel="stylesheet">
    

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <script src="{{ asset('/js/app.js') }}"></script>
    <script src="{{ asset('/js/jquery-1.11.1.min.js') }}"></script>
    <script src="{{ asset('/js/jquery-ui-1.11.1.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.appendGrid-1.6.2.js') }}"></script>

     <script src="{{ asset('/js/raphael.min.js') }}"></script>
      <script src="{{ asset('/js/morris-0.4.1.min.js') }}"></script>
    <script src="{{ asset('/js/dataTable/jquery-1.12.4.js') }}"></script>
    <script src="{{ asset('/js/dataTable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.select2.js') }}"></script>

</head>
</head>
<body style="background-color: #fafafa;">
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" style="background-image: url({{ URL::asset('images/qwe6.jpg') }});">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    @if(Auth::user()->hasRole('Front_Office'))
                        <a class="navbar-brand" href="{{ url('/front/home') }}">
                    @elseif(Auth::user()->hasRole('Analyst'))
                        <a class="navbar-brand" href="{{ url('/analyst/home') }}">
                    @elseif(Auth::user()->hasRole('Supervisor'))
                        <a class="navbar-brand" href="{{ url('/supervisor/home') }}">
                    @elseif(Auth::user()->hasRole('Technical_manager'))
                        <a class="navbar-brand" href="{{ url('/tm/home') }}">
                    @elseif(Auth::user()->hasRole('Administrator'))
                        <a class="navbar-brand" href="{{ url('/admin/home') }}">
                    @elseif(Auth::user()->hasRole('Head_of_unit'))
                        <a class="navbar-brand" href="{{ url('/head/home') }}">
                    @endif
                        {{ config('app.name', 'Sistem Layanan UPT LabLing DLH Prov Jatim') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>


                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            @if (Auth::user()->hasRole('Front_Office') )
                                <li @if($menu_active == 'registrasi') id="active" @endif ><a href="{{ url('/front/home') }}">Registrasi</a> </li>
                                <li @if($menu_active == 'job') id="active" @endif ><a href="{{ url('/front/joblist') }}">List Data</a></li>
                                <li @if($menu_active == 'customer') id="active" @endif><a href="{{ url('/front/customerlist') }}">List Customer</a></li>
                                <li @if($menu_active == 'sertif') id="active" @endif><a href="{{ url('/front/certificatelist') }}">Cetak Sertifikat</a></li>
                                <li @if($menu_active == 'price') id="active" @endif><a href="{{ url('/front/pricelist') }}">Daftar Harga</a></li>
                            @elseif (Auth::user()->hasRole('Analyst') && Auth::user()->hasRole('Supervisor'))
                                <li class="dropdown">
                                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <i class="fa fa-bell" aria-hidden="true" style="padding-right: 10px;color:dimgray"></i>{{$notif->count()}} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        @foreach($notif as $value)
                                            <li><a href="{{ url('/analyst/samplelist/'.$value->link)}}">Revisi {{$value->parameter->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                                @if($menu_active != null)
                                    @foreach($param as $value)
                                        <li  @if($menu_active->name == $value->parameter->name) id="active" @endif ><a href="{{ url('/analyst/'.$value->parameter_id) }}">{{$value->parameter->name}}</a></li>
                                    @endforeach
                                        <li><a href="{{ url('/supervisor/home')}}">Penyelia</a></li>
                                @else
                                    @foreach($param as $value)
                                        <li><a href="{{ url('/analyst/'.$value->parameter_id) }}">{{$value->parameter->name}}</a></li>
                                    @endforeach
                                        <li id="active"><a href="{{ url('/supervisor/home')}}">Penyelia</a></li>
                                @endif


                            @elseif (Auth::user()->hasRole('Analyst'))
                            <li class="dropdown">
                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <i class="fa fa-bell" aria-hidden="true" style="padding-right: 10px;color:dimgray"></i>{{$notif->count()}} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    @foreach($notif as $value)
                                        <li><a href="{{ url('/analyst/samplelist/'.$value->link)}}">Revisi {{$value->parameter->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                             @foreach($param as $value)
                                    <li  @if($menu_active->name == $value->parameter->name) id="active" @endif ><a href="{{ url('/analyst/'.$value->parameter_id) }}">{{$value->parameter->name}}</a></li>
                                @endforeach
                            @elseif (Auth::user()->hasRole('Head_of_unit'))
                                <?php $year= \Carbon\Carbon::now()->year ?>
                                <li @if($menu_active == 'Harian') id="active" @endif ><a href="{{ url('/head/home') }}">Laporan Harian</a></li>
                                <li @if($menu_active == 'Bulanan') id="active" @endif ><a href="{{ url('/head/bulanan/'.$year) }}">Laporan Bulanan</a></li>
                                <li @if($menu_active == 'Tahunan') id="active" @endif><a href="{{ url('/head/tahunan') }}">Laporan Tahunan</a></li>
                            @endif
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        @if(Auth::user()->hasRole('Front_Office'))
                                            <a href="{{ url('/front/home') }}">
                                        @elseif(Auth::user()->hasRole('Analyst'))
                                            <a href="{{ url('/analyst/home') }}">
                                        @elseif(Auth::user()->hasRole('Supervisor'))
                                            <a href="{{ url('/supervisor/home') }}">
                                        @elseif(Auth::user()->hasRole('Technical_manager'))
                                            <a href="{{ url('/tm/home') }}">
                                        @elseif(Auth::user()->hasRole('Administrator'))
                                            <a href="{{ url('/admin/home') }}">
                                        @elseif(Auth::user()->hasRole('Head_of_unit'))
                                            <a href="{{ url('/head/home') }}">
                                        @elseif(Auth::user()->hasRole('Section_head'))
                                            <a href="{{ url('/kasi/home') }}">
                                        @elseif(Auth::user()->hasRole('Viewer'))
                                            <a href="{{ url('/viewer/home') }}">
                                        @endif
                                            Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>
</body>
</html>
