<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function () {
//     return view('errors.404');
// });

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::group(['prefix' => 'front', 'middleware' => ['role:Front_Office']], function(){
	Route::get('/', 'FrontController@index');
	Route::get('/home', 'FrontController@index');
	Route::get('/step2', 'FrontController@registrasi_2');
	Route::get('/step3', 'FrontController@registrasi_3');
	Route::get('/review', 'FrontController@review');
	Route::post('/tt', 'FrontController@save_registrasi');
	Route::get('/downloadExcel/{data}', 'ExcelController@downloadExcel');
	Route::get('/downloadPdf/{data}', 'ExcelController@downloadPdf');
	Route::get('/joblist', 'FrontController@list_job');
	Route::get('/customerlist', 'FrontController@customer_list');
	Route::get('/customerlist/edit/{id}', 'FrontController@edit_customer');
	Route::post('/customerlist/edit/save/{id}', 'FrontController@customer_save');
	Route::get('/joblist/samplelist/{id}', 'FrontController@list_sample');
	Route::post('/customerlist/add', 'FrontController@add_customer');
	Route::get('/joblist/edit/{id}', 'FrontController@edit_job');
	Route::get('/customerlist/nonactive/{id}', 'FrontController@nonactive_customer');
	Route::post('/joblist/edit/save', 'FrontController@save_edit');
	Route::get('/certificatelist', 'FrontController@certificate');
	Route::get('/certificatelist/detail/{id}', 'FrontController@certificate_sample');
	// Route::get('/certificatelist/detail/print/{data}', 'ExcelController@downloadSertifikat');
	Route::get('/certificatelist/detail/print/{id}', 'FrontController@certificate_per_sample');
	Route:: get('/coba', ['as'=>'coba','uses'=>'WordController@index']);
	Route:: get('/pricelist', ['as'=>'pricelist','uses'=>'FrontController@pricelist']);
	Route:: get('/pricelist/detail', ['as'=>'detailprice','uses'=>'FrontController@detailprice']);
});

Route::group(['prefix' => 'analyst', 'middleware' => ['role:Analyst']], function(){
	Route::get('/', 'AnalystController@index');
	Route::get('/home', 'AnalystController@index');
	Route::get('/samplelist/{id}/{date}', 'AnalystController@sample_list');
	Route::get('/{id}', 'AnalystController@get_sample');
	Route::post('/result/add/{id}', 'AnalystController@add_result');
	Route::get('/downloadExcelAnalis/{id}/{data}', 'ExcelController@downloadExcelAnalis');
	Route::post('/uploadExcelAnalis', 'ExcelController@get_file');
});

Route::group(['prefix' => 'supervisor','middleware' => ['role:Supervisor']], function(){
	Route::get('/', 'SupervisorController@index');
	Route::get('/home', 'SupervisorController@index');
	Route::get('/parameterlist/{id}', 'SupervisorController@para_sample_list');
	Route::get('/parameterlist/acceptation/{id}', 'SupervisorController@acceptation');
	Route::get('/parameterlist/revision/{id}', 'SupervisorController@revision');
});

Route::group(['prefix' => 'tm','middleware' => ['role:Technical_manager']], function(){
	Route::get('/', 'TMController@index');
	Route::get('/home', 'TMController@index');
	Route::get('/samplelist/{id}', 'TMController@samplelist');
	Route::get('/samplelist/{id}/detail/{kode}', 'TMController@detail_sample');
	Route::get('/parameterlist/acceptation/{id}', 'TMController@acceptation');
	Route::get('/parameterlist/revision/{id}', 'TMController@revision');
});

Route::group(['prefix' => 'admin','middleware' => ['role:Administrator']], function(){
	Route::get('/', 'AdminController@index');
	Route::get('/home', 'AdminController@index');
	Route::get('/register', 'AdminController@register');
	Route::post('/registration', 'AdminController@registration');
	Route::get('/userlist', 'AdminController@userlist');
	Route::get('/userlist/edit/{id}', 'AdminController@user_edit');
	Route::get('/userlist/edit/{id}/resetpassword', 'AdminController@user_edit_password');
	Route::post('/userlist/edit/{id}/resetpassword/save', 'AdminController@user_edit_password_save');
	Route::get('/userlist/delete/{id}', 'AdminController@user_delete');
	Route::post('/userlist/edit/save/{id}', 'AdminController@user_edit_save');
	Route::get('/analyst', 'AdminController@analyst');
	Route::post('/analyst/save/{id}', 'AdminController@analyst_update_save');
	Route::get('/matrix', 'AdminController@matrix');
	Route::get('/matrix/delete/{id}', 'AdminController@matrix_delete');
	Route::post('/matrix/new/save', 'AdminController@matrix_new_save');
	Route::post('/matrix/save/{id}', 'AdminController@matrix_update_save');
	Route::get('/procedurelist', 'AdminController@procedure');
	Route::post('/procedurelist/new/save', 'AdminController@procedure_new_save');
	Route::post('/procedurelist/save/{id}', 'AdminController@procedure_save');
	Route::get('/parameterlist', 'AdminController@parameter');
	Route::post('/parameterlist/new/save', 'AdminController@parameter_new_save');
	Route::get('/parameterlist/edit/{id}', 'AdminController@parameter_edit');
	Route::post('/parameterlist/edit/{id}/save', 'AdminController@parameter_edit_save');
	Route::get('/customerlist', 'AdminController@customer_list');
	Route::post('/customerlist/add', 'FrontController@add_customer');
	Route::get('/customerlist/edit/{id}', 'AdminController@edit_customer');
	Route::post('/customerlist/edit/save/{id}', 'AdminController@customer_save');
	Route::get('/customerlist/nonactive/{id}', 'FrontController@nonactive_customer');
	Route::get('/reftestlist', 'AdminController@reftest_list');
	Route::get('/reftestlist/add', 'AdminController@reftest_add');
	Route::get('/reftestlist/edit/{id}', 'AdminController@reftest_edit');
	Route::post('/reftestlist/edit/{id}/save', 'AdminController@reftest_edit_save');
	Route::get('/reftestlist/delete/{id}', 'AdminController@reftest_delete');
	Route::post('/reftestlist/add/save', 'AdminController@reftest_add_save');
	Route::get('/joblist', 'AdminController@joblist');
	Route::get('/joblist/samplelist/{id}', 'AdminController@list_sample');
	Route::get('/joblist/edit/{id}', 'FrontController@edit_job');
	Route::post('/joblist/edit/save', 'AdminController@save_edit');
	Route::get('/joblist/samplelist/{id}/edit', 'AdminController@sample_edit');
	Route::get('/joblist/samplelist/{id}/delete', 'AdminController@job_delete');
	Route::get('/joblist/samplelist/{id}/delete/{sample}', 'AdminController@sample_delete');
	Route::post('/joblist/samplelist/{id}/edit/parameter/save', 'AdminController@save_parameter_sample');
	Route::get('/joblist/samplelist/{id}/edit/matrix/edit', 'AdminController@edit_matrix');
	Route::post('/joblist/samplelist/{id}/edit/save', 'AdminController@save_edit_sample');
});

Route::group(['prefix' => 'head','middleware' => ['role:Head_of_unit']], function(){
	Route::get('/', 'HeadController@index');
	Route::get('/home', 'HeadController@index');
	Route::post('/harian/detail', 'HeadController@daily');
	Route::get('/bulanan/{year}', 'HeadController@monthly_graph');
	Route::post('/bulanan/detail', 'HeadController@monthly');
	Route::get('/tahunan', 'HeadController@yearly_graph');
	Route::get('/tahunan/detail', 'HeadController@yearly');
});

Route::group(['prefix' => 'kasi','middleware' => ['role:Section_head']], function(){
	Route::get('/', 'SectionheadController@index');
	// Route::get('/home', 'SectionheadController@index');
	Route::get('/latelist', 'SectionheadController@latelist');
	Route::get('/home', 'HeadController@index');
	Route::post('/harian/detail', 'HeadController@daily');
	Route::get('/bulanan/{year}', 'HeadController@monthly_graph');
	Route::post('/bulanan/detail', 'HeadController@monthly');
	Route::get('/tahunan', 'HeadController@yearly_graph');
	Route::get('/tahunan/detail', 'HeadController@yearly');
});

Route::group(['prefix' => 'viewer','middleware' => ['role:Viewer']], function(){
	Route::get('/', 'ViewerController@index');
	// Route::get('/home', 'ViewerController@index');
	Route::get('/home', 'HeadController@index');
	Route::post('/harian/detail', 'HeadController@daily');
	Route::get('/bulanan/{year}', 'HeadController@monthly_graph');
	Route::post('/bulanan/detail', 'HeadController@monthly');
	Route::get('/tahunan', 'HeadController@yearly_graph');
	Route::get('/tahunan/detail', 'HeadController@yearly');
});

Route:: get('matrix/pro', ['as'=>'dropdown_procedure','uses'=>'FrontController@procedure']);
Route:: get('matrix/ref', ['as'=>'dropdown','uses'=>'FrontController@ref']);
Route:: get('ref/sub', ['as'=>'dropdown2','uses'=>'FrontController@ref_sub1']);
Route:: get('ref/sub2', ['as'=>'dropdown21','uses'=>'FrontController@ref_sub2']);
Route:: get('ref/param', ['as'=>'dropdown3','uses'=>'FrontController@ref_param']);
Route:: get('customer', ['as'=>'dataCustom','uses'=>'FrontController@dataCustom']);
Route:: get('customer2', ['as'=>'dataCustom2','uses'=>'FrontController@dataCustom2']);
Route:: get('date', ['as'=>'getDate','uses'=>'FrontController@interval_date']);
