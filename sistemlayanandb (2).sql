-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02 Mei 2017 pada 19.22
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistemlayanandb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `assignment`
--

CREATE TABLE `assignment` (
  `user_id` int(11) NOT NULL,
  `parameter_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `assignment`
--

INSERT INTO `assignment` (`user_id`, `parameter_id`, `created_at`, `updated_at`) VALUES
(2, 1, '2017-04-19 14:57:26', '0000-00-00 00:00:00'),
(3, 2, '2017-04-19 14:57:26', '0000-00-00 00:00:00'),
(2, 3, '2017-04-19 14:57:26', '0000-00-00 00:00:00'),
(3, 6, '2017-04-19 14:57:26', '0000-00-00 00:00:00'),
(2, 8, '2017-04-19 14:57:26', '0000-00-00 00:00:00'),
(3, 3, '2017-04-19 14:57:26', '0000-00-00 00:00:00'),
(3, 8, '2017-04-19 14:57:26', '0000-00-00 00:00:00'),
(3, 1, '2017-04-19 14:57:26', '0000-00-00 00:00:00'),
(7, 4, '2017-04-19 14:57:26', '0000-00-00 00:00:00'),
(7, 5, '2017-04-19 14:57:26', '0000-00-00 00:00:00'),
(7, 7, '2017-04-19 14:57:26', '0000-00-00 00:00:00'),
(22, 6, '2017-04-19 15:00:54', '2017-04-19 15:00:54'),
(25, 3, '2017-04-19 15:01:25', '2017-04-19 15:01:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `name_customer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nonaktif` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `customer`
--

INSERT INTO `customer` (`id_customer`, `name_customer`, `phone`, `fax`, `address`, `city`, `nonaktif`, `created_at`, `updated_at`) VALUES
(1, 'PT. NUSANTARA CHEMICAL INDONESIA', '082141009225', '-', 'JL.PERUM GRAHA JUANDA BLOK A-17 DAMARSI', 'SIDOARJO', 0, '2017-04-09 00:27:50', '2017-04-27 11:09:46'),
(2, 'PT. ISTANAH TIARA', '031-7496869', '-', 'JL. BUNTARAN NO. 8', 'SURABAYA', 0, '2017-04-09 00:33:04', '2017-04-09 00:33:04'),
(3, 'PUSKESMAS TANJUNG', '-', '-', 'JL. CEMARA NO.9 KEC TANJUNG', 'BREBES', 0, '2017-04-09 00:34:06', '2017-04-09 00:34:06'),
(4, 'PT INDOFOOD CBP SUKSES MAKMUR TBk', '0343-656177', '-', 'Jl. Raya Cangkringmalang KM 32 Beji', 'Pasuruan', 0, '2017-04-09 00:48:11', '2017-04-09 00:48:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer_departement`
--

CREATE TABLE `customer_departement` (
  `customer_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `customer_departement`
--

INSERT INTO `customer_departement` (`customer_id`, `department_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL),
(3, 1, NULL, NULL),
(4, 1, NULL, NULL),
(5, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `department`
--

CREATE TABLE `department` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_department` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `department`
--

INSERT INTO `department` (`id`, `name_department`, `created_at`, `updated_at`) VALUES
(1, 'Industri', NULL, NULL),
(2, 'Kesehatan', NULL, NULL),
(3, 'Pelayaran', NULL, NULL),
(4, 'Pendidikan', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `hierarchy_ref_test`
--

CREATE TABLE `hierarchy_ref_test` (
  `id_hierarchy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_child` int(11) DEFAULT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `hierarchy_ref_test`
--

INSERT INTO `hierarchy_ref_test` (`id_hierarchy`, `id_child`, `id_parent`, `created_at`, `updated_at`) VALUES
('', 4, 1, NULL, NULL),
('', 5, 1, NULL, NULL),
('', 6, 4, NULL, NULL),
('', 7, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `job`
--

CREATE TABLE `job` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `sampled_by` varchar(50) NOT NULL,
  `attention` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `status` varchar(50) DEFAULT NULL,
  `date_recent` date NOT NULL,
  `time_recent` time NOT NULL,
  `number_samples` int(11) NOT NULL,
  `price` varchar(50) DEFAULT NULL,
  `interval` date NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `job`
--

INSERT INTO `job` (`id`, `customer_id`, `sampled_by`, `attention`, `location`, `status`, `date_recent`, `time_recent`, `number_samples`, `price`, `interval`, `updated_at`, `created_at`) VALUES
(7, 4, 'Eka', 'Mira', 'Surabaya', 'Onprogress (TM)', '2017-04-10', '08:33:00', 3, '0', '2017-04-16', '2017-04-18 06:22:31', '2017-04-10 02:14:23'),
(9, 3, 'Momo', 'Ani', 'Malang', 'Onprogress', '2016-12-31', '02:46:00', 2, '0', '2017-04-16', '2017-05-01 03:23:42', '2017-04-10 07:50:30'),
(10, 4, 'Koki', 'Lola', 'Pasuruan', 'Onprogress (TM)', '2017-04-16', '11:44:00', 3, '0', '2017-04-22', '2017-04-17 05:13:18', '2017-04-16 16:51:31'),
(11, 4, 'Mona', 'Fifi', 'Pasuruan', 'OK', '2017-04-11', '01:43:00', 1, '0', '2017-04-23', '2017-04-17 06:59:19', '2017-04-17 06:44:08'),
(12, 4, 'Moa', 'Fua', 'Pasuruan', 'OK', '2017-04-12', '01:44:00', 1, '0', '2017-04-23', '2017-04-17 06:59:25', '2017-04-17 06:45:14'),
(13, 4, 'Momo', 'Uti', 'Pasuruan', 'OK', '2017-04-13', '01:45:00', 1, '0', '2017-04-23', '2017-04-17 06:59:30', '2017-04-17 06:46:23'),
(14, 4, 'Juan', 'Juju', 'Pasuruan', 'OK', '2017-04-14', '01:46:00', 1, '0', '2017-04-23', '2017-04-17 07:03:20', '2017-04-17 06:47:19'),
(15, 2, 'qwe', 'Maudy', 'Surabaya', 'Onprogress', '2017-04-27', '05:21:00', 2, '0', '2017-05-03', '2017-04-30 03:52:20', '2017-04-27 10:34:00'),
(16, 3, 'Mona', 'Maudy', 'Brebes', 'Onprogress', '2017-04-28', '08:30:00', 2, '21000', '2017-05-04', '2017-04-30 03:52:11', '2017-04-28 13:35:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `late_record`
--

CREATE TABLE `late_record` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_input` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `late_record`
--

INSERT INTO `late_record` (`id`, `user_id`, `duration`, `created_at`, `updated_at`, `date_input`) VALUES
(1, 3, 3, '2017-05-02 16:35:24', '2017-05-02 16:35:24', '2017-05-02 16:35:24'),
(2, 3, 4, '2017-05-02 17:00:24', '2017-05-02 17:00:24', '2017-05-02 17:00:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `list_analyst`
--

CREATE TABLE `list_analyst` (
  `id_user` int(11) NOT NULL,
  `id_parameter` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_02_04_085150_create_metode_pengujian_table', 1),
(4, '2017_02_04_135310_create_sample_matrix_table', 1),
(5, '2017_02_07_022446_create_ref_test_table', 1),
(6, '2017_02_07_022552_create_sub_ref_table', 1),
(7, '2017_02_07_022626_create_parameter_table', 1),
(8, '2017_02_07_022715_create_parameter_sample_table', 1),
(9, '2017_02_07_023539_create_sample_table', 1),
(10, '2017_02_07_040651_create_jenis_user_table', 1),
(11, '2017_02_07_073650_create_list_analis_table', 1),
(12, '2017_02_07_074922_create_customer_table', 1),
(13, '2017_02_07_074942_create_jenis_kegiatan_table', 1),
(14, '2017_02_07_075915_create_kegiatan_customer_table', 1),
(15, '2017_02_07_090200_create_ref_parameter_table', 1),
(16, '2017_02_07_090739_create_result_table', 1),
(17, '2017_02_09_003310_entrust_setup_tables', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `parameter`
--

CREATE TABLE `parameter` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `procedure_id` int(11) NOT NULL,
  `MDL` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `accreditation` smallint(6) NOT NULL,
  `standard` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `check` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `parameter`
--

INSERT INTO `parameter` (`id`, `name`, `procedure_id`, `MDL`, `price`, `accreditation`, `standard`, `check`, `created_at`, `updated_at`) VALUES
(1, 'CO', 0, '', 1000, 0, '', NULL, NULL, NULL),
(2, 'DOH', 0, '', 2000, 0, '', NULL, NULL, NULL),
(3, 'BOH', 0, '', 3000, 0, '', NULL, NULL, NULL),
(4, 'COD', 0, '', 4000, 0, '', NULL, NULL, NULL),
(5, 'BOD', 0, '', 5000, 0, '', NULL, NULL, NULL),
(6, 'CL', 0, '', 6000, 0, '', NULL, NULL, NULL),
(7, 'O', 0, '', 7000, 0, '', NULL, NULL, NULL),
(8, 'pH', 0, '', 8000, 0, '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `parameter_sample`
--

CREATE TABLE `parameter_sample` (
  `id` int(191) NOT NULL,
  `sample_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parameter_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status_parameter` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `parameter_sample`
--

INSERT INTO `parameter_sample` (`id`, `sample_id`, `parameter_id`, `created_at`, `updated_at`, `user_id`, `status_parameter`) VALUES
(359, '51', 1, '2017-04-10 02:14:23', '2017-04-16 17:21:21', 3, 'OK (TM)'),
(360, '51', 2, '2017-04-10 02:14:23', '2017-04-16 17:21:25', 3, 'OK (TM)'),
(361, '52', 1, '2017-04-10 02:14:24', '2017-04-18 06:22:29', 2, 'Onprogress (TM)'),
(362, '52', 2, '2017-04-10 02:14:24', '2017-04-18 06:22:30', 3, 'Onprogress (TM)'),
(363, '52', 3, '2017-04-10 02:14:24', '2017-04-18 06:22:31', 2, 'Onprogress (TM)'),
(373, '55', 1, '2017-04-10 07:50:30', '2017-04-10 07:50:30', 2, 'Onprogress (AN)'),
(374, '55', 2, '2017-04-10 07:50:30', '2017-05-02 15:52:22', 3, 'onprogress (SV)'),
(375, '55', 6, '2017-04-10 07:50:30', '2017-04-10 07:56:38', 3, 'Onprogress (SV)'),
(376, '55', 8, '2017-04-10 07:50:30', '2017-04-10 07:50:30', 2, 'Onprogress (AN)'),
(377, '56', 1, '2017-04-10 07:50:30', '2017-04-10 07:50:30', 2, 'Onprogress (AN)'),
(378, '56', 5, '2017-04-10 07:50:30', '2017-04-10 07:50:30', 7, 'Onprogress (AN)'),
(379, '56', 6, '2017-04-10 07:50:30', '2017-04-18 05:50:41', 3, 'Onprogress (TM)'),
(380, '57', 1, '2017-04-16 16:51:32', '2017-04-16 17:04:05', 2, 'Onprogress (TM)'),
(381, '57', 2, '2017-04-16 16:51:32', '2017-04-16 17:04:08', 3, 'Onprogress (TM)'),
(382, '58', 1, '2017-04-16 16:51:32', '2017-04-16 17:04:25', 2, 'Onprogress (TM)'),
(383, '58', 2, '2017-04-16 16:51:32', '2017-04-16 17:04:26', 3, 'Onprogress (TM)'),
(384, '58', 3, '2017-04-16 16:51:32', '2017-04-16 17:04:27', 2, 'Onprogress (TM)'),
(385, '59', 1, '2017-04-16 16:51:32', '2017-04-16 17:04:41', 3, 'Onprogress (TM)'),
(386, '60', 1, '2017-04-17 06:44:09', '2017-04-17 06:58:02', 2, 'OK (TM)'),
(387, '60', 2, '2017-04-17 06:44:09', '2017-04-17 06:58:05', 3, 'OK (TM)'),
(388, '61', 1, '2017-04-17 06:45:14', '2017-04-17 06:57:41', 2, 'OK (TM)'),
(389, '61', 2, '2017-04-17 06:45:14', '2017-04-17 06:57:44', 3, 'OK (TM)'),
(390, '61', 6, '2017-04-17 06:45:14', '2017-04-17 06:57:47', 3, 'OK (TM)'),
(391, '62', 1, '2017-04-17 06:46:23', '2017-04-17 06:56:31', 2, 'OK (TM)'),
(392, '62', 3, '2017-04-17 06:46:23', '2017-04-17 06:56:35', 2, 'OK (TM)'),
(393, '62', 6, '2017-04-17 06:46:23', '2017-04-17 06:56:37', 3, 'OK (TM)'),
(394, '62', 8, '2017-04-17 06:46:23', '2017-04-17 06:56:40', 2, 'OK (TM)'),
(395, '63', 1, '2017-04-17 06:47:19', '2017-04-17 07:03:10', 3, 'OK (TM)'),
(396, '63', 3, '2017-04-17 06:47:19', '2017-04-17 07:03:13', 2, 'OK (TM)'),
(398, '63', 6, '2017-04-17 06:47:19', '2017-04-17 07:03:16', 3, 'OK (TM)'),
(399, '63', 8, '2017-04-17 06:47:19', '2017-04-17 07:03:20', 2, 'OK (TM)'),
(400, '64', 1, '2017-04-27 10:34:01', '2017-05-02 17:00:24', 3, 'onprogress (SV)'),
(401, '64', 2, '2017-04-27 10:34:01', '2017-05-02 15:51:26', 3, 'onprogress (SV)'),
(402, '64', 3, '2017-04-27 10:34:01', '2017-04-27 10:34:01', 2, 'Onprogress (AN)'),
(403, '64', 4, '2017-04-27 10:34:01', '2017-04-27 10:34:01', 7, 'Onprogress (AN)'),
(404, '64', 5, '2017-04-27 10:34:01', '2017-04-27 10:34:01', 7, 'Onprogress (AN)'),
(405, '64', 6, '2017-04-27 10:34:01', '2017-05-02 16:35:24', 3, 'onprogress (SV)'),
(406, '64', 7, '2017-04-27 10:34:01', '2017-04-27 10:34:01', 7, 'Onprogress (AN)'),
(407, '65', 1, '2017-04-27 10:34:01', '2017-04-27 10:34:01', 2, 'Onprogress (AN)'),
(408, '65', 7, '2017-04-27 10:34:01', '2017-04-27 10:34:01', 7, 'Onprogress (AN)'),
(409, '66', 6, '2017-04-28 13:35:56', '2017-04-28 13:35:56', 3, 'Onprogress (AN)'),
(410, '66', 7, '2017-04-28 13:35:56', '2017-04-28 13:35:56', 7, 'Onprogress (AN)'),
(411, '67', 1, '2017-04-28 13:35:56', '2017-04-28 13:35:56', 2, 'Onprogress (AN)'),
(412, '67', 7, '2017-04-28 13:35:56', '2017-04-28 13:35:56', 7, 'Onprogress (AN)');

--
-- Trigger `parameter_sample`
--
DELIMITER $$
CREATE TRIGGER `para_sample_status` AFTER UPDATE ON `parameter_sample` FOR EACH ROW BEGIN
		DECLARE count_tm, count_oktm, count_osv, count_all Int;
        SET count_tm = 0;
		SET count_all = 0;
        SET count_oktm = 0;
        SET count_osv = 0;
		
		SELECT count(*) INTO count_all FROM parameter_sample WHERE sample_id = NEW.sample_id;
        SELECT SUM(IF(status_parameter = "Onprogress (TM)", 1,0)) INTO count_tm FROM parameter_sample WHERE sample_id = NEW.sample_id;
        SELECT SUM(IF(status_parameter = "OK (TM)", 1,0)) INTO count_oktm FROM parameter_sample WHERE sample_id = NEW.sample_id;
        SELECT SUM(IF(status_parameter = "Onprogress (SV)", 1,0)) INTO count_osv FROM parameter_sample WHERE sample_id = NEW.sample_id;

		IF count_all = count_tm THEN
			UPDATE sample SET status = 'Onprogress (TM)' WHERE id = NEW.sample_id;
        ELSEIF count_all = count_oktm THEN
			UPDATE sample SET status = 'OK (TM)' WHERE id = NEW.sample_id;
		ELSEIF count_osv > 0 THEN
			UPDATE sample SET status = 'Onprogress (SV)' WHERE id = NEW.sample_id;
        ELSE
        	UPDATE sample SET status = 'Onprogress (AN)' WHERE id = NEW.sample_id;
		END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ref_parameter`
--

CREATE TABLE `ref_parameter` (
  `id` int(11) NOT NULL,
  `ref_test_id` int(11) NOT NULL,
  `parameter_id` int(11) NOT NULL,
  `volume_waste_maksimum` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sign` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dosage_maksimum` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `ref_parameter`
--

INSERT INTO `ref_parameter` (`id`, `ref_test_id`, `parameter_id`, `volume_waste_maksimum`, `sign`, `dosage_maksimum`, `created_at`, `updated_at`) VALUES
(1, 3, 1, '', NULL, '2', NULL, NULL),
(2, 4, 3, '', NULL, '4', NULL, NULL),
(3, 4, 4, '', NULL, '6', NULL, NULL),
(4, 3, 7, '', NULL, '10', NULL, NULL),
(5, 5, 2, '', NULL, '15', NULL, NULL),
(6, 5, 3, '', NULL, '16', NULL, NULL),
(7, 5, 5, '', NULL, '20', NULL, NULL),
(8, 1, 1, '', NULL, '25', NULL, NULL),
(9, 1, 3, '', NULL, '1', NULL, NULL),
(10, 1, 6, '', NULL, '3', NULL, NULL),
(11, 1, 8, '', '>', '6', NULL, NULL),
(12, 2, 2, '', NULL, '7', NULL, NULL),
(13, 2, 5, '', NULL, '4', NULL, NULL),
(14, 2, 4, '', NULL, '9', NULL, NULL),
(15, 8, 7, '', NULL, '10', NULL, NULL),
(16, 8, 6, '', NULL, '11', NULL, NULL),
(17, 9, 1, '', NULL, '12', NULL, NULL),
(18, 9, 2, '', NULL, '13', NULL, NULL),
(19, 9, 5, '', NULL, '5', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ref_test`
--

CREATE TABLE `ref_test` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `matrix_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `ref_test`
--

INSERT INTO `ref_test` (`id`, `kode`, `name`, `matrix_id`, `level`, `created_at`, `updated_at`) VALUES
(1, '1', 'PERGUB/72/2014', 1, 0, NULL, NULL),
(2, '2', 'PERGUB/52/2014', 1, 0, NULL, NULL),
(3, '3', 'KEPPRES/72/2014', 3, 0, NULL, NULL),
(4, '1.1', 'Kertas', 1, 1, NULL, NULL),
(5, '1.2', 'Minyak', 1, 1, NULL, NULL),
(6, '1.1.1', 'Pulp', 1, 2, NULL, NULL),
(7, '1.1.2', 'Kayu', 1, 2, NULL, NULL),
(8, '4', 'PERMENKES/19/2010', 2, 0, NULL, NULL),
(9, '5', 'Perda/02/2008', 2, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `result`
--

CREATE TABLE `result` (
  `id` int(11) NOT NULL,
  `para_sample_id` int(11) NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_result` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `result`
--

INSERT INTO `result` (`id`, `para_sample_id`, `value`, `status_result`, `created_at`, `updated_at`) VALUES
(2, 315, '3154', 'waiting', '2017-03-31 08:13:30', '2017-03-31 08:13:30'),
(3, 321, '3213', 'waiting', '2017-03-31 08:13:30', '2017-03-31 08:13:30'),
(4, 327, '3272', 'waiting', '2017-03-31 08:13:30', '2017-03-31 08:13:30'),
(5, 333, '3331', 'waiting', '2017-03-31 08:13:30', '2017-03-31 08:13:30'),
(6, 315, '3159', 'waiting', '2017-03-31 08:28:58', '2017-03-31 08:28:58'),
(7, 327, '3272', 'waiting', '2017-03-31 08:28:58', '2017-03-31 08:28:58'),
(8, 287, '1', 'waiting', NULL, NULL),
(9, 288, '2', 'waiting', NULL, NULL),
(10, 289, '3', 'waiting', NULL, NULL),
(11, 290, '4', 'waiting', NULL, NULL),
(16, 345, '6', 'revision(SV)', '2017-04-09 15:54:21', '2017-04-09 22:41:22'),
(17, 349, '7', 'waiting', '2017-04-09 15:54:21', '2017-04-09 15:54:21'),
(18, 352, '8', 'waiting', '2017-04-09 15:54:21', '2017-04-09 15:54:21'),
(20, 351, '9', 'waiting', '2017-04-09 15:57:33', '2017-04-09 15:57:33'),
(26, 346, '20', 'revision(SV)', '2017-04-09 16:13:49', '2017-04-09 22:44:49'),
(27, 345, '16', 'OK(SV)', '2017-04-09 22:56:23', '2017-04-09 23:00:37'),
(28, 346, '21', 'OK(SV)', '2017-04-09 22:58:34', '2017-04-09 23:00:41'),
(29, 347, '5.9', 'waiting', '2017-04-09 22:59:00', '2017-04-09 22:59:00'),
(30, 343, '8.7', 'OK(SV)', '2017-04-09 23:02:01', '2017-04-09 23:02:25'),
(31, 344, '1.1', 'OK(SV)', '2017-04-09 23:02:15', '2017-04-09 23:02:29'),
(32, 356, '9', 'revision(SV)', '2017-04-09 23:42:11', '2017-04-10 07:59:15'),
(33, 367, '1', 'waiting', '2017-04-10 07:56:38', '2017-04-10 07:56:38'),
(34, 371, '2', 'waiting', '2017-04-10 07:56:38', '2017-04-10 07:56:38'),
(35, 375, '3', 'waiting', '2017-04-10 07:56:38', '2017-04-10 07:56:38'),
(36, 379, '4', 'OK (SV)', '2017-04-10 07:56:38', '2017-04-18 05:50:41'),
(37, 356, '10', 'waiting', '2017-04-10 08:01:36', '2017-04-10 08:01:36'),
(38, 359, '10', 'Revision (SV)', '2017-04-10 23:42:11', '2017-04-10 23:42:11'),
(39, 359, '11', 'Revision (SV)', '2017-04-10 23:42:20', '2017-04-10 23:42:20'),
(40, 359, '12', 'OK (TM)', '2017-04-10 23:42:30', '2017-04-16 17:21:21'),
(41, 360, '60.1', 'Revision (SV)', '2017-04-10 23:42:12', '2017-04-10 23:42:12'),
(42, 360, '10', 'OK (TM)', '2017-04-10 23:42:45', '2017-04-16 17:21:25'),
(43, 380, '11', 'OK (SV)', '2017-04-16 16:55:19', '2017-04-16 17:04:05'),
(44, 382, '13', 'OK (SV)', '2017-04-16 16:55:25', '2017-04-16 17:04:25'),
(45, 384, '9.9', 'OK (SV)', '2017-04-16 16:55:56', '2017-04-16 17:04:27'),
(46, 385, '77', 'OK (SV)', '2017-04-16 16:58:16', '2017-04-16 17:04:41'),
(47, 381, '99', 'OK (SV)', '2017-04-16 16:58:56', '2017-04-16 17:04:08'),
(48, 383, '78', 'OK (SV)', '2017-04-16 16:59:04', '2017-04-16 17:04:26'),
(49, 386, '1', 'OK (TM)', '2017-04-17 06:48:14', '2017-04-17 06:58:02'),
(50, 388, '2', 'OK (TM)', '2017-04-17 06:48:21', '2017-04-17 06:57:41'),
(51, 391, '3', 'OK (TM)', '2017-04-17 06:48:28', '2017-04-17 06:56:31'),
(52, 392, '10', 'OK (TM)', '2017-04-17 06:48:49', '2017-04-17 06:56:35'),
(53, 396, '11', 'OK (TM)', '2017-04-17 06:48:55', '2017-04-17 07:03:13'),
(54, 394, '9', 'OK (TM)', '2017-04-17 06:49:26', '2017-04-17 06:56:40'),
(55, 399, '12', 'OK (TM)', '2017-04-17 06:49:33', '2017-04-17 07:03:20'),
(56, 395, '9', 'OK (TM)', '2017-04-17 06:50:22', '2017-04-17 07:03:10'),
(57, 387, '11', 'OK (TM)', '2017-04-17 06:50:36', '2017-04-17 06:58:06'),
(58, 389, '12', 'OK (TM)', '2017-04-17 06:50:42', '2017-04-17 06:57:44'),
(59, 390, '11', 'OK (TM)', '2017-04-17 06:51:27', '2017-04-17 06:57:47'),
(60, 393, '12', 'OK (TM)', '2017-04-17 06:51:36', '2017-04-17 06:56:37'),
(61, 398, '13', 'OK (TM)', '2017-04-17 06:51:44', '2017-04-17 07:03:16'),
(62, 361, '10', 'OK (SV)', '2017-04-18 06:20:53', '2017-04-18 06:22:29'),
(63, 363, '10', 'OK (SV)', '2017-04-18 06:21:06', '2017-04-18 06:22:31'),
(64, 362, '11', 'OK (SV)', '2017-04-18 06:22:08', '2017-04-18 06:22:30'),
(65, 401, '12', 'waiting', '2017-05-02 15:51:26', '2017-05-02 15:51:26'),
(66, 374, '12', 'waiting', '2017-05-02 15:52:22', '2017-05-02 15:52:22'),
(67, 405, '12', 'waiting', '2017-05-02 16:35:24', '2017-05-02 16:35:24'),
(68, 400, '12', 'waiting', '2017-05-02 17:00:24', '2017-05-02 17:00:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Front_Office', 'Front Office', NULL, '2017-02-13 00:13:59', '2017-02-13 00:13:59'),
(5, 'Analyst', 'Analyst', NULL, '2017-02-13 00:14:00', '2017-02-13 00:14:00'),
(6, 'Supervisor', 'Supervisor', NULL, '2017-02-13 00:14:00', '2017-02-13 00:14:00'),
(7, 'Technical_manager', 'Technical Manager', NULL, NULL, NULL),
(8, 'Administrator', 'Administrator', NULL, NULL, NULL),
(9, 'Head_of_unit', 'Head of Unit', NULL, NULL, NULL),
(10, 'Section_head', 'Selection Head', NULL, NULL, NULL),
(11, 'viewer', 'Viewer', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 5),
(2, 6),
(3, 5),
(4, 6),
(5, 7),
(6, 8),
(7, 5),
(21, 5),
(22, 5),
(23, 1),
(24, 1),
(25, 5),
(29, 5),
(29, 6),
(30, 9),
(31, 10),
(32, 11);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sample`
--

CREATE TABLE `sample` (
  `id` int(11) NOT NULL,
  `kode_sample` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_test_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `procedure_id` int(11) NOT NULL,
  `matrix_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_sampled` date NOT NULL,
  `time_sampled` time NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `preservative` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coordinat_S` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coordinat_E` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `sample`
--

INSERT INTO `sample` (`id`, `kode_sample`, `ref_test_id`, `job_id`, `procedure_id`, `matrix_id`, `name`, `date_sampled`, `time_sampled`, `status`, `price`, `preservative`, `coordinat_S`, `coordinat_E`, `created_at`, `updated_at`) VALUES
(51, '7.2', 1, 7, 4, 3, 'INLET/2', '2017-04-05', '11:11:00', 'OK (TM)', '0', 'Es', '1°1''1"', '1°1''1"', '2017-04-10 02:14:23', '2017-04-10 02:14:23'),
(52, '7.3', 1, 7, 4, 1, 'INLET/3', '2017-04-05', '11:11:00', 'Onprogress (TM)', '0', 'Es', '1°1''1"', '1°1''1"', '2017-04-10 02:14:24', '2017-04-10 02:14:24'),
(55, '9.1', 1, 9, 4, 3, 'INLET/1', '2017-04-04', '13:00:00', 'Onprogress (SV)', '0', 'Es', '1°1''1"', '1°1''1"', '2017-04-10 07:50:30', '2017-04-10 07:50:30'),
(56, '9.2', 1, 9, 4, 3, 'OUTLET/1', '2017-04-06', '13:01:00', 'Onprogress (AN)', '0', 'Es', '1°1''1"', '1°1''1"', '2017-04-10 07:50:30', '2017-04-10 07:50:30'),
(57, '10.1', 1, 10, 1, 3, 'INLET/1', '2017-04-11', '11:11:00', 'Onprogress (TM)', '0', 'Es', '1°1''11"', '1°1''1"', '2017-04-16 16:51:31', '2017-04-16 16:51:31'),
(58, '10.2', 1, 10, 4, 1, 'INLET/2', '2017-04-10', '12:00:00', 'Onprogress (TM)', '0', 'Es', '1°1''11"', '1°1''1"', '2017-04-16 16:51:32', '2017-04-16 16:51:32'),
(59, '10.3', 1, 10, 3, 2, 'INLET/3', '2017-04-11', '13:00:00', 'Onprogress (TM)', '0', 'Es', '11°1''1"', '1°1''1"', '2017-04-16 16:51:32', '2017-04-16 16:51:32'),
(60, '11.1', 1, 11, 4, 3, 'INLET/1', '2017-04-05', '11:11:00', 'OK (TM)', '0', 'Es', '1°1''11"', '1°1''1"', '2017-04-17 06:44:08', '2017-04-17 06:44:08'),
(61, '12.1', 1, 12, 4, 3, 'INLET/1', '2017-04-11', '00:00:01', 'OK (TM)', '0', 'Es', '1°1''1"', '1°11''1"', '2017-04-17 06:45:14', '2017-04-17 06:45:14'),
(62, '13.1', 1, 13, 4, 3, 'INLET/1', '2017-04-12', '11:11:00', 'OK (TM)', '0', 'Es', '1°1''1"', '1°1''1"', '2017-04-17 06:46:23', '2017-04-17 06:46:23'),
(63, '14.1', 1, 14, 1, 1, 'IN', '2017-04-11', '11:11:00', 'OK (TM)', '0', 'Es', '1°1''1"', '1°1''11"', '2017-04-17 06:47:19', '2017-04-17 06:47:19'),
(64, '15.1', 8, 15, 3, 2, 'INLET', '2017-04-01', '11:11:00', 'Onprogress (SV)', '0', 'Es', '1°1''1"', '11°1''1"', '2017-04-27 10:34:01', '2017-04-27 10:34:01'),
(65, '15.2', 3, 15, 4, 3, 'OUTLET', '2017-04-02', '22:22:00', 'Onprogress (AN)', '0', 'Es', '2°2''2"', '2°2''22"', '2017-04-27 10:34:01', '2017-04-27 10:34:01'),
(66, '16.1', 8, 16, 3, 2, 'INLET', '2017-04-04', '11:11:00', 'Onprogress (AN)', '13000', 'Es', '1°1''1"', '11°1''1"', '2017-04-28 13:35:56', '2017-04-28 13:35:56'),
(67, '16.2', 3, 16, 4, 3, 'OUTLET', '2017-04-06', '12:00:00', 'Onprogress (AN)', '8000', 'Es', '2°2''2"', '2°22''2"', '2017-04-28 13:35:56', '2017-04-28 13:35:56');

--
-- Trigger `sample`
--
DELIMITER $$
CREATE TRIGGER `sample_status` AFTER UPDATE ON `sample` FOR EACH ROW BEGIN
		DECLARE count_tm, count_oktm, count_all Int;
        SET count_tm = 0;
		SET count_all = 0;
        SET count_oktm = 0;
		
		SELECT count(*) INTO count_all FROM sample WHERE job_id = NEW.job_id;
        SELECT SUM(IF(status = "Onprogress (TM)", 1,0)) INTO count_tm FROM sample WHERE job_id = NEW.job_id;
        SELECT SUM(IF(status = "OK (TM)", 1,0)) INTO count_oktm FROM sample WHERE job_id = NEW.job_id;

		IF count_all = count_oktm THEN
			UPDATE job SET status = 'OK' WHERE id = NEW.job_id;
		ELSEIF count_tm > 0 THEN
			UPDATE job SET status = 'Onprogress (TM)' WHERE id = NEW.job_id;
        ELSE
        	UPDATE job SET status = 'Onprogress'WHERE id = NEW.job_id;
		END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sample_matrix`
--

CREATE TABLE `sample_matrix` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `sample_matrix`
--

INSERT INTO `sample_matrix` (`id`, `name`, `alias`, `created_at`, `updated_at`) VALUES
(1, 'Air Limbah', 'ALI', NULL, '2017-04-19 15:30:23'),
(2, 'Air Badan Air', 'ABA', NULL, '2017-04-19 15:57:10'),
(3, 'AM', 'AM', NULL, NULL),
(4, 'Udara Ambient', 'UA', '2017-04-19 15:45:49', '2017-04-19 15:45:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sampling_procedure`
--

CREATE TABLE `sampling_procedure` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_procedure` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `matrix_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `sampling_procedure`
--

INSERT INTO `sampling_procedure` (`id`, `name_procedure`, `matrix_id`, `created_at`, `updated_at`) VALUES
(1, 'SNI1', 1, NULL, NULL),
(2, 'SNI2', 1, NULL, NULL),
(3, 'SNI3', 2, NULL, NULL),
(4, 'SNI4', 3, NULL, NULL),
(5, 'SNI5', 1, NULL, NULL),
(6, 'SNI6', 2, NULL, NULL),
(7, 'SNI7', 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `type_user`
--

CREATE TABLE `type_user` (
  `id_type_user` int(10) UNSIGNED NOT NULL,
  `name_type_user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `address`, `email`, `phone`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Front', 'Malang', 'front@dlh.com', '0317496869', '$2y$10$Xw1OT6xTZa.Ydny5ScATXuG882NiKzprrQ/HsDPAdlvpanUUh0XoG', 'Qiu1LdZrOkgCrbY8vbHtxVhVVE9spOgKgzP6eA8JofvWKRSA7e6YdjcgdXgE', '2017-02-13 00:14:00', '2017-04-19 03:35:37'),
(2, 'Analyst', NULL, 'analyst@dlh.com', NULL, '$2y$10$kAWTmcjG9nbamodRazHXiuL3S0sP2fZIGOn82eyOxsQfT7qu/tJ26', 'hagnhGE2LsjreNar2152C1D8GTPDhuQVE3CqPb2FizTR4r3zZHMVNAMTFqwz', '2017-02-13 00:14:00', '2017-02-13 00:14:00'),
(3, 'analyst2', NULL, 'analyst2@dlh.com', NULL, '$2y$10$sT1mOY1pSChOXGlOj8kvL.SBXl4LKK0KZjNjCErQDiVJiJDkVZy56', '4GxCuhp3e3HrmvuYdGHDgJsP4bxf87XiaJRj8QZ1hYmEuhSDK8ugUJxP97vx', '2017-03-21 13:49:58', '2017-03-21 13:49:58'),
(4, 'supervisor', NULL, 'supervisor@dlh.com', NULL, '$2y$10$kEoRsrp/kgYqIGJJMGHxUO5bcf7dv7js2lKWeh33BPA2vdgFKs4xq', 'QtZAP0Cy5K4fVxMSPKRPKri7G7bIKgSBsSjkK0H8RI1G0KeqAKR9balFsMrm', '2017-03-31 15:38:47', '2017-03-31 15:38:47'),
(5, 'Technical Manager', NULL, 'technical_manager@dlh.com', NULL, '$2y$10$XHryGZ3dZ3KVovktpFRTD.ldIXS2zYUvpci0T/hbTbNJvjEL4hBVi', '78GNWJq1epXQtpols1mSTa285p7XH25fT5odI2pFpS5C75ErzTGPzuXXSyLE', '2017-04-04 00:08:20', '2017-04-04 00:08:20'),
(6, 'Administrator', NULL, 'admin@dlh.com', NULL, '$2y$10$2w1DlA0ZjjhVk8tuG3WCNOcPaZXgtTujLrP.jh4BljXBAQgrx6i3a', 'dUacPyZnXt6hEythctHsmzJsRUQ28kQB1QN0yQRJgjEU0pTdp3qDsaBLl21T', '2017-04-04 04:35:26', '2017-04-04 04:35:26'),
(7, 'analyst3', 'JL. BUNTARAN NO. 8', 'analyst3@dlh.com', '0999999', '$2y$10$ixlv1SwCyzzBUp8VH8O9Y.L45r/.W59xauMlQ7fL8oZoVw2qYt4gO', 'ByG7U21tPwujckT2xHqSiBCCSsCzpQKc5djW7vdxi0ztav4G08mNfgTOmrAt', '2017-04-04 08:11:11', '2017-04-19 03:36:15'),
(20, 'analyst4', NULL, 'analyst4@dlh.com', NULL, '$2y$10$VxfC1nb1m1phFFmlZqQFfeWC/GcLwmr4GVv76RpSn0RZp/zS8tM4O', NULL, '2017-04-04 15:03:09', '2017-04-04 15:03:09'),
(21, 'analyst5', NULL, 'analyst5@dlh.com', NULL, '$2y$10$KJS6vucyrRGRD6MRshym4eaVcdi2iNH5JEi5GcT4ll6PczlAzUCIW', NULL, '2017-04-04 15:03:48', '2017-04-04 15:03:48'),
(22, 'analyst6', NULL, 'analyst6@dlh.com', NULL, '$2y$10$gBWLfl8myvBTs1NyregNS.zuLN2v.5i3EyATNrCkJCFNMmbDZw0SC', NULL, '2017-04-04 15:43:43', '2017-04-04 15:43:43'),
(23, 'Maya', NULL, 'maya@dlh.com', NULL, '$2y$10$iS/CENorQgDEohmC8R9/BuoqIj6EJhez11wVcurdCiZztgSDnMX3q', NULL, '2017-04-19 01:08:19', '2017-04-19 01:08:19'),
(24, 'Dono', NULL, 'dono@dlh.com', NULL, '$2y$10$t02LNnMz2xShNc4vN6bHUuLi4tyhAfyY7XwitgGCsHQQj9OlBFpNG', NULL, '2017-04-19 01:12:11', '2017-04-19 01:12:11'),
(25, 'Diana', 'Surabaya', 'diana@dlh.com', '0987654321', '$2y$10$PMgrUDti2FNBX.OiqnbeueV2zRVWaUm7HhWCmmsTDOgGhPwX0YtWm', NULL, '2017-04-19 01:19:14', '2017-04-19 01:19:14'),
(29, 'Mauwi', 'Ambarawa', 'mauwi@dlh.com', '0987654', '$2y$10$YJFDphi5S3bIrfv17ncYTeRM2I1rFnKvDv7dN9HBF8saqcPvArsqC', NULL, '2017-04-19 01:50:19', '2017-04-19 01:50:19'),
(30, 'Head of Unit', 'Surabaya', 'head@dlh.com', '09875555', '$2y$10$8LVqvXgmr69h3/dwFK824u4H4khpTSUy/XlPrRkhdkQfLX5QAvQXm', 'WrowMUZL8T9om1Tjwa4Cp3dOsAZQ2ogt3m8BiqRZVv3yaBIG1Ol4FanQLAZQ', '2017-04-28 06:44:29', '2017-04-28 06:44:29'),
(31, 'Selection Head', 'Surabaya', 'selection@dlh.com', '0987654321', '$2y$10$YY5W6cP21iaa9sm6dUddIOHYdwVQWAdwCjO7jCe28HJlzJqUGchY2', '2hXlNRsnaBI0nnLRsXBqYcVVuZgu24PIRjvapvGzSLg60qAecxRKfoUnNoBO', '2017-04-28 07:12:16', '2017-04-28 07:12:16'),
(32, 'viewer', 'Sidoarjo', 'viewer@dlh.com', '0546728282888', '$2y$10$A2mke5ePMw.K962JmzVd7uiYi9jF1hcjvqdIm0Gl0Sapy9VxbM6DS', 'bqSgsSdA5GwOLzmojImvDhQDdTa8jSg8PVLwKAQDhNwqHTCQHTbhyJ2I4HCV', '2017-04-28 07:17:33', '2017-04-28 07:17:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `late_record`
--
ALTER TABLE `late_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parameter`
--
ALTER TABLE `parameter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parameter_sample`
--
ALTER TABLE `parameter_sample`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `ref_parameter`
--
ALTER TABLE `ref_parameter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_test`
--
ALTER TABLE `ref_test`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode` (`kode`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sample`
--
ALTER TABLE `sample`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode_sample` (`kode_sample`);

--
-- Indexes for table `sample_matrix`
--
ALTER TABLE `sample_matrix`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sampling_procedure`
--
ALTER TABLE `sampling_procedure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_user`
--
ALTER TABLE `type_user`
  ADD PRIMARY KEY (`id_type_user`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `job`
--
ALTER TABLE `job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `late_record`
--
ALTER TABLE `late_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `parameter`
--
ALTER TABLE `parameter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `parameter_sample`
--
ALTER TABLE `parameter_sample`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=413;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ref_parameter`
--
ALTER TABLE `ref_parameter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `ref_test`
--
ALTER TABLE `ref_test`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sample`
--
ALTER TABLE `sample`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `sample_matrix`
--
ALTER TABLE `sample_matrix`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sampling_procedure`
--
ALTER TABLE `sampling_procedure`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `type_user`
--
ALTER TABLE `type_user`
  MODIFY `id_type_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
